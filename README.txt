General Information

DPDesktop provides an desktop interface to the web-based project management
application dotProject.

We would like to thank you for the usage of this program and hope you feel
comfortable with it.

This program is written by Heiner Reinhardt and Tolleiv Nietsch

********************************************************************************

Building

- You need to install the program "ant".

- Just type "ant" in console and a jar file plus the project documentation will
  be available in the distribution path.

********************************************************************************

License

See LICENSE.txt for more information.