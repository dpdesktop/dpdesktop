/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/


//TODO[z][Feature] Does not allow to use 2 dpdesktop applications at the same time.
//TODO[z][Feature] Language hint from webservice, which languages are allowed for commit
//TODO[z][Feature] commit stack
//TODO[z][Feature] clean up directories when RemotePlaceID is changed, or RemotePlace is deleted
//TODO[z] idle controller may listen on keyboard input (JNI?)
//TODO[z][Feature] support reference-field of dotProject
package net.sf.dpdesktop;

import java.util.Locale;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
/**
 *
 * @author Heiner Reinhardt
 */
public class Main {

    public static void main(String args[]) {

        //LOGGING LEVEL

        Level level = Level.INFO;



        //ENABLE TRAY IF SUPPORTED

        boolean enableTrayIfSupported = true;


        //INIT


        BasicConfigurator.configure();
        Logger root = Logger.getRootLogger();
        root.setLevel(level);


        new MainController(enableTrayIfSupported);

    }



}
