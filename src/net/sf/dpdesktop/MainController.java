/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop;


import java.awt.event.ActionEvent;
import net.sf.dpdesktop.module.update.UpdateController;
import net.sf.dpdesktop.module.version.VersionController;
import net.sf.dpdesktop.module.about.AboutController;
import net.sf.dpdesktop.module.guisave.GuiSaveController;
import net.sf.dpdesktop.module.tracking.IdleController;
import net.sf.dpdesktop.module.tracking.TrackingController;
import net.sf.dpdesktop.module.util.hash.HashController;
import net.sf.dpdesktop.module.util.ssl.HttpsController;
import net.sf.dpdesktop.module.settings.SettingsController;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import java.awt.Frame;
import java.awt.SystemTray;
import java.awt.event.ActionListener;
import net.sf.dpdesktop.gui.DatabasePane;
import net.sf.dpdesktop.gui.HistoryPane;
import net.sf.dpdesktop.gui.submit.LogPane;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.gui.MessageDialog;
import net.sf.dpdesktop.gui.QuestionDialog;
import net.sf.dpdesktop.gui.SelectionPane;
import net.sf.dpdesktop.gui.TrackingPane;
import net.sf.dpdesktop.gui.submit.QuickSubmitDialog;
import net.sf.dpdesktop.gui.CommonTray;
import net.sf.dpdesktop.module.guistate.Tray;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.guistate.StateController;
import net.sf.dpdesktop.module.quicksubmit.QuickSubmitController;
import net.sf.dpdesktop.module.submit.SubmitController;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.module.remote.RemotePlaceController;

/**
 *
 * @author Heiner Reinhardt
 */
public class MainController {


    public MainController(boolean enableTrayIfSupported) {

        Injector injector = Guice.createInjector(new Module());



        //injector.getInstance(ConfigurationService.class);

        // check whether MD5 is available
        injector.getInstance(HashController.class);

        // enable https
        injector.getInstance(HttpsController.class);

        // GUI STATE SAVER

        injector.getInstance(GuiSaveController.class);

        // IDLE

        injector.getInstance(IdleController.class); 

        // set language
        // injector.getInstance(LocaleController.class);

        // make about dialog available
        AboutController aboutController = injector.getInstance(AboutController.class);


        // REMOTE PLACE CONTROLLER

        injector.getInstance(RemotePlaceController.class);

        // SETTINGS CONTROLLER

        SettingsController settingsController = injector.getInstance(SettingsController.class);

        // UPDATE CONTROLLER

        UpdateController updateController = injector.getInstance(UpdateController.class);

        updateController.registerComponent(injector.getInstance(DatabasePane.class));

        // STATE CONTROLLER

        boolean supportTray = enableTrayIfSupported && SystemTray.isSupported();

        StateController stateController = injector.getInstance(StateController.class);
        
        stateController.setTraySupported(supportTray);

        if(supportTray) {
            stateController.registerComponent(injector.getInstance(CommonTray.class));
        }

        stateController.registerComponent(injector.getInstance(MainWindow.class));

        // TRACKING CONTROLLER

        TrackingController trackingController = injector.getInstance(TrackingController.class);

        trackingController.registerComponent(injector.getInstance(MainWindow.class));
        trackingController.registerComponent(injector.getInstance(SelectionPane.class));
        trackingController.registerComponent(injector.getInstance(TrackingPane.class));

        if(supportTray) {
            trackingController.registerComponent(injector.getInstance(CommonTray.class));
        }

        // MAIN WINDOW CREATION

        MainWindow mainWindow = injector.getInstance(MainWindow.class);

        mainWindow.addSubView(injector.getInstance(SelectionPane.class));
        mainWindow.addSubView(injector.getInstance(TrackingPane.class));
        mainWindow.addSubView(injector.getInstance(DatabasePane.class));
        mainWindow.addSubView(injector.getInstance(HistoryPane.class));

        settingsController.registerStartupComponent(mainWindow);

        aboutController.registerComponent(mainWindow); 


        // QUICK STORE WINDOW

        injector.getInstance(QuickSubmitController.class);

        // SUBMIT WINDOW 
        
        injector.getInstance(SubmitController.class);
              

        // VERSION CONTROLLER

        VersionController versionController = injector.getInstance(VersionController.class);



        // forcing init of message dialog
        injector.getInstance(MessageDialog.class);
        // forcing init of question dialog
        injector.getInstance(QuestionDialog.class);

        /**
         * In order to provide better user experience, I would prefere the
         * application maxmimized, shortly before starting auto update.
         */

        ApplicationStateModel applicationStateModel = injector.getInstance(ApplicationStateModel.class);
        applicationStateModel.maximize();


        versionController.init();

        updateController.init();

    }

}
