/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import java.awt.Frame;
import net.sf.dpdesktop.gui.DatabasePane;
import net.sf.dpdesktop.gui.HistoryPane;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.gui.SelectionPane;
import net.sf.dpdesktop.gui.TrackingPane;
import net.sf.dpdesktop.gui.CommonTray;
import net.sf.dpdesktop.module.guistate.Tray;
import net.sf.dpdesktop.module.about.AboutModule;
import net.sf.dpdesktop.module.guisave.GuiSaveModule;
import net.sf.dpdesktop.module.guistate.GuiStateModule;
import net.sf.dpdesktop.module.message.MessageModule;
import net.sf.dpdesktop.module.progress.ProgressModule;
import net.sf.dpdesktop.module.quicksubmit.QuickSubmitModule;
import net.sf.dpdesktop.module.settings.SettingsModule;
import net.sf.dpdesktop.module.submit.SubmitModule;
import net.sf.dpdesktop.module.tracking.TrackingModule;
import net.sf.dpdesktop.module.update.UpdateModule;
import net.sf.dpdesktop.module.util.hash.HashModule;
import net.sf.dpdesktop.module.version.VersionModule;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.log.LogItemRepository;
import net.sf.dpdesktop.module.remote.RemotePlaceModule;
import net.sf.dpdesktop.module.util.ssl.SSLModule;

/**
 *
 * @author Heiner Reinhardt
 */
public class Module extends AbstractModule {



    @Override
    protected void configure() {

        this.install(new SSLModule());

        this.install(new SettingsModule());

        this.install(new AboutModule());
        this.install(new GuiSaveModule());
        this.install(new GuiStateModule());
        this.install(new MessageModule());
        this.install(new ProgressModule());
        this.install(new TrackingModule());
        this.install(new QuickSubmitModule());
        this.install(new SubmitModule()); 
        this.install(new UpdateModule());
        this.install(new VersionModule());
        this.install(new RemotePlaceModule());

        // Utilities
        this.install(new HashModule());


        bind(Frame.class).to( MainWindow.class );
        bind(MainWindow.class).in(Scopes.SINGLETON);

        // TRAY

        bind(Tray.class).to(CommonTray.class);
        bind(CommonTray.class).in(Scopes.SINGLETON);

        // GUI

        bind(DatabasePane.class).in(Scopes.SINGLETON);
        bind(HistoryPane.class).in(Scopes.SINGLETON);
        bind(TrackingPane.class).in(Scopes.SINGLETON);
        bind(SelectionPane.class).in(Scopes.SINGLETON); 

        // REPOSITORIES

        bind(LogItemRepository.class).in(Scopes.SINGLETON); 
        bind(ContainerRepository.class).in(Scopes.SINGLETON);

        // MESSAGES


        // TOOLS
        
        bind(ContextProvider.class).in(Scopes.SINGLETON);

    }

















}
