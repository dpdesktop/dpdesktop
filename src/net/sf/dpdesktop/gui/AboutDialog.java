/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui;

import com.google.inject.Inject;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;
import net.sf.dpdesktop.service.config.Configuration;
import org.jdesktop.swingx.JXHyperlink;

/**
 *
 * @author Heiner Reinhardt
 */
public class AboutDialog extends javax.swing.JDialog implements LocaleListener {

    private final LanguageModel languageModel;

    /** Creates new form AboutDialog */
    @Inject
    public AboutDialog(java.awt.Frame parent, LanguageModel languageModel) {
        super(parent, false);
        this.languageModel = languageModel;
        initComponents();
        languageModel.addLocaleListener(this); 


        homepageTextField.setEditable(false);
        versionTextField.setEditable(false);
        licenseTextField.setEditable(false);

    }

    public void setVersion(String version) {
        versionTextField.setText(version);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headlineLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        homepageTextField = new javax.swing.JTextField();
        licenseLabel = new javax.swing.JLabel();
        homepageLabel = new javax.swing.JLabel();
        versionLabel = new javax.swing.JLabel();
        versionTextField = new javax.swing.JTextField();
        licenseTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        contentTextArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(languageModel.getString("AboutDialog.title")); // NOI18N
        setResizable(false);

        headlineLabel.setFont(headlineLabel.getFont());
        headlineLabel.setText(languageModel.getString("AboutDialog.headline")); // NOI18N

        homepageTextField.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        homepageTextField.setText(languageModel.getString("AboutDialog.homepage")); // NOI18N
        homepageTextField.setBorder(null);

        licenseLabel.setText(languageModel.getString("AboutDialog.label.license.text")); // NOI18N

        homepageLabel.setFont(homepageLabel.getFont());
        homepageLabel.setText(languageModel.getString("AboutDialog.label.homepage.text")); // NOI18N

        versionLabel.setFont(versionLabel.getFont());
        versionLabel.setText(languageModel.getString("AboutDialog.label.version.text")); // NOI18N

        versionTextField.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        versionTextField.setText(languageModel.getString("AboutDialog.version")); // NOI18N
        versionTextField.setBorder(null);

        licenseTextField.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        licenseTextField.setText(languageModel.getString("AboutDialog.license")); // NOI18N
        licenseTextField.setBorder(null);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(versionLabel)
                    .addComponent(homepageLabel)
                    .addComponent(licenseLabel))
                .addGap(39, 39, 39)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(versionTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                    .addComponent(licenseTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homepageTextField))
                .addContainerGap(230, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(versionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(versionLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(homepageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homepageLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(licenseTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(licenseLabel))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jScrollPane1.setBorder(null);

        contentTextArea.setColumns(20);
        contentTextArea.setEditable(false);
        contentTextArea.setLineWrap(true);
        contentTextArea.setRows(3);
        contentTextArea.setText(languageModel.getString("AboutDialog.content")); // NOI18N
        contentTextArea.setWrapStyleWord(true);
        contentTextArea.setAutoscrolls(false);
        contentTextArea.setBorder(null);
        contentTextArea.setOpaque(false);
        jScrollPane1.setViewportView(contentTextArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(headlineLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                        .addGap(476, 476, 476))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(headlineLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea contentTextArea;
    private javax.swing.JLabel headlineLabel;
    private javax.swing.JLabel homepageLabel;
    private javax.swing.JTextField homepageTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel licenseLabel;
    private javax.swing.JTextField licenseTextField;
    private javax.swing.JLabel versionLabel;
    private javax.swing.JTextField versionTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void updateLocale(LanguageModel languageModel) {
        this.setTitle(languageModel.getString("AboutDialog.title"));
        headlineLabel.setText(languageModel.getString("AboutDialog.headline"));
        homepageTextField.setText(languageModel.getString("AboutDialog.homepage"));
        licenseLabel.setText(languageModel.getString("AboutDialog.label.license.text"));
        homepageLabel.setText(languageModel.getString("AboutDialog.label.homepage.text"));
        versionLabel.setText(languageModel.getString("AboutDialog.label.version.text"));
        versionTextField.setText(languageModel.getString("AboutDialog.version"));
        licenseTextField.setText(languageModel.getString("AboutDialog.license"));
        contentTextArea.setText(languageModel.getString("AboutDialog.content"));

    }
}
