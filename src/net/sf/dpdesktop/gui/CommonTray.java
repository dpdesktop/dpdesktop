/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui;

import net.sf.dpdesktop.module.guistate.Tray;
import com.google.inject.Inject;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import net.sf.dpdesktop.module.guistate.StateComponent;
import net.sf.dpdesktop.module.guistate.ApplicationStateListener;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.guistate.StateController;
import net.sf.dpdesktop.module.guistate.TraySupportEnum;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.module.tracking.TrackingComponent;
import net.sf.dpdesktop.module.tracking.TrackingController;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class CommonTray implements Tray, TrackingComponent, TrackingListener, ApplicationStateListener, LocaleListener {

    private final Image IMAGE_STOPPED = new ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/tray_stopped.png")).getImage();
    private final Image IMAGE_WORKING = new ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/tray_working.png")).getImage();
    private MenuItem exitItem = new MenuItem();
    private MenuItem startItem = new MenuItem();
    private MenuItem stopItem = new MenuItem();
    private MenuItem minimizeItem = new MenuItem();
    private MenuItem maximizeItem = new MenuItem();
    private SystemTray systemTray;
    private PopupMenu popupMenu = new PopupMenu();
    private String m_statusString;
    private Tracker m_tracker;
    private final ApplicationStateModel m_applicationStateModel;
    private TrayIcon trayIcon = new TrayIcon(IMAGE_STOPPED);
    private final LanguageModel languageModel;

    @Override
    public void updateLocale(LanguageModel languageModel) {

        popupMenu.setLabel(languageModel.getString("CommonTray.popupMenuLabel.text"));

        exitItem.setLabel(languageModel.getString("CommonTray.exitItemLabel.text"));
        startItem.setLabel(languageModel.getString("CommonTray.startItemLabel.text"));
        stopItem.setLabel(languageModel.getString("CommonTray.stopItemLabel.text"));
        minimizeItem.setLabel(languageModel.getString("CommonTray.minimizeItemLabel.text"));
        maximizeItem.setLabel(languageModel.getString("CommonTray.maximizeItemLabel.text"));

        trayIcon.setToolTip(languageModel.getString("CommonTray.tooltip.default"));


    }
    /**
     * Creates a new CommonTray object, if any CommonTray is not supported by
     * the current JRE.
     * In addition this method is thread save. 
     * @return null or a TrayIcon instance
     */
    @Inject
    public CommonTray(ApplicationStateModel applicationStateModel, Tracker tracker, LanguageModel languageModel) {

        this.languageModel = languageModel;
        this.m_tracker = tracker;
        this.m_applicationStateModel = applicationStateModel;


        systemTray = SystemTray.getSystemTray();

        trayIcon.setImageAutoSize(true);
        try {
            systemTray.add(trayIcon);
        } catch (AWTException ex) {

            /*
             * can be ignored because if tray is not supported, a dummy
             * tray was created before
             */

            Logger.getLogger(this.getClass()).fatal("Critical error, due hard coded stuff", ex);

        }

        popupMenu.add(startItem);
        popupMenu.add(stopItem);
        popupMenu.addSeparator();
        popupMenu.add(maximizeItem);
        popupMenu.add(minimizeItem);
        popupMenu.add(exitItem);

        trayIcon.setPopupMenu(popupMenu);



        m_tracker.addListener(this);
        m_applicationStateModel.addApplicationStateListener(this);
        languageModel.addLocaleListener(this);

    }



    @Override
    public void addTrackingListener(final TrackingListener l) {
        startItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                l.start();
            }
        });

        stopItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                l.stop();
            }
        });
    }



    @Override
    public void setApplicationStateListener(final ApplicationStateListener l) {
        exitItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                l.exiting();
            }

        });
        minimizeItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                l.minimize();
            }

        });
        maximizeItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                l.maximize();
            }
        });


        trayIcon.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getButton() == MouseEvent.BUTTON1) {

                    if (m_applicationStateModel.isMaximized()) {
                        l.minimize();
                    } else {
                        l.maximize();
                    }

                }

            }
        });
    }

    private void refreshToolTip() {

        String str;

        if (m_tracker.getTrackableContainer() == null) {
            str = languageModel.getString("CommonTray.tooltip.selectTask");
        } else {
            str = m_tracker.getTrackableContainer().getName() + " : " +
                    m_statusString + "  " +
                    m_tracker.getTimeAsString();
        }

        trayIcon.setToolTip(str);
    }
    public void infoMessage(String caption, String message) {
        this.infoMessage(caption, message, true);
    }

    public void infoMessage(String caption, String message, boolean forceRefresh) {
        if(forceRefresh) {
            refreshTray();
        }
        trayIcon.displayMessage(caption, message, TrayIcon.MessageType.INFO);

    }

    private void refreshTray() {
        try {
            systemTray.remove(trayIcon);
            systemTray.add(trayIcon);
        } catch (AWTException ex) {
            /*
             * can be ignored because if tray is not supported, a dummy
             * tray was created before
             */
             Logger.getLogger(this.getClass()).fatal("Hardcoded error: TrayIcon support has been already checked. Consequently this exception should not occur ", ex);
        }
    }

    @Override
    public void containerChanged(Container container) {
        refreshToolTip();
    }

    @Override
    public void start() {
        m_statusString = languageModel.getString("CommonTray.statusLabel.working.text");
        trayIcon.setImage(IMAGE_WORKING);
        startItem.setEnabled(false);
        stopItem.setEnabled(true);
        refreshToolTip();


        if (m_tracker.wasInIdle()) {
            infoMessage(languageModel.getString("CommonTray.idleMessageHeadline.text"), languageModel.getString("CommonTray.idleMessage.leavingIdleMode"));
        }

    }

    @Override
    public void stop() {
        m_statusString = languageModel.getString("CommonTray.statusLabel.stopped.text");
        trayIcon.setImage(IMAGE_STOPPED);
        startItem.setEnabled(true);
        stopItem.setEnabled(false);
        refreshToolTip();


        if (m_tracker.isInIdle()) {
            infoMessage(languageModel.getString("CommonTray.idleMessageHeadline.text"), languageModel.getString("CommonTray.idleMessage.enteringIdleMode"));
        }

    }

    @Override
    public void clear() {
        refreshToolTip();
    }

    @Override
    public void timeChanged() {
        refreshToolTip();
    }

    @Override
    public void ready() {
        m_statusString = languageModel.getString("CommonTray.statusLabel.stopped.text");
        startItem.setEnabled(true);
        stopItem.setEnabled(false);
    }

    @Override
    public void unready() {
        m_statusString = languageModel.getString("CommonTray.statusLabel.stopped.text");
        trayIcon.setImage(IMAGE_STOPPED);
        stopItem.setEnabled(false);
        startItem.setEnabled(false);
    }

    @Override
    public void commentChanged(String comment) {
        //
    }

    @Override
    public void summaryChanged(String summary) {
        //
    }

    @Override
    public void completeChanged(int complete) {
        //
    }

    @Override
    public void maximize() {
        minimizeItem.setEnabled(true);
        maximizeItem.setEnabled(false);
    }

    @Override
    public void minimize() {
        minimizeItem.setEnabled(false);
        maximizeItem.setEnabled(true);
    }

    @Override
    public void exiting() {
        // do nothing
    }

    @Override
    public TraySupportEnum isTraySupported() {
        return TraySupportEnum.TRAY_ENABLED; 
    }


}
