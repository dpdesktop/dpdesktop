/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui;

import java.awt.event.ComponentEvent;
import net.sf.dpdesktop.gui.submit.QuickSubmitDialog;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import java.util.ResourceBundle;
import net.sf.dpdesktop.module.guistate.ApplicationStateListener;
import net.sf.dpdesktop.module.guistate.TraySupportEnum;
import net.sf.dpdesktop.module.tracking.TrackingComponent;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import net.sf.dpdesktop.module.about.AboutStartupComponent;
import net.sf.dpdesktop.module.guisave.GuiSaveComponent;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.guistate.StateComponent;
import net.sf.dpdesktop.module.guistate.StateController;
import net.sf.dpdesktop.module.progress.ProgressListener;
import net.sf.dpdesktop.module.progress.ProgressModel;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;
import net.sf.dpdesktop.module.settings.SettingsStartupComponent;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.container.Container;
import org.jdesktop.swingx.JXFrame;
import org.jdesktop.swingx.JXTaskPane;

/**
 *
 * @author Heiner Reinhardt
 */
public class MainWindow extends JFrame implements TrackingComponent, StateComponent, GuiSaveComponent, TrackingListener, PropertyChangeListener, AboutStartupComponent, SettingsStartupComponent, ProgressListener, LocaleListener {

    private List<JXTaskPane> jXTaskPaneList = new LinkedList<JXTaskPane>();
    private MainWindowListener windowListener = new MainWindowListener();
    private final LanguageModel languageModel;

    @Inject
    /** Creates new form MainWindow */
    public MainWindow(ProgressModel progressModel, Tracker tracker, final ApplicationStateModel applicationStateModel, LanguageModel languageModel) {

        this.languageModel = languageModel;
        initComponents();

        languageModel.addLocaleListener(this);

        jXTaskPaneList.add(selectionTaskPane);
        jXTaskPaneList.add(databaseTaskPane);
        jXTaskPaneList.add(historyTaskPane);
        jXTaskPaneList.add(trackingTaskPane);


        this.addWindowListener(windowListener);

        this.setDefaultCloseOperation(MainWindow.DO_NOTHING_ON_CLOSE);

        this.addTaskPaneCollapsedPropertyListener(this);

        progressModel.addProgressListener(this);

        tracker.addListener(this);

        applicationStateModel.addApplicationStateListener(new ApplicationStateListener() {

            @Override
            public void maximize() {
                MainWindow.this.setVisible(true);
                MainWindow.this.setState(JFrame.NORMAL);
            }

            @Override
            public void minimize() {
                //throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void exiting() {
                //hrow new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public TraySupportEnum isTraySupported() {
                return TraySupportEnum.TRAY_DISABLED;
            }
        });



        applicationStateModel.addApplicationStateListener(new ApplicationStateListener() {

            @Override
            public void maximize() {
                windowListener.setEnabled(false);
                //MainWindow.this.setAlwaysOnTop(true); //no need under linux
                MainWindow.this.setVisible(true);
                MainWindow.this.setState(JFrame.NORMAL);
                //applicationStateModel.refresh();
                //MainWindow.this.doLayout(); //no need under linux
                //MainWindow.this.requestFocus(); //no need under linux
                windowListener.setEnabled(true);
            }

            @Override
            public void minimize() {

                //MainWindow.this.setAlwaysOnTop(false); /no need under linux
                windowListener.setEnabled(false);
                MainWindow.this.setVisible(false);
                //applicationStateModel.refresh();
                //MainWindow.this.doLayout(); //no need under linux
                windowListener.setEnabled(true);

            }

            @Override
            public void exiting() {
                //
            }

            @Override
            public TraySupportEnum isTraySupported() {
                return TraySupportEnum.TRAY_ENABLED;
            }
        });

        pack();


    }

    public Iterable<JXTaskPane> getJXTaskPanes() {
        return jXTaskPaneList;
    }

    @Override
    public void addTrackingListener(final TrackingListener l) {
        this.startMenuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                l.start();
            }
        });

        this.stopMenuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                l.stop();
            }
        });
    }

    @Override
    public void addAboutStartupListener(ActionListener actionListener) {
        aboutMenuItem.addActionListener(actionListener);
    }

    @Override
    public void addSettingsStartupListener(ActionListener actionListener) {
        settingsMenuItem.addActionListener(actionListener);
    }

    @Override
    public void addTaskPaneCollapsedPropertyListener(PropertyChangeListener propertyChangeListener) {

        for (JXTaskPane jXTaskPane : jXTaskPaneList) {
            jXTaskPane.addPropertyChangeListener("collapsed", propertyChangeListener);
        }

    }

    @Override
    public void setApplicationStateListener(final ApplicationStateListener listener) {

        this.exitMenuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                listener.exiting();
            }
        });

        windowListener.setApplicationStateListener(listener);



    }

    @Override
    public void containerChanged(Container container) {
        //
    }

    @Override
    public void start() {
        startMenuItem.setEnabled(false);
        stopMenuItem.setEnabled(true);
    }

    @Override
    public void stop() {
        startMenuItem.setEnabled(true);
        stopMenuItem.setEnabled(false);
    }

    @Override
    public void clear() {
        //
    }

    @Override
    public void timeChanged() {
        //
    }

    @Override
    public void ready() {
        stop();
    }

    @Override
    public void unready() {
        startMenuItem.setEnabled(false);
        stopMenuItem.setEnabled(false);
    }

    @Override
    public void commentChanged(String comment) {
        //
    }

    @Override
    public void summaryChanged(String summary) {
        //
    }

    @Override
    public void completeChanged(int complete) {
        //
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        MainWindow.this.pack();
    }

    public void addSubView(final Component c) {

        if (c instanceof SelectionPane) {
            selectionTaskPane.add(c);
        } else if (c instanceof DatabasePane) {
            databaseTaskPane.add(c);
        } else if (c instanceof TrackingPane) {
            trackingTaskPane.add(c);
        } else if (c instanceof HistoryPane) {
            historyTaskPane.add(c);
        }
        pack();


    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectionTaskPane = new org.jdesktop.swingx.JXTaskPane();
        trackingTaskPane = new org.jdesktop.swingx.JXTaskPane();
        databaseTaskPane = new org.jdesktop.swingx.JXTaskPane();
        historyTaskPane = new org.jdesktop.swingx.JXTaskPane();
        progressPanel = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        progressBar = new javax.swing.JProgressBar();
        progressLabel = new javax.swing.JLabel();
        mainMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        settingsMenuItem = new javax.swing.JMenuItem();
        trackingMenu = new javax.swing.JMenu();
        startMenuItem = new javax.swing.JMenuItem();
        stopMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(languageModel.getString("MainWindow.title")); // NOI18N
        setResizable(false);

        selectionTaskPane.setAnimated(false);
        selectionTaskPane.setFocusable(false);
        selectionTaskPane.setName("selectionTaskPane"); // NOI18N
        selectionTaskPane.setTitle(languageModel.getString("MainWindow.selectionTaskPane.text")); // NOI18N

        trackingTaskPane.setAnimated(false);
        trackingTaskPane.setFocusable(false);
        trackingTaskPane.setName("trackingTaskPane"); // NOI18N
        trackingTaskPane.setTitle(languageModel.getString("MainWindow.trackingTaskPane.text")); // NOI18N

        databaseTaskPane.setAnimated(false);
        databaseTaskPane.setFocusable(false);
        databaseTaskPane.setName("databaseTaskPane"); // NOI18N
        databaseTaskPane.setTitle(languageModel.getString("MainWindow.databaseTaskPane.text")); // NOI18N

        historyTaskPane.setAnimated(false);
        historyTaskPane.setFocusable(false);
        historyTaskPane.setName("historyTaskPane"); // NOI18N
        historyTaskPane.setTitle(languageModel.getString("MainWindow.historyTaskPane.text")); // NOI18N

        progressBar.setFont(progressBar.getFont().deriveFont(progressBar.getFont().getSize()-2f));
        progressBar.setStringPainted(true);

        progressLabel.setFont(progressLabel.getFont().deriveFont(progressLabel.getFont().getStyle() & ~java.awt.Font.BOLD, progressLabel.getFont().getSize()-3));
        progressLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        progressLabel.setText("progressing...");

        javax.swing.GroupLayout progressPanelLayout = new javax.swing.GroupLayout(progressPanel);
        progressPanel.setLayout(progressPanelLayout);
        progressPanelLayout.setHorizontalGroup(
            progressPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, progressPanelLayout.createSequentialGroup()
                .addComponent(progressLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
        );
        progressPanelLayout.setVerticalGroup(
            progressPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(progressPanelLayout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(progressPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(progressBar, 0, 0, Short.MAX_VALUE)
                    .addComponent(progressLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26))
        );

        fileMenu.setText(languageModel.getString("MainWindow.fileMenu.text")); // NOI18N
        fileMenu.setFont(fileMenu.getFont().deriveFont(fileMenu.getFont().getStyle() & ~java.awt.Font.BOLD));

        exitMenuItem.setFont(exitMenuItem.getFont().deriveFont(exitMenuItem.getFont().getStyle() & ~java.awt.Font.BOLD));
        exitMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/exit.png"))); // NOI18N
        exitMenuItem.setText(languageModel.getString("MainWindow.exitMenuItem.text")); // NOI18N
        fileMenu.add(exitMenuItem);

        mainMenuBar.add(fileMenu);

        editMenu.setText(languageModel.getString("MainWindow.editMenu.text")); // NOI18N
        editMenu.setFont(editMenu.getFont().deriveFont(editMenu.getFont().getStyle() & ~java.awt.Font.BOLD));

        settingsMenuItem.setFont(settingsMenuItem.getFont().deriveFont(settingsMenuItem.getFont().getStyle() & ~java.awt.Font.BOLD));
        settingsMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/configure.png"))); // NOI18N
        settingsMenuItem.setText(languageModel.getString("MainWindow.settingsMenuItem.text")); // NOI18N
        editMenu.add(settingsMenuItem);

        mainMenuBar.add(editMenu);

        trackingMenu.setText(languageModel.getString("MainWindow.trackingMenu.text")); // NOI18N
        trackingMenu.setFont(trackingMenu.getFont().deriveFont(trackingMenu.getFont().getStyle() & ~java.awt.Font.BOLD));

        startMenuItem.setFont(startMenuItem.getFont().deriveFont(startMenuItem.getFont().getStyle() & ~java.awt.Font.BOLD));
        startMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/work_small.png"))); // NOI18N
        startMenuItem.setText(languageModel.getString("MainWindow.startMenuItem.text")); // NOI18N
        trackingMenu.add(startMenuItem);

        stopMenuItem.setFont(stopMenuItem.getFont().deriveFont(stopMenuItem.getFont().getStyle() & ~java.awt.Font.BOLD));
        stopMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/stop_small.png"))); // NOI18N
        stopMenuItem.setText(languageModel.getString("MainWindow.stopMenuItem.text")); // NOI18N
        trackingMenu.add(stopMenuItem);

        mainMenuBar.add(trackingMenu);

        helpMenu.setText(languageModel.getString("MainWindow.helpMenu.text")); // NOI18N
        helpMenu.setFont(helpMenu.getFont().deriveFont(helpMenu.getFont().getStyle() & ~java.awt.Font.BOLD));

        aboutMenuItem.setFont(aboutMenuItem.getFont().deriveFont(aboutMenuItem.getFont().getStyle() & ~java.awt.Font.BOLD));
        aboutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/info.png"))); // NOI18N
        aboutMenuItem.setText(languageModel.getString("MainWindow.aboutMenuItem.text")); // NOI18N
        helpMenu.add(aboutMenuItem);

        mainMenuBar.add(helpMenu);

        setJMenuBar(mainMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(selectionTaskPane, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
            .addComponent(trackingTaskPane, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
            .addComponent(databaseTaskPane, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
            .addComponent(historyTaskPane, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
            .addComponent(progressPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(selectionTaskPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(trackingTaskPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(databaseTaskPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(historyTaskPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private org.jdesktop.swingx.JXTaskPane databaseTaskPane;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private org.jdesktop.swingx.JXTaskPane historyTaskPane;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JMenuBar mainMenuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel progressLabel;
    private javax.swing.JPanel progressPanel;
    private org.jdesktop.swingx.JXTaskPane selectionTaskPane;
    private javax.swing.JMenuItem settingsMenuItem;
    public javax.swing.JMenuItem startMenuItem;
    public javax.swing.JMenuItem stopMenuItem;
    private javax.swing.JMenu trackingMenu;
    private org.jdesktop.swingx.JXTaskPane trackingTaskPane;
    // End of variables declaration//GEN-END:variables

    @Override
    public void running(String text, int percent) {

        setProgressPaneVisible(true);

        progressBar.setValue(percent);
        progressLabel.setText(text);

        this.pack();
    }

    @Override
    public void done() {
        setProgressPaneVisible(false);
    }

    private void setProgressPaneVisible(boolean shouldBeVisible) {
        if (progressPanel.isVisible() == !shouldBeVisible) {
            progressPanel.setVisible(shouldBeVisible);
            this.pack();
        }
    }

    @Override
    public void updateLocale(LanguageModel languageModel) {
        setTitle(languageModel.getString("MainWindow.title"));
        selectionTaskPane.setTitle(languageModel.getString("MainWindow.selectionTaskPane.text"));
        trackingTaskPane.setTitle(languageModel.getString("MainWindow.trackingTaskPane.text"));
        databaseTaskPane.setTitle(languageModel.getString("MainWindow.databaseTaskPane.text"));
        historyTaskPane.setTitle(languageModel.getString("MainWindow.historyTaskPane.text"));
        fileMenu.setText(languageModel.getString("MainWindow.fileMenu.text"));
        exitMenuItem.setText(languageModel.getString("MainWindow.exitMenuItem.text"));
        editMenu.setText(languageModel.getString("MainWindow.editMenu.text"));
        settingsMenuItem.setText(languageModel.getString("MainWindow.settingsMenuItem.text"));
        trackingMenu.setText(languageModel.getString("MainWindow.trackingMenu.text"));
        startMenuItem.setText(languageModel.getString("MainWindow.startMenuItem.text"));
        stopMenuItem.setText(languageModel.getString("MainWindow.stopMenuItem.text"));
        helpMenu.setText(languageModel.getString("MainWindow.helpMenu.text"));
        aboutMenuItem.setText(languageModel.getString("MainWindow.aboutMenuItem.text"));
        this.pack();
    }
}
