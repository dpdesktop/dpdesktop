/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import net.sf.dpdesktop.module.guistate.ApplicationStateListener;
import net.sf.dpdesktop.module.guistate.TraySupportEnum;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class MainWindowListener implements WindowListener {

    private ApplicationStateListener stateComponentListener;
    private boolean enabled = false;

    public void setApplicationStateListener(ApplicationStateListener applicationStateListener) {
        this.stateComponentListener = applicationStateListener;

        this.setEnabled(applicationStateListener != null);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {

        Logger.getLogger(this.getClass()).debug("Is MainWindowListener enabled? " + enabled);


        return enabled;
    }

    public void windowClosing(WindowEvent e) {
        Logger.getLogger(this.getClass()).debug("Window Closing");
        if (isEnabled()) {
            if (stateComponentListener.isTraySupported() == TraySupportEnum.TRAY_ENABLED) {
                stateComponentListener.minimize();
            } else if (stateComponentListener.isTraySupported() == TraySupportEnum.TRAY_DISABLED) {
                stateComponentListener.exiting();
            }
        }
    }

    public void windowIconified(WindowEvent e) {
        Logger.getLogger(this.getClass()).debug("Window Iconified");
        if (isEnabled()) {
            if (stateComponentListener.isTraySupported() == TraySupportEnum.TRAY_ENABLED) {
                stateComponentListener.minimize();
            }
        }
    }

    public void windowDeiconified(WindowEvent e) {
        Logger.getLogger(this.getClass()).debug("Window Deiconified");

        if (isEnabled()) {
            if (stateComponentListener.isTraySupported() == TraySupportEnum.TRAY_ENABLED) {
                stateComponentListener.maximize();
            }
        }
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
        Logger.getLogger(this.getClass()).debug("Window deactivated");

        Logger.getLogger(this.getClass()).debug(e);
        
        /*
         * This is not good for windows :(
         *

        if (isEnabled()) {

        if(e.getOppositeWindow()==null) {
        stateComponentListener.minimize();
        }

        }
         */
    }
}


