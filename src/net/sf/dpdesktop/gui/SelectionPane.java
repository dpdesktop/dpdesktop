/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui;

import com.google.inject.Inject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import net.sf.dpdesktop.gui.util.SearchingDocumentListener;
import net.sf.dpdesktop.gui.util.ContainerCellRenderer;
import net.sf.dpdesktop.gui.util.StringHelper;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.module.tracking.TrackingComponent;
import net.sf.dpdesktop.module.tracking.TrackingController;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.container.ContainerRepositoryListener;
import net.sf.dpdesktop.service.container.ContainerService;

/**
 *
 * @author Heiner Reinhardt
 */
public class SelectionPane extends javax.swing.JPanel implements TrackingComponent, TrackingListener, LocaleListener {

    private final ContainerCellRenderer m_containerCellRenderer;
    private Document searchDocument;
    private List<TrackingListener> trackingListenerList = new LinkedList<TrackingListener>();
    private ActionListener comboBoxActionListener = new ActionListener() {

        public void actionPerformed(ActionEvent arg0) {

            if (comboBox.getSelectedItem() instanceof Container) {

                Container container = (Container) comboBox.getSelectedItem();

                for (TrackingListener trackingListener : trackingListenerList) {
                    trackingListener.containerChanged(container);
                }

                comboBox.removeActionListener(this);
                if (m_tracker.getTrackableContainer() == null) {
                    comboBox.setSelectedIndex(0);
                } else {
                    comboBox.setSelectedItem(m_tracker.getTrackableContainer());
                }
                comboBox.addActionListener(this);

            }

        }
    };
    private final SearchingDocumentListener searchDocumentListener;
    private final Tracker m_tracker;
    private final LanguageModel languageModel;

    /** Creates new form SelectionPane */
    @Inject
    public SelectionPane(Tracker tracker, ContainerRepository containerRepository, LanguageModel languageModel, ContainerCellRenderer containerCellRenderer) {

        this.languageModel = languageModel;
        initComponents();
        languageModel.addLocaleListener(this);
        this.m_tracker = tracker;
        this.m_containerCellRenderer = containerCellRenderer;

        searchDocumentListener = new SearchingDocumentListener(
                comboBox,
                searchTextField,
                containerCellRenderer,
                comboBoxActionListener,
                containerRepository,
                this);

        searchDocument = searchTextField.getDocument();

        initComboBox();

        Container c = tracker.getTrackableContainer();
        if (c != null) {
            comboBox.setSelectedItem(tracker.getTrackableContainer());
        }
        tracker.addListener(this);

        containerRepository.addContainerRepositoryListener(new ContainerRepositoryListener() {

            @Override
            public void rootContainerChanged(Container container) {
                applyContainer(container);
            }
        });


    }

    private void initComboBox() {

        comboBox.setRenderer(m_containerCellRenderer);
        comboBox.addActionListener(comboBoxActionListener);

        searchDocument.removeDocumentListener(searchDocumentListener);
        searchTextField.setText("");
        searchDocument.addDocumentListener(searchDocumentListener);
        m_containerCellRenderer.setText("");

    }

    @Override
    public void addTrackingListener(TrackingListener trackingListener) {
        trackingListenerList.add(trackingListener);
    }

    public void applyContainer(Container container) {
        comboBox.removeAllItems();

        traverseContainer(container);

    }

    private void traverseContainer(Container container) {
        for (Container child : container.getChildren()) {
            comboBox.addItem(child);
            traverseContainer(child);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        searchLabel = new javax.swing.JLabel();
        searchTextField = new javax.swing.JTextField();
        comboBox = new javax.swing.JComboBox();

        searchLabel.setFont(searchLabel.getFont().deriveFont(searchLabel.getFont().getSize()-1f));
        searchLabel.setText(languageModel.getString("SelectionPane.searchLabel.text")); // NOI18N

        comboBox.setFont(comboBox.getFont().deriveFont(comboBox.getFont().getStyle() & ~java.awt.Font.BOLD, comboBox.getFont().getSize()-1));
        comboBox.setMaximumRowCount(20);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(searchLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE))
            .addComponent(comboBox, 0, 353, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchLabel)
                    .addComponent(searchTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JComboBox comboBox;
    private javax.swing.JLabel searchLabel;
    private javax.swing.JTextField searchTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void containerChanged(Container container) {
        comboBox.removeActionListener(comboBoxActionListener);
        comboBox.setSelectedItem(container);
        comboBox.addActionListener(comboBoxActionListener);
    }

    @Override
    public void start() {
        //
    }

    @Override
    public void stop() {
        //
    }

    @Override
    public void clear() {
        //
    }

    @Override
    public void timeChanged() {
        //
    }

    @Override
    public void ready() {
        //
    }

    @Override
    public void unready() {
        comboBox.removeActionListener(comboBoxActionListener);
        try {
            comboBox.setSelectedIndex(0);
        } catch (IllegalArgumentException ex) {
            // don't care
            }
        comboBox.addActionListener(comboBoxActionListener);
    }

    @Override
    public void commentChanged(String comment) {
        //
    }

    @Override
    public void summaryChanged(String summary) {
        //
    }

    @Override
    public void completeChanged(int complete) {
        //
    }

    @Override
    public void updateLocale(LanguageModel languageModel) {
        searchLabel.setText(languageModel.getString("SelectionPane.searchLabel.text"));
    }
}
