/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
/*
 * Created on 23.10.2009, 17:11:36
 */
package net.sf.dpdesktop.gui;

import net.sf.dpdesktop.module.settings.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.sf.dpdesktop.gui.util.LocaleListCellRenderer;
import net.sf.dpdesktop.gui.util.MinuteSpinnerModel;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.SettingsChangedListener;
import net.sf.dpdesktop.module.settings.SettingsComponent;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.service.config.ConfigurationListener;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.module.remote.RemotePlaceRepository;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.remote.SelectionListener;
import net.sf.dpdesktop.module.tracking.IdleController;
import org.jdesktop.swingx.autocomplete.ComboBoxAdaptor;

/**
 *
 * @author Heiner Reinhardt
 */
public class SettingsDialog extends javax.swing.JDialog implements SettingsComponent, LocaleListener {

    private final RemotePlaceRepository m_remotePlaceRepository;
    boolean enableDocumentListener = true;
    private final LanguageModel languageModel;

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);

        if(visible) {
            apply(); 
        }
    }

    @Override
    public void updateLocale(LanguageModel languageModel) {
        configurationPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(languageModel.getString("SettingsDialog.configurationPanel.border.title"))); // NOI18N
        remotePlacesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(languageModel.getString("SettingsDialog.remotePlacesPanel.border.title"))); // NOI18N

        cancelButton.setText(languageModel.getString("SettingsDialog.cancelButton.text"));
        saveButton.setText(languageModel.getString("SettingsDialog.saveButton.text"));
        versionNotifyCheckBox.setText(languageModel.getString("SettingsDialog.versionNotifyCheckBox.text"));
        autoUpdateCheckBox.setText(languageModel.getString("SettingsDialog.autoUpdateCheckBox.text"));
        timeUntilIdleLabel.setText(languageModel.getString("SettingsDialog.timeUntilIdleLabel.text"));
        languageLabel.setText(languageModel.getString("SettingsDialog.languageLabel.text"));
        malformedUrlLabel.setText(languageModel.getString("SettingsDialog.malformedUrlLabel.text"));
        companyLabel.setText(languageModel.getString("SettingsDialog.companyLabel.text"));
        urlLabel.setText(languageModel.getString("SettingsDialog.urlLabel.text"));
        usernameLabel.setText(languageModel.getString("SettingsDialog.usernameLabel.text"));
        passwordLabel.setText(languageModel.getString("SettingsDialog.passwordLabel.text"));

        this.pack();

    }

    private abstract class MyDocumentListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            if (enableDocumentListener) {
                enableDocumentListener = false;
                this.changedUpdate(e);
                enableDocumentListener = true;
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.insertUpdate(e);
        }

        @Override
        public abstract void changedUpdate(DocumentEvent e);
    }
    private List<SettingsChangedListener> listenerList = new LinkedList<SettingsChangedListener>();

    @Inject
    /** Creates new form SettingsDialog */
    public SettingsDialog(java.awt.Frame parent,
            @Named("settings") final ConfigurationService configuration,
            @Named("clone") RemotePlaceRepository remotePlaceRepository,
            LanguageModel languageModel,
            @Named("isAutoIdleSupported") final Boolean isAutoIdleSupported) {

        super(parent, true);

        this.setDefaultCloseOperation(SettingsDialog.DO_NOTHING_ON_CLOSE);

        this.languageModel = languageModel;
        initComponents();
        languageModel.addLocaleListener(this);

        malformedUrlLabel.setVisible(false);

        idleTimeSpinner.setModel(new MinuteSpinnerModel(idleTimeSpinner, languageModel));

        languageComboBox.setRenderer(new LocaleListCellRenderer());
        languageComboBox.setModel(languageModel);

        this.m_remotePlaceRepository = remotePlaceRepository;

        companyTextField.getDocument().addDocumentListener(new MyDocumentListener() {

            @Override
            public void changedUpdate(DocumentEvent e) {
                for (SettingsChangedListener settingsChangedListener : listenerList) {
                    settingsChangedListener.changeRemotePlaceCompany(getSelectedItem(), companyTextField.getText());
                }
            }
        });

        urlTextField.getDocument().addDocumentListener(new MyDocumentListener() {

            @Override
            public void changedUpdate(DocumentEvent e) {
                for (SettingsChangedListener settingsChangedListener : listenerList) {
                    boolean wellformedURL = settingsChangedListener.changeRemotePlaceUrl(getSelectedItem(), urlTextField.getText());

                    malformedUrlLabel.setVisible(!wellformedURL);

                }
            }
        });

        usernameTextField.getDocument().addDocumentListener(new MyDocumentListener() {

            @Override
            public void changedUpdate(DocumentEvent e) {
                for (SettingsChangedListener settingsChangedListener : listenerList) {
                    settingsChangedListener.changeRemotePlaceUser(getSelectedItem(), usernameTextField.getText());
                }
            }
        });

        passwordField.getDocument().addDocumentListener(new MyDocumentListener() {

            @Override
            public void changedUpdate(DocumentEvent e) {
                for (SettingsChangedListener settingsChangedListener : listenerList) {
                    settingsChangedListener.changeRemotePlacePass(getSelectedItem(), new String(passwordField.getPassword()));
                }
            }
        });

        configuration.addUniqueConfigurationListener(
                new ConfigurationListener("maxIdleTime") {

                    @Override
                    public void valueChanged(String value) {
                        if(isAutoIdleSupported) {
                            idleTimeSpinner.setValue(value);
                        }
                    }

                    @Override
                    public void flagChanged(boolean b) {
                        //ignore
                    }
                });

        configuration.addUniqueConfigurationListener(
                new ConfigurationListener("notifyOnNewVersionAvailable") {

                    @Override
                    public void valueChanged(String value) {
                        //ignore
                    }

                    @Override
                    public void flagChanged(
                            boolean b) {
                        versionNotifyCheckBox.setSelected(b);
                    }
                });

        configuration.addUniqueConfigurationListener(
                new ConfigurationListener("autoUpdate") {

                    @Override
                    public void valueChanged(String value) {
                        //ignore
                    }

                    @Override
                    public void flagChanged(
                            boolean b) {
                        autoUpdateCheckBox.setSelected(b);
                    }
                });

        remotePlaceList.setModel(remotePlaceRepository);

        remotePlaceList.addListSelectionListener(
                new ListSelectionListener() {

                    @Override
                    public void valueChanged(ListSelectionEvent e) {

                        apply();

                    }
                });

        remotePlaceRepository.addSelectionListener(
                new SelectionListener() {

                    @Override
                    public void selected(RemotePlace remotePlace, int index) {

                        remotePlaceList.setSelectedValue(remotePlace, true);

                    }
                });

        if (!isAutoIdleSupported) {
            idleTimeSpinner.setValue(0);
            idleTimeSpinner.setEnabled(false);
        }

    }

    @Override
    public void addSettingsChangedListener(final SettingsChangedListener settingsChangedListener) {

        listenerList.add(settingsChangedListener);


        saveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                settingsChangedListener.settingsChanged(autoUpdateCheckBox.isSelected(),
                        versionNotifyCheckBox.isSelected(),
                        (Integer) idleTimeSpinner.getValue(),
                        (Locale) languageComboBox.getSelectedItem());
            }
        });

        cancelButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {

                settingsChangedListener.cancel();
                remotePlaceList.setSelectedIndex(0);
                apply();

            }
        });

        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosed(WindowEvent e) {
                settingsChangedListener.cancel();
                remotePlaceList.setSelectedIndex(0);
                apply();
            }
        });

        addButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                settingsChangedListener.addRemotePlace();
            }
        });

        removeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int index = remotePlaceList.getSelectedIndex();
                boolean x = (index == m_remotePlaceRepository.getSize() - 1);

                settingsChangedListener.removeRemotePlace(getSelectedItem());

                if (x) {
                    remotePlaceList.setSelectedIndex(index - 1);
                } else {
                    remotePlaceList.setSelectedIndex(index);
                }

            }
        });
    }

    private RemotePlaceServiceProvider getSelectedItem() {
        return (RemotePlaceServiceProvider) remotePlaceList.getSelectedValue();
    }

    private void apply() {
        RemotePlace remotePlace = (RemotePlace) remotePlaceList.getSelectedValue();

        if (remotePlace != null) {

            this.enableDocumentListener = false;

            companyTextField.setText(remotePlace.getCompany());
            urlTextField.setText(remotePlace.getURLAsString());
            usernameTextField.setText(remotePlace.getUsername());
            passwordField.setText(remotePlace.getPassword());

            this.enableDocumentListener = true;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cancelButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        configurationPanel = new javax.swing.JPanel();
        versionNotifyCheckBox = new javax.swing.JCheckBox();
        autoUpdateCheckBox = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        idleTimeSpinner = new javax.swing.JSpinner();
        languageComboBox = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        timeUntilIdleLabel = new javax.swing.JLabel();
        languageLabel = new javax.swing.JLabel();
        remotePlacesPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        companyTextField = new javax.swing.JTextField();
        urlTextField = new javax.swing.JTextField();
        malformedUrlLabel = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        companyLabel = new javax.swing.JLabel();
        urlLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        remotePlaceList = new javax.swing.JList();
        addButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        usernameLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        usernameTextField = new javax.swing.JTextField();
        passwordField = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(languageModel.getString("SettingsDialog.text")); // NOI18N

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/cancel.png"))); // NOI18N
        cancelButton.setText(languageModel.getString("SettingsDialog.cancelButton.text")); // NOI18N

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/ok.png"))); // NOI18N
        saveButton.setText(languageModel.getString("SettingsDialog.saveButton.text")); // NOI18N

        configurationPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(languageModel.getString("SettingsDialog.configurationPanel.border.title"))); // NOI18N

        versionNotifyCheckBox.setFont(versionNotifyCheckBox.getFont().deriveFont(versionNotifyCheckBox.getFont().getStyle() & ~java.awt.Font.BOLD));
        versionNotifyCheckBox.setText(languageModel.getString("SettingsDialog.versionNotifyCheckBox.text")); // NOI18N

        autoUpdateCheckBox.setFont(autoUpdateCheckBox.getFont().deriveFont(autoUpdateCheckBox.getFont().getStyle() & ~java.awt.Font.BOLD));
        autoUpdateCheckBox.setText(languageModel.getString("SettingsDialog.autoUpdateCheckBox.text")); // NOI18N

        idleTimeSpinner.setPreferredSize(new java.awt.Dimension(28, 19));

        languageComboBox.setFont(languageComboBox.getFont().deriveFont(languageComboBox.getFont().getStyle() & ~java.awt.Font.BOLD));
        languageComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        languageComboBox.setMinimumSize(new java.awt.Dimension(68, 15));
        languageComboBox.setPreferredSize(new java.awt.Dimension(68, 19));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(languageComboBox, 0, 190, Short.MAX_VALUE)
            .addComponent(idleTimeSpinner, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(idleTimeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(languageComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        timeUntilIdleLabel.setFont(timeUntilIdleLabel.getFont().deriveFont(timeUntilIdleLabel.getFont().getStyle() | java.awt.Font.BOLD));
        timeUntilIdleLabel.setText(languageModel.getString("SettingsDialog.timeUntilIdleLabel.text")); // NOI18N
        timeUntilIdleLabel.setPreferredSize(new java.awt.Dimension(154, 19));

        languageLabel.setFont(languageLabel.getFont().deriveFont(languageLabel.getFont().getStyle() | java.awt.Font.BOLD));
        languageLabel.setText(languageModel.getString("SettingsDialog.languageLabel.text")); // NOI18N
        languageLabel.setPreferredSize(new java.awt.Dimension(60, 19));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(languageLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(timeUntilIdleLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(timeUntilIdleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(languageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout configurationPanelLayout = new javax.swing.GroupLayout(configurationPanel);
        configurationPanel.setLayout(configurationPanelLayout);
        configurationPanelLayout.setHorizontalGroup(
            configurationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(configurationPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(configurationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(autoUpdateCheckBox)
                    .addGroup(configurationPanelLayout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(versionNotifyCheckBox))
                .addContainerGap(338, Short.MAX_VALUE))
        );
        configurationPanelLayout.setVerticalGroup(
            configurationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, configurationPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(autoUpdateCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(versionNotifyCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(configurationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        remotePlacesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(languageModel.getString("SettingsDialog.remotePlacesPanel.border.title"))); // NOI18N

        companyTextField.setText("jTextField1");

        urlTextField.setText("jTextField2");

        malformedUrlLabel.setFont(malformedUrlLabel.getFont().deriveFont(malformedUrlLabel.getFont().getStyle() & ~java.awt.Font.BOLD, malformedUrlLabel.getFont().getSize()-3));
        malformedUrlLabel.setForeground(new java.awt.Color(196, 45, 45));
        malformedUrlLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        malformedUrlLabel.setText(languageModel.getString("SettingsDialog.malformedUrlLabel.text")); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(urlTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)
            .addComponent(companyTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(malformedUrlLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(companyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(urlTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(malformedUrlLabel)
                .addContainerGap())
        );

        companyLabel.setText(languageModel.getString("SettingsDialog.companyLabel.text")); // NOI18N
        companyLabel.setPreferredSize(new java.awt.Dimension(59, 21));

        urlLabel.setText(languageModel.getString("SettingsDialog.urlLabel.text")); // NOI18N
        urlLabel.setPreferredSize(new java.awt.Dimension(24, 21));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(companyLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(urlLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(52, 52, 52))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(companyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(urlLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        remotePlaceList.setFont(remotePlaceList.getFont().deriveFont(remotePlaceList.getFont().getStyle() & ~java.awt.Font.BOLD));
        remotePlaceList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(remotePlaceList);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/list_add.png"))); // NOI18N

        removeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/list_remove.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(removeButton, 0, 0, Short.MAX_VALUE)
                    .addComponent(addButton)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(addButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(removeButton)
                .addContainerGap(181, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
        );

        usernameLabel.setText(languageModel.getString("SettingsDialog.usernameLabel.text")); // NOI18N
        usernameLabel.setPreferredSize(new java.awt.Dimension(62, 21));

        passwordLabel.setText(languageModel.getString("SettingsDialog.passwordLabel.text")); // NOI18N
        passwordLabel.setPreferredSize(new java.awt.Dimension(60, 21));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(usernameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(passwordLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(2, 2, 2)))
                .addGap(26, 26, 26))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(usernameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(passwordLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
        );

        usernameTextField.setText("jTextField3");

        passwordField.setText("jPasswordField1");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(usernameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(246, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(usernameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout remotePlacesPanelLayout = new javax.swing.GroupLayout(remotePlacesPanel);
        remotePlacesPanel.setLayout(remotePlacesPanelLayout);
        remotePlacesPanelLayout.setHorizontalGroup(
            remotePlacesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(remotePlacesPanelLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(remotePlacesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(remotePlacesPanelLayout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(14, 14, 14))
                    .addGroup(remotePlacesPanelLayout.createSequentialGroup()
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(2, 2, 2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(remotePlacesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        remotePlacesPanelLayout.setVerticalGroup(
            remotePlacesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(remotePlacesPanelLayout.createSequentialGroup()
                .addGroup(remotePlacesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(remotePlacesPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, remotePlacesPanelLayout.createSequentialGroup()
                        .addGroup(remotePlacesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, remotePlacesPanelLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(remotePlacesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(remotePlacesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(configurationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(527, 527, 527)
                .addComponent(saveButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(remotePlacesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(configurationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JCheckBox autoUpdateCheckBox;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel companyLabel;
    private javax.swing.JTextField companyTextField;
    private javax.swing.JPanel configurationPanel;
    private javax.swing.JSpinner idleTimeSpinner;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox languageComboBox;
    private javax.swing.JLabel languageLabel;
    private javax.swing.JLabel malformedUrlLabel;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JList remotePlaceList;
    private javax.swing.JPanel remotePlacesPanel;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JLabel timeUntilIdleLabel;
    private javax.swing.JLabel urlLabel;
    private javax.swing.JTextField urlTextField;
    private javax.swing.JLabel usernameLabel;
    private javax.swing.JTextField usernameTextField;
    private javax.swing.JCheckBox versionNotifyCheckBox;
    // End of variables declaration//GEN-END:variables
}
