/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui;

import com.google.inject.Inject;
import java.awt.Color;
import java.util.ResourceBundle;
import javax.swing.event.DocumentEvent;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.DocumentListener;
import net.sf.dpdesktop.gui.util.Helper;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;
import net.sf.dpdesktop.module.submit.SubmitComponent;
import net.sf.dpdesktop.module.submit.SubmitListener;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.module.tracking.TrackingComponent;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.module.tracking.TrackingManager;

/**
 *
 * @author Heiner Reinhardt
 */
public class TrackingPane extends javax.swing.JPanel implements TrackingComponent, SubmitComponent, LocaleListener {

    private Tracker m_tracker;
    boolean documentListenerEnabled = true;
    private final LanguageModel languageModel;

    /** Creates new form TrackingPane */
    @Inject
    public TrackingPane(Tracker tracker, LanguageModel myLanguageModel) {
        this.languageModel = myLanguageModel;
        initComponents();
        this.m_tracker = tracker;

        languageModel.addLocaleListener(this);



        this.m_tracker.addListener(new TrackingListener() {

            public void start() {
                clearButton.setEnabled(true);
                startButton.setEnabled(false);
                stopButton.setEnabled(true);

                statusValueLabel.setForeground(Color.GREEN);
                statusValueLabel.setText(languageModel.getString("TrackingPane.statusValueLabel.working.text"));
                Helper.setTextAreaEnabled(commentTextArea, true);

            }

            public void stop() {
                clearButton.setEnabled(true);
                startButton.setEnabled(true);
                stopButton.setEnabled(false);

                statusValueLabel.setForeground(Color.RED);
                statusValueLabel.setText(languageModel.getString("TrackingPane.statusValueLabel.stopped.text"));

            }

            public void clear() {
                clearButton.setEnabled(false);
            }

            public void timeChanged() {

                timeValueLabel.setText(m_tracker.getTimeAsString());
            }

            public void ready() {
                clearButton.setEnabled(false);
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
                submitButton.setEnabled(true);
                Helper.setTextAreaEnabled(commentTextArea, true);
            }

            public void unready() {
                clearButton.setEnabled(false);
                startButton.setEnabled(false);
                stopButton.setEnabled(false);

                statusValueLabel.setForeground(Color.RED);
                statusValueLabel.setText(languageModel.getString("TrackingPane.statusValueLabel.stopped.text"));

                Helper.setTextAreaEnabled(commentTextArea, false);

                submitButton.setEnabled(false);

                containerLabel.setText("");
            }

            public void containerChanged(Container container) {
                containerLabel.setText(container.getName());
            }

            @Override
            public void commentChanged(String comment) {
                try {
                    commentTextArea.setText(comment);
                } catch (IllegalStateException ex) {
                    /**
                     * An IllegalStateException is thrown when changing text inside
                     * a document event handlement
                     *
                     * It can be ignored!
                     */
                }
            }

            @Override
            public void summaryChanged(String summary) {
                //
            }

            @Override
            public void completeChanged(int complete) {
                //
            }
        });

    }

    @Override
    public void addTrackingListener(final TrackingListener trackingListener) {
        startButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                trackingListener.start();
            }
        });

        stopButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                trackingListener.stop();
            }
        });

        clearButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                trackingListener.clear();
            }
        });

        commentTextArea.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                trackingListener.commentChanged(commentTextArea.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                //insertUpdate(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                //
            }
        });
    }

    @Override
    public void addSubmitListener(final SubmitListener submitListener) {
        submitButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                submitListener.submit();
            }
        });
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        objectiveLabel = new javax.swing.JLabel();
        statusLabel = new javax.swing.JLabel();
        statusValueLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        timeValueLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        containerLabel = new org.jdesktop.swingx.JXLabel();
        jPanel2 = new javax.swing.JPanel();
        startButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        submitButton = new javax.swing.JButton();
        clearButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        commentLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        commentTextArea = new javax.swing.JTextArea();

        objectiveLabel.setFont(objectiveLabel.getFont().deriveFont(objectiveLabel.getFont().getSize()-1f));
        objectiveLabel.setText(languageModel.getString("TrackingPane.objectiveLabel.text")); // NOI18N

        statusLabel.setFont(statusLabel.getFont().deriveFont(statusLabel.getFont().getSize()-1f));
        statusLabel.setText(languageModel.getString("TrackingPane.statusLabel.text")); // NOI18N

        statusValueLabel.setFont(statusValueLabel.getFont().deriveFont(statusValueLabel.getFont().getStyle() & ~java.awt.Font.BOLD, statusValueLabel.getFont().getSize()-1));
        statusValueLabel.setText("...");

        timeLabel.setFont(timeLabel.getFont().deriveFont(timeLabel.getFont().getSize()-1f));
        timeLabel.setText(languageModel.getString("TrackingPane.timeLabel.text")); // NOI18N

        timeValueLabel.setFont(timeValueLabel.getFont().deriveFont(timeValueLabel.getFont().getStyle() & ~java.awt.Font.BOLD, timeValueLabel.getFont().getSize()-1));
        timeValueLabel.setText("00:00:00");

        containerLabel.setText("...");
        containerLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        containerLabel.setFont(containerLabel.getFont().deriveFont(containerLabel.getFont().getStyle() & ~java.awt.Font.BOLD, containerLabel.getFont().getSize()-1));
        containerLabel.setLineWrap(true);
        jScrollPane1.setViewportView(containerLabel);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(objectiveLabel)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(statusLabel)
                                .addComponent(timeLabel))
                            .addGap(39, 39, 39)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(timeValueLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(statusValueLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(objectiveLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusLabel)
                    .addComponent(statusValueLabel))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(timeLabel)
                    .addComponent(timeValueLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/work.png"))); // NOI18N
        startButton.setBorderPainted(false);
        startButton.setContentAreaFilled(false);
        startButton.setFocusPainted(false);

        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/stop.png"))); // NOI18N
        stopButton.setBorderPainted(false);
        stopButton.setContentAreaFilled(false);
        stopButton.setFocusPainted(false);

        submitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/db_commit.png"))); // NOI18N
        submitButton.setText(languageModel.getString("TrackingPane.submitButton.text")); // NOI18N

        clearButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/clear.png"))); // NOI18N
        clearButton.setBorderPainted(false);
        clearButton.setContentAreaFilled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addComponent(clearButton, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stopButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(submitButton, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 36, Short.MAX_VALUE)
                    .addComponent(stopButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 36, Short.MAX_VALUE)
                    .addComponent(clearButton)
                    .addComponent(submitButton, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE))
                .addGap(12, 12, 12))
        );

        commentLabel.setFont(commentLabel.getFont().deriveFont(commentLabel.getFont().getSize()-1f));
        commentLabel.setText(languageModel.getString("TrackingPane.commentLabel.text")); // NOI18N

        commentTextArea.setColumns(20);
        commentTextArea.setLineWrap(true);
        commentTextArea.setRows(5);
        commentTextArea.setWrapStyleWord(true);
        jScrollPane2.setViewportView(commentTextArea);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(commentLabel)
                .addContainerGap(233, Short.MAX_VALUE))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(commentLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton clearButton;
    private javax.swing.JLabel commentLabel;
    public javax.swing.JTextArea commentTextArea;
    private org.jdesktop.swingx.JXLabel containerLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel objectiveLabel;
    public javax.swing.JButton startButton;
    private javax.swing.JLabel statusLabel;
    public javax.swing.JLabel statusValueLabel;
    public javax.swing.JButton stopButton;
    private javax.swing.JButton submitButton;
    private javax.swing.JLabel timeLabel;
    public javax.swing.JLabel timeValueLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void updateLocale(LanguageModel languageModel) {
        objectiveLabel.setText(languageModel.getString("TrackingPane.objectiveLabel.text"));
        statusLabel.setText(languageModel.getString("TrackingPane.statusLabel.text"));
        timeLabel.setText(languageModel.getString("TrackingPane.timeLabel.text"));
        submitButton.setText(languageModel.getString("TrackingPane.submitButton.text"));
        commentLabel.setText(languageModel.getString("TrackingPane.commentLabel.text"));

        if(m_tracker.isRunning()) {
            statusValueLabel.setText(languageModel.getString("TrackingPane.statusValueLabel.working.text"));
        } else {
            statusValueLabel.setText(languageModel.getString("TrackingPane.statusValueLabel.stopped.text"));
        }

    }
}
