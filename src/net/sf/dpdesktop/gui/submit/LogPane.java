/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui.submit;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.util.ResourceBundle;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.sf.dpdesktop.gui.util.TimeSpinnerModel;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.service.log.LogItem;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.service.config.ConfigurationListener;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class LogPane extends javax.swing.JPanel implements  LocaleListener{

    private TimeSpinnerModel workedModel;
    private TimeSpinnerModel billableModel;
    private final LanguageModel languageModel;

    /** Creates new form LogPane */
    @Inject
    public LogPane(final Tracker tracker, LanguageModel languageModel) {
        this.languageModel = languageModel;
        initComponents();
        this.billableTimeSpinner.setVisible(false);

        languageModel.addLocaleListener(this);

        workedModel = new TimeSpinnerModel(workedTimeSpinner);
        billableModel = new TimeSpinnerModel(billableTimeSpinner);

        workedTimeSpinner.setModel(workedModel);

        tracker.addListener(new TrackingListener() {

            @Override
            public void containerChanged(Container container) {

                String str = container.getName();

                if (str.length() > 80) {
                    str = str.substring(0, 77) + "...";
                }


                objectLabel.setText(str);

                completeSpinner.setValue(Integer.valueOf(container.get("complete", "0")));

                try {
                    Configuration c = ((RemotePlaceServiceProvider) container.getCreator().getRemotePlace()).getRemoteConfigurationService();

                    LogPane.this.addConfigurationListener(c);

                } catch (ClassCastException ex) {
                    Logger.getLogger(this.getClass()).fatal("Hardcoded Error", ex);
                }


            }

            @Override
            public void start() {
            }

            @Override
            public void stop() {
            }

            @Override
            public void clear() {
            }

            @Override
            public void timeChanged() {
                workedModel.setValue(tracker.getHours(), tracker.getMinutes());
            }

            @Override
            public void ready() {
            }

            @Override
            public void unready() {
                summaryTextField.setText("");
                commentTextArea.setText("");
                workedModel.clear();
                billableModel.clear();
                objectLabel.setText("...");

                repaint();
            }

            @Override
            public void commentChanged(String comment) {
                commentTextArea.setText(comment);
            }

            @Override
            public void summaryChanged(String summary) {
                summaryTextField.setText(summary);
            }

            @Override
            public void completeChanged(int complete) {
                completeSpinner.setValue(complete);
            }
        });

    }
    private ChangeListener changeListener = new ChangeListener() {

        public void stateChanged(ChangeEvent e) {
            billableModel.setValue(workedModel.getHours(), workedModel.getMinutes());
        }
    };

    private void addConfigurationListener(Configuration remoteConfiguration) {
        remoteConfiguration.addUniqueConfigurationListener(new ConfigurationListener("timeStepSize") {

            @Override
            public void valueChanged(String value) {

                int size = Integer.valueOf(value);

                billableModel.setStepSize(size);
                workedModel.setStepSize(size);

            }

            @Override
            public void flagChanged(boolean b) {
                //
            }
        });

        remoteConfiguration.addUniqueConfigurationListener(new ConfigurationListener("enableBillableTime") {

            @Override
            public void valueChanged(String value) {
            }

            @Override
            public void flagChanged(boolean enable) {

                workedTimeSpinner.removeChangeListener(changeListener);

                if (enable) {
                    billableTimeSpinner.setModel(billableModel);
                    workedTimeSpinner.addChangeListener(changeListener);
                }

                billableTimeSpinner.setVisible(enable);
                billableTimeLabel.setVisible(enable);

            }
        });
    }

    public String getSummary() {
        return summaryTextField.getText();
    }

    public String getComment() {
        return commentTextArea.getText();
    }

    public int getComplete() {
        return (Integer) completeSpinner.getValue();
    }

    public int getWorkedTimeHours() {
        return workedModel.getHours();
    }

    public int getWorkedTimeMinutes() {
        return workedModel.getMinutes();
    }

    public int getBillableTimeHours() {
        return billableModel.getHours();
    }

    public int getBillableTimeMinutes() {
        return billableModel.getMinutes();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        annotationLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        objectiveLabel = new javax.swing.JLabel();
        summaryLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        workedTimeLabel = new javax.swing.JLabel();
        billableTimeLabel = new javax.swing.JLabel();
        currentStatusLabel = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        objectLabel = new javax.swing.JLabel();
        summaryTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        commentTextArea = new javax.swing.JTextArea();
        workedTimeSpinner = new javax.swing.JSpinner();
        billableTimeSpinner = new javax.swing.JSpinner();
        completeSpinner = new javax.swing.JSpinner();

        setPreferredSize(new java.awt.Dimension(590, 330));

        annotationLabel.setFont(annotationLabel.getFont().deriveFont(annotationLabel.getFont().getStyle() & ~java.awt.Font.BOLD, annotationLabel.getFont().getSize()-1));
        annotationLabel.setText(languageModel.getString("LogPane.annotationLabel.text")); // NOI18N

        objectiveLabel.setFont(objectiveLabel.getFont().deriveFont(objectiveLabel.getFont().getSize()-1f));
        objectiveLabel.setText(languageModel.getString("LogPane.objectiveLabel.text")); // NOI18N

        summaryLabel.setFont(summaryLabel.getFont().deriveFont(summaryLabel.getFont().getSize()-1f));
        summaryLabel.setText(languageModel.getString("LogPane.summaryLabel.text")); // NOI18N

        descriptionLabel.setFont(descriptionLabel.getFont().deriveFont(descriptionLabel.getFont().getSize()-1f));
        descriptionLabel.setText(languageModel.getString("LogPane.descriptionLabel.text")); // NOI18N

        workedTimeLabel.setFont(workedTimeLabel.getFont().deriveFont(workedTimeLabel.getFont().getSize()-1f));
        workedTimeLabel.setText(languageModel.getString("LogPane.workedTimeLabel.text")); // NOI18N
        workedTimeLabel.setPreferredSize(new java.awt.Dimension(76, 22));

        billableTimeLabel.setFont(billableTimeLabel.getFont().deriveFont(billableTimeLabel.getFont().getSize()-1f));
        billableTimeLabel.setText(languageModel.getString("LogPane.billableTimeLabel.text")); // NOI18N
        billableTimeLabel.setPreferredSize(new java.awt.Dimension(76, 22));

        currentStatusLabel.setFont(currentStatusLabel.getFont().deriveFont(currentStatusLabel.getFont().getSize()-1f));
        currentStatusLabel.setText(languageModel.getString("LogPane.currentStatusLabel.text")); // NOI18N
        currentStatusLabel.setPreferredSize(new java.awt.Dimension(106, 22));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(summaryLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                    .addComponent(objectiveLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                    .addComponent(descriptionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                    .addComponent(currentStatusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                    .addComponent(billableTimeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                    .addComponent(workedTimeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(objectiveLabel)
                .addGap(18, 18, 18)
                .addComponent(summaryLabel)
                .addGap(18, 18, 18)
                .addComponent(descriptionLabel)
                .addGap(90, 90, 90)
                .addComponent(workedTimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(billableTimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(currentStatusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        objectLabel.setFont(objectLabel.getFont().deriveFont(objectLabel.getFont().getStyle() & ~java.awt.Font.BOLD, objectLabel.getFont().getSize()-1));
        objectLabel.setText("...");

        commentTextArea.setColumns(20);
        commentTextArea.setRows(5);
        jScrollPane1.setViewportView(commentTextArea);

        workedTimeSpinner.setFont(workedTimeSpinner.getFont().deriveFont(workedTimeSpinner.getFont().getStyle() & ~java.awt.Font.BOLD));

        billableTimeSpinner.setFont(billableTimeSpinner.getFont().deriveFont(billableTimeSpinner.getFont().getStyle() & ~java.awt.Font.BOLD));

        completeSpinner.setFont(completeSpinner.getFont().deriveFont(completeSpinner.getFont().getStyle() & ~java.awt.Font.BOLD));
        completeSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(objectLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                    .addComponent(workedTimeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                    .addComponent(summaryTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                    .addComponent(billableTimeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(completeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(objectLabel)
                .addGap(18, 18, 18)
                .addComponent(summaryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(workedTimeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(billableTimeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(completeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(annotationLabel)
                .addContainerGap(462, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(annotationLabel)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel annotationLabel;
    private javax.swing.JLabel billableTimeLabel;
    public javax.swing.JSpinner billableTimeSpinner;
    public javax.swing.JTextArea commentTextArea;
    public javax.swing.JSpinner completeSpinner;
    private javax.swing.JLabel currentStatusLabel;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel objectLabel;
    private javax.swing.JLabel objectiveLabel;
    private javax.swing.JLabel summaryLabel;
    public javax.swing.JTextField summaryTextField;
    private javax.swing.JLabel workedTimeLabel;
    public javax.swing.JSpinner workedTimeSpinner;
    // End of variables declaration//GEN-END:variables

    @Override
    public void updateLocale(LanguageModel languageModel) {
        annotationLabel.setText(languageModel.getString("LogPane.annotationLabel.text"));
        objectiveLabel.setText(languageModel.getString("LogPane.objectiveLabel.text"));
        summaryLabel.setText(languageModel.getString("LogPane.summaryLabel.text"));
        descriptionLabel.setText(languageModel.getString("LogPane.descriptionLabel.text"));
        workedTimeLabel.setText(languageModel.getString("LogPane.workedTimeLabel.text")); 
        billableTimeLabel.setText(languageModel.getString("LogPane.billableTimeLabel.text"));
        currentStatusLabel.setText(languageModel.getString("LogPane.currentStatusLabel.text"));

    }
}
