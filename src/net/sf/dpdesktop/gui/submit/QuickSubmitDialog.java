/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui.submit;

import net.sf.dpdesktop.gui.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.Action;
import javax.swing.JDialog;
import net.sf.dpdesktop.module.quicksubmit.QuickSubmitComponent;
import net.sf.dpdesktop.module.quicksubmit.QuickSubmitListener;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.module.tracking.TrackingComponent;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import net.sf.dpdesktop.service.container.Container;

/**
 *
 * @author Heiner Reinhardt
 */
public class QuickSubmitDialog extends javax.swing.JDialog implements TrackingComponent, QuickSubmitComponent, TrackingListener, LocaleListener  {

    private List<TrackingListener> list = new LinkedList<TrackingListener>();
    private final LanguageModel languageModel;

    @Inject
    /** Creates new form QuickSubmitDialog */
    public QuickSubmitDialog(java.awt.Frame parent, Tracker tracker, LanguageModel languageModel) {
        super(parent, true);
        this.languageModel = languageModel;
        initComponents();
        languageModel.addLocaleListener(this);

        //this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

        tracker.addListener(this);

        //this.setPreferredSize(new Dimension(800,600));

        this.cancelButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                QuickSubmitDialog.this.setVisible(false);
            }
        });

        this.pack();
    }

    public void addSubView(Component component) {
        if (component instanceof LogPane) {
            logTaskPaneContainer.removeAll();
            logTaskPaneContainer.add(component);
        } else if (component instanceof SelectionPane) {
            selectionTaskPaneContainer.removeAll();
            selectionTaskPaneContainer.add(component);
        }
        this.pack();

    }

    public void setStoreable(boolean b) {
        this.submitAndCloseButton.setEnabled(b);
        this.submitWithoutCloseButton.setEnabled(b);
    }

    @Override
    public void addTrackingListener(TrackingListener trackingListener) {

        list.add(trackingListener);

    }

    @Override
    public void addQuickSubmitListener(final QuickSubmitListener quickSubmitListener) {
        this.submitWithoutCloseButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                quickSubmitListener.submit();
            }
        });

        this.submitAndCloseButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (quickSubmitListener.submit()) {
                    QuickSubmitDialog.this.setVisible(false);
                }
            }
        });

        this.cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                quickSubmitListener.cancel();
            }
        });

    }

    @Override
    public void containerChanged(Container container) {
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void clear() {
    }

    @Override
    public void timeChanged() {
    }

    @Override
    public void ready() {
        submitAndCloseButton.setEnabled(true);
        submitWithoutCloseButton.setEnabled(true);
    }

    @Override
    public void unready() {
        submitAndCloseButton.setEnabled(false);
        submitWithoutCloseButton.setEnabled(false);
    }

    @Override
    public void commentChanged(String comment) {
        //
    }

    @Override
    public void summaryChanged(String summary) {
        //
    }

    @Override
    public void completeChanged(int complete) {
        //
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectionTaskPaneContainer = new org.jdesktop.swingx.JXTaskPaneContainer();
        cancelButton = new javax.swing.JButton();
        submitAndCloseButton = new javax.swing.JButton();
        submitWithoutCloseButton = new javax.swing.JButton();
        logTaskPaneContainer = new org.jdesktop.swingx.JXTaskPaneContainer();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(languageModel.getString("QuickSubmitDialog.title")); // NOI18N
        setResizable(false);

        selectionTaskPaneContainer.setOpaque(false);

        cancelButton.setFont(cancelButton.getFont().deriveFont(cancelButton.getFont().getStyle() & ~java.awt.Font.BOLD));
        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/cancel.png"))); // NOI18N
        cancelButton.setText(languageModel.getString("QuickSubmitDialog.cancelButton.text")); // NOI18N

        submitAndCloseButton.setFont(submitAndCloseButton.getFont().deriveFont(submitAndCloseButton.getFont().getStyle() & ~java.awt.Font.BOLD));
        submitAndCloseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/db_commit.png"))); // NOI18N
        submitAndCloseButton.setText(languageModel.getString("QuickSubmitDialog.submitAndCloseButton.text")); // NOI18N

        submitWithoutCloseButton.setFont(submitWithoutCloseButton.getFont().deriveFont(submitWithoutCloseButton.getFont().getStyle() & ~java.awt.Font.BOLD));
        submitWithoutCloseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/net/sf/dpdesktop/gui/resource/db_commit.png"))); // NOI18N
        submitWithoutCloseButton.setText(languageModel.getString("QuickSubmitDialog.submitWithoutCloseButton.text")); // NOI18N

        logTaskPaneContainer.setOpaque(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(102, Short.MAX_VALUE)
                .addComponent(submitAndCloseButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(submitWithoutCloseButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
            .addComponent(logTaskPaneContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 635, Short.MAX_VALUE)
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 635, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(selectionTaskPaneContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 623, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(selectionTaskPaneContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logTaskPaneContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(submitWithoutCloseButton)
                    .addComponent(submitAndCloseButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JSeparator jSeparator1;
    private org.jdesktop.swingx.JXTaskPaneContainer logTaskPaneContainer;
    private org.jdesktop.swingx.JXTaskPaneContainer selectionTaskPaneContainer;
    private javax.swing.JButton submitAndCloseButton;
    private javax.swing.JButton submitWithoutCloseButton;
    // End of variables declaration//GEN-END:variables

    @Override
    public void updateLocale(LanguageModel languageModel) {
        setTitle(languageModel.getString("QuickSubmitDialog.title"));
        cancelButton.setText(languageModel.getString("QuickSubmitDialog.cancelButton.text"));
        submitAndCloseButton.setText(languageModel.getString("QuickSubmitDialog.submitAndCloseButton.text"));
        submitWithoutCloseButton.setText(languageModel.getString("QuickSubmitDialog.submitWithoutCloseButton.text"));
        this.pack();
    }
}
