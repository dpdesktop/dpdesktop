/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui.util;

import com.google.inject.Inject;
import java.awt.Color;
import java.awt.Component;
import java.util.ResourceBundle;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.service.container.Container;


/**
 * Cell renderer for task combobox items. It creates a nice colorful HTML
 * representation :-) for container and strings.
 *
 * @author Heiner Reinhardt
 */
public class ContainerCellRenderer extends DefaultListCellRenderer {

    /**
     * This is a 'fallback-color' for projects. So this color will be used
     * everytime an color is not given within the project container object. 
     */

    private static final String DEFAULT_PROJECT_COLOR = "FFFFFF";
     

    private int index;
    private boolean isSelected;
    private JList list;
    private boolean cellHasFoucs;
    private Container container;
    private String substring="";

    private final static JComboBox c = new JComboBox();
    private final LanguageModel m_languageModel;

    @Inject 
    public ContainerCellRenderer(LanguageModel languageModel) {
        this.m_languageModel = languageModel;
    }




    /**
     * Will replace the index number with a nice colorful HTML string. In
     * addition a tool tip is added to each element.
     * @param list The combo box list
     * @param value The current value, which is the index!
     * @param index The current index.
     * @param isSelected Specifies whether the current item is selected.
     * @param cellHasFocus Specifies whether the current item has focus.
     * @return This
     */
    @Override
    public Component getListCellRendererComponent(
            JList list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus) {

        this.list = list;
        this.index = index;
        this.isSelected = isSelected;
        this.cellHasFoucs = cellHasFocus;


        if (value instanceof Container) {

            container = (Container) value;

            render();

        } else if(value == null) {
            // do nothhing then
        } else if(value instanceof String) {

            setForeground(c.getForeground());
            setBackground(c.getBackground());


            setText( value.toString() );
        } else {
            setText( value.toString() ); 
        }


        return this;

    }

    public void setSubstring(String substring) {
        this.substring = substring;
    }




    /**
     * Rendering a container 
     */

    private void render() {
        int count = -1;
        Container parent = container;
        while(parent.hasParent()) {
            parent = parent.getParent(); 
            count++; 
        }


        if(isSelected) {
            EasyTable table;
            table = new EasyTable();


            table.addRow(m_languageModel.getString("ContainerCellRenderer.objectiveLabel.text"), container.getName());
            table.addRowIfValueIsDifferentFromNull(m_languageModel.getString("ContainerCellRenderer.contactLabel.text"), container.get("contactPerson"));

            table.addRowIfValueIsDifferentFromNull(m_languageModel.getString("ContainerCellRenderer.phone1Label.text"), container.get("phone1"));
            table.addRowIfValueIsDifferentFromNull(m_languageModel.getString("ContainerCellRenderer.phone2Label.text"), container.get("phone2"));
            table.addRowIfValueIsDifferentFromNull(m_languageModel.getString("ContainerCellRenderer.emailLabel.text"), container.get("email"));
            table.addRowIfValueIsDifferentFromNull(m_languageModel.getString("ContainerCellRenderer.priorityLabel.text"), container.get("priority"));
            table.addRowIfValueIsDifferentFromNull(m_languageModel.getString("ContainerCellRenderer.statusLabel.text"), container.get("complete"));

            String maxSLA = container.get("sla-tracking-max");
            String currentSLA = container.get("sla-tracking-current");

            if(maxSLA != null && currentSLA != null) {
                table.addRow("SLA-Tracking", currentSLA + "/" + maxSLA);
            }


            list.setToolTipText(table.toString());
        }

        //contactPerson="" name="" phone1="" phone2="" email="" priority="" complete=""

        StringHelper stringHelper = new StringHelper(container.getName());


        setText(stringHelper.highlightSubstring(substring).indent(count).toString());


        if(isSelected && container.isTrackable()) {

            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
            
        } else {

            String color = container.get("color", DEFAULT_PROJECT_COLOR);

            int r =
                    Integer.parseInt(color.substring(0, 2), 16);
            int g =
                    Integer.parseInt(color.substring(2, 4), 16);
            int b =
                    Integer.parseInt(color.substring(4, 6), 16);

            if (Math.abs(r + g + b - 765) > 350) {
                setForeground(Color.white);
            } else {
                setForeground(Color.black);
            }

            setBackground(new Color(r, g, b));

        }


    }
}