/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui.util;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.service.log.LogItem;

/**
 *
 * @author Heiner Reinhardt
 */
public class DateTableCellRenderer extends DefaultTableCellRenderer {
    private final LanguageModel m_languageModel;


    public DateTableCellRenderer(LanguageModel languageModel) {
        this.m_languageModel = languageModel;
    }

    public Component getTableCellRendererComponent(JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {

        Component x = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        ((JLabel) x).setText(m_languageModel.getDate((String) value));


        
        return x;
    }
}
