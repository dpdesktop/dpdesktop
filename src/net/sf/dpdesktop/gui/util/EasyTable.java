/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.gui.util;

import java.util.LinkedList;

/**
 * This object provides a simple HTML table builder. The table has 2 columns.
 * @author Heiner Reinhardt
 */

public class EasyTable {
    private LinkedList<String[]> rows = new LinkedList<String[]>();
    private LinkedList<String> annotations = new LinkedList<String>();

    /**
     * This function will add a row to the table.
     * @param head The table head (written bold).
     * @param value The "normal" column value.
     */
    public void addRow( String head, String value) {
        rows.add(new String[] { head, value } );
    }


    /**
     * This function checks wether the value is null or not. Only if it is not
     * null, it will be added to the table.
     * @param head The table head (written bold).
     * @param value The "normal" column value.
     */


    public void addRowIfValueIsDifferentFromNull( String head, String value) {
        if(value!=null) {
            this.addRow(head, value);
        }
    }

    /**
     * Adds an annotation below the table.
     * @param annotation
     */

    public void addAnnotation( String annotation ) {
        annotations.add(annotation);
    }


    /**
     * Will create the table and return it as a HTML string.
     * @return HTML string with the table.
     */

    @Override
    public String toString() {
        String str = "";

        str += "<html>";
        str += "<table>";

        for(int i=0; i<rows.size(); i++) {
            str += "<tr>";
            str += "<th style='text-align: left; '>" + rows.get(i)[0] + "</th>";
            str += "<td>" + rows.get(i)[1] + "</td>";
            str += "</tr>";
        }


        str += "</table><br>";

        for(int i=0; i<annotations.size(); i++) {
            for(int j=0; j<=i; j++) {
                str += "*";
            }
            str += " " + annotations.get(i);
        }


        str += "</html>";


        return str;

    }
    
}
