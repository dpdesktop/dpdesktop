/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.gui.util;

import javax.swing.JTextArea;

/**
 *
 * @author Heiner Reinhardt
 */
public class Helper {

    /**
     * Little workaround for enabling and disabling a JTextArea.
     * background and foreground will be set to the respective colors, which
     * are equal to textbox colors.
     * @param jTextArea - a JTextArea
     * @param enabled - True - Textarea will be enabled; False - TextArea will be disabled.
     */

    public static void setTextAreaEnabled(JTextArea jTextArea, boolean enabled) {
        jTextArea.setEnabled(enabled);
        if(enabled) {
            jTextArea.setBackground(javax.swing.UIManager.getDefaults().getColor("TextField.background"));
            jTextArea.setForeground(javax.swing.UIManager.getDefaults().getColor("TextField.foreground"));
        } else {
            jTextArea.setBackground(javax.swing.UIManager.getDefaults().getColor("TextField.inactiveBackground"));
            jTextArea.setForeground(javax.swing.UIManager.getDefaults().getColor("TextField.inactiveForeground"));
        }
    }

}
