/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui.util;

import java.awt.event.FocusListener;
import javax.swing.AbstractSpinnerModel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.SwingUtilities;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.settings.LocaleListener;

/**
 *
 * @author Heiner Reinhardt
 */
public class MinuteSpinnerModel extends AbstractSpinnerModel implements LocaleListener {

    int defaultValue = 10;
    int value = defaultValue;
    int maxValue = 60;
    private final JTextField textField;
    private final JSpinner spinner;
    private String textFieldStringNever;

    public MinuteSpinnerModel(JSpinner spinner, LanguageModel languageModel) {
        this.textField = new JTextField();
        this.spinner = spinner;

        this.textField.setHorizontalAlignment(JTextField.RIGHT);

        this.spinner.setEditor(textField);

        this.textField.addFocusListener(new FocusListener() {

            public void focusLost(FocusEvent e) {
                setValue(getValue());
            }

            public void focusGained(FocusEvent e) {
                //do nothing
                }
        });


        this.textField.addKeyListener(new KeyListener() {

            public void keyTyped(KeyEvent arg0) {
                //throw new UnsupportedOperationException("Not supported yet.");
            }

            public void keyPressed(KeyEvent arg0) {
                //throw new UnsupportedOperationException("Not supported yet.");
            }

            public void keyReleased(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    autoset();
                }
            }
        });



        setValue(defaultValue);

        languageModel.addLocaleListener(this);

    }

    private void autoset() {
        setValue(getValue());
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {

        int newValue = 0;

        try {

            newValue = Integer.parseInt(value.toString(), 10);

            if (newValue > maxValue) {
                newValue = maxValue;
            } else if (newValue < 0) {
                newValue = 0;
            }



        } catch (NumberFormatException ex) {

            if (value.toString().toLowerCase().equals(textFieldStringNever)) {
                newValue = 0;
            } else {
                newValue = this.value;
            }


        }


        if (newValue == 0) {
            textField.setText(textFieldStringNever);
        } else {
            textField.setText("" + newValue);
        }


        this.value = newValue;

    }

    @Override
    public Object getNextValue() {

        int newValue = value + 1;

        setValue(newValue);

        return newValue;
    }

    @Override
    public Object getPreviousValue() {

        int newValue = value - 1;


        setValue(newValue);

        return newValue;

    }

    @Override
    public void updateLocale(LanguageModel languageModel) {
        textFieldStringNever = languageModel.getString("MinuteSpinnerModel.neverLabel.text");
        if(value==0) {
            textField.setText(textFieldStringNever);
        }
    }
}
