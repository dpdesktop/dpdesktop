/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.gui.util;

import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import net.sf.dpdesktop.gui.SelectionPane;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.service.container.ContainerRepository;

/**
 *
 * @author Heiner Reinhardt
 */
public class SearchingDocumentListener implements DocumentListener {
    private final JComboBox comboBox;
    private final JTextField searchTextField;
    private final ContainerCellRenderer containerCellRenderer;
    private final ActionListener comboBoxActionListener;
    private final ContainerRepository containerRepository;
    private final SelectionPane selectionPane;

    public SearchingDocumentListener(
            JComboBox jComboBox,
            JTextField jTextField,
            ContainerCellRenderer containerCellRenderer,
            ActionListener comboBoxActionListener,
            ContainerRepository containerRepository,
            SelectionPane selectionPane
            ) {

        this.comboBox = jComboBox;
        this.searchTextField = jTextField;
        this.containerCellRenderer = containerCellRenderer;
        this.comboBoxActionListener = comboBoxActionListener;
        this.containerRepository = containerRepository;
        this.selectionPane = selectionPane;

    }

        private HashMap<Container, Boolean> map;

        private void applyChildren(Container c) {
            for (Container child : c.getChildren()) {

                Boolean b = map.get(child);
                if (b == null) {
                    b = false;
                }
                map.put(child, b || true);
                applyChildren(child);
            }
        }

        private void applyParent(Container c) {

            Container parent = c.getParent();

            if (parent != null) {

                Boolean b = map.get(parent);
                if (b == null) {
                    b = false;
                }

                map.put(parent, b || true);
                applyParent(parent);
            }
        }
        private String searchFailedString = "Search failed.";

        public void insertUpdate(DocumentEvent arg0) {


            comboBox.removeActionListener(comboBoxActionListener);

            comboBox.removeItem(searchFailedString);

            map = new HashMap<Container, Boolean>();

            comboBox.setPopupVisible(false);

            selectionPane.applyContainer(containerRepository.getRootContainer());

            for (int i = 0; i < comboBox.getItemCount(); i++) {
                Container c = (Container) comboBox.getItemAt(i);

                StringHelper stringHelper = new StringHelper(c.getName());

                Boolean b = map.get(c);
                if (b == null) {
                    b = false;
                }


                if (stringHelper.hasSubstring(searchTextField.getText())) {
                    map.put(c, b || true);
                    applyChildren(c);
                    applyParent(c);
                } else {
                    map.put(c, b || false);
                }

            }

            for (Container c : map.keySet()) {
                if (map.get(c) == false) {
                    comboBox.removeItem(c);
                }
            }

            if (comboBox.getItemCount() == 0) {
                comboBox.addItem(searchFailedString);
            }


            containerCellRenderer.setSubstring(searchTextField.getText());

            comboBox.setPopupVisible(true);

            comboBox.addActionListener(comboBoxActionListener);

        }

        public void removeUpdate(DocumentEvent arg0) {
            insertUpdate(arg0);
        }

        public void changedUpdate(DocumentEvent arg0) {
            // can be ignored
        }

}
