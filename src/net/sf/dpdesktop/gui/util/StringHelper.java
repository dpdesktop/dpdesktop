/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.gui.util;

/**
 * This class allows to indent strings, as well as highlight a substring of a
 * string. The output is in HTML format.
 * @author Heiner Reinhardt
 */
public class StringHelper {

    /** The multiplier for indention */
    private int indentMultiplier = 0;

    /** The String to work with */
    private String string;
    
    /** Highlight color of the substring*/
    private final static String HIGHLIGHT_COLOR = "blue";

    /**
     * Creates the a StringHelper object
     * @param string The string to work at
     */
    public StringHelper(String string) {
        this.string = string;
    }

    /**
     * Indent the string by the given multiplier
     * @param multiplier Number of indentions
     * @return StringHelper
     */
    public StringHelper indent(Integer multiplier) {
        indentMultiplier = multiplier;

        return this;
    }

    /**
     * This function highlights a substring in the given String.
     * The search for the substring is case insensitive. But the output will
     * be case sensitive.
     * @param substring String to be highlighted
     * @return StringHelper
     */
    public StringHelper highlightSubstring(String substring) {

        int index = indexOf(substring);

        if (index >= 0) {

            string = string.substring(0, index) +
                    "<span style=\"color: "+ HIGHLIGHT_COLOR +"; font-weight: bold;\" >" +
                    string.substring(index, index + substring.length()) +
                    "</span>" +
                    string.substring(index + substring.length());

        }

        return this;
    }

    /**
     * Simply finds the position of a substring in the string. This function
     * is case insensitive.
     * @param match String to be searched for
     * @return
     */
    private int indexOf(String match) {

        String tmpString = string.toLowerCase();
        String tmpMatch = match.toLowerCase();

        return tmpString.indexOf(tmpMatch);

    }

    /**
     * Simply returns whether there is a substring inside the String given
     * through constructor or not. This function is case insensitive.
     * @param match
     * @return StringHelper
     */
    public boolean hasSubstring(String match) {
        return this.indexOf(match)>=0;
    }


    /**
     * Delivers the HTML representation of the indented or highlighted string.
     * @return String in HTML format
     */
    @Override
    public String toString() {

        String str = "<html>"
                + "<div style='margin-left: " + (this.indentMultiplier*10) + "px'>"
                + string
                + "</div>"
                + "</html>";
        
        return str;
    }

}
