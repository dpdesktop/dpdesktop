/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.gui.util;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Arrays;
import javax.swing.AbstractSpinnerModel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

/**
 *
 * @author Heiner Reinhardt
 */
public class TimeSpinnerModel extends AbstractSpinnerModel {


    private int stepSize = 10;
    /**
     * This time will be used when there were parse errors.
     * Index 0 - Hours
     * Index 1 - Minutes
     * It is initiliazed with {0,0} but will be the 'last value'
     */
    private Integer fallbackTime[] = {0, 0};
    private JTextField textField;
    private JSpinner spinner;

    public TimeSpinnerModel(JSpinner spinner) {
        
        this.textField = new JTextField();
        this.spinner = spinner;

        spinner.setEditor(textField);

        this.textField.addFocusListener(new FocusListener() {

            public void focusLost(FocusEvent e) {
                setValue(getValue());
            }

            public void focusGained(FocusEvent e) {
                //do nothing
                }
        });

        this.textField.addKeyListener(new KeyListener() {

            public void keyTyped(KeyEvent arg0) {
                //throw new UnsupportedOperationException("Not supported yet.");
            }

            public void keyPressed(KeyEvent arg0) {
                //throw new UnsupportedOperationException("Not supported yet.");
            }

            public void keyReleased(KeyEvent arg0) {
                if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                    autoset();
                }
            }
        });

        this.textField.setText("00:00");

    }

    public void clear() {
        setValue(0,0); 
    }

    /**
     * This function sets the time spinner back to the right format.
     */
    private void autoset() {
        setValue(getValue());
    }
    
    /**
     * Returns the current formated value (HH:mm)
     * @return The respective value
     */
    public Object getValue() {
        Integer time[] = parse();

        return format(time[0], time[1]);
    }


    /**
     * Returns the hours of the spinner.
     * @return Hours
     */
    public int getHours() {
        return this.parse()[0];
    }

    /**
     * Returns the minutes of the spinner.
     * @return Minutes
     */
    public int getMinutes() {
        return this.parse()[1];
    }

    /**
     * Returns true when time is reset.
     * @return true when time is set to 00:00
     */
    public boolean isCleared() {
        Integer time[] = this.parse();
        return (time[0] == 0 && time[1] == 0);
    }

    /**
     * Writes the given value into the spinner. But before, the given value
     * will be parsed. In addition it fires a stateChanged event.
     * @see #parse()
     * @param value
     */
    public void setValue(Object value) {

        Integer time[] = this.parse();

        setValue(time[0], time[1]);
    }

    /**
     * Writes given hours and minutes in the spinner.
     * @param hours
     * @param minutes
     */

    public void setValue(int hours, int minutes) {

        if (hours < 0) {
            hours = 0;
        }

        int currentStep = 0;

        boolean ready = false;

        while (currentStep <= 60 && !ready) {
            if (minutes <= currentStep) {
                minutes = currentStep;
                if (currentStep == 60) {
                    hours = hours + 1;
                    minutes = 0;
                }
                ready = true;
            }
            currentStep = currentStep + stepSize;
        }


        this.fallbackTime[0] = hours;
        this.fallbackTime[1] = minutes;

        textField.setText(format(hours, minutes));

        this.fireStateChanged();

    }

    /**
     * Calculates the next value and returns a string representation
     * @see #format(int, int)
     * @return string representation of time (HH:mm)
     */
    public Object getNextValue() {

        Integer time[] = this.parse();

        int nextH = time[0];
        int nextM = time[1] + stepSize;

        if (nextM >= 60) {
            nextH = nextH + 1;
            nextM = 0;
        }

        setValue(nextH, nextM);
        return format(nextH, nextM);
    }

    /**
     * Calculates the previous value and returns a string representation
     * @see #format(int, int)
     * @return string representation of time (HH:mm)
     */
    public Object getPreviousValue() {

        Integer time[] = this.parse();

        int prevH = time[0];
        int prevM = time[1];

        if (prevM <= 0) {
            prevM = 60 - stepSize;
            prevH = prevH - 1;
        } else {
            prevM = prevM - stepSize;
        }

        if (prevH < 0) {
            prevH = 0;
            prevM = 0;
        }

        setValue(prevH, prevM);

        return format(prevH, prevM);
    }

    /**
     * Formats given hours and minutes in this format: HH:mm
     * @param hours
     * @param minutes
     * @return The respective formated string
     */
    private String format(int hours, int minutes) {
        return String.format("%02d:%02d", hours, minutes);
    }

    /**
     * Parses the current data of the spinner. When there are parsing errors
     * the spinner value is set to the last correct value or to 00:00 if there
     * was no correct value before.
     * @return 0 - Hour, 1 - Minute
     */
    private Integer[] parse() {

        Integer time[] = {0, 0};

        String s = textField.getText();
        String split[] = s.split(":");

        try {
            if (split.length == 2) {
                time[0] = Integer.valueOf(split[0]);
                time[1] = Integer.valueOf(split[1]);
            } else {
                time[0] = Integer.valueOf(s);
                time[1] = 0;
            }
        } catch (NumberFormatException e) {

            //Exception this Exception is no problem

            time = fallbackTime;
        }

        return time;

    }



    private int[] allowedStepSizes = new int[] {1,2,5,10,15,20,30};

    /**
     * Possible values are "1","2","5","10","15","20","30".
     *
     * Default value is 10 and will be used as fall back, if any unsual number
     * was given.
     *
     * @param stepSize the stepSize to set
     */
    public void setStepSize(int stepSize) {

        if(Arrays.binarySearch(allowedStepSizes, stepSize)>=0) {
            this.stepSize = stepSize;
        }

    }
}
