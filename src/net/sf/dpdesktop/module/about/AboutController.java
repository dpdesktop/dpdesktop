/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.about;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import net.sf.dpdesktop.gui.AboutDialog;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.service.config.Configuration;

/**
 *
 * @author Heiner Reinhardt
 */
public class AboutController implements ActionListener {

    private final AboutDialog m_aboutView;

    @Inject
    public AboutController(AboutDialog aboutDialog) {
        this.m_aboutView = aboutDialog;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        m_aboutView.setVisible(true);
    }

    public void registerComponent(AboutStartupComponent aboutComponent) {
        aboutComponent.addAboutStartupListener(this);
    }
}
