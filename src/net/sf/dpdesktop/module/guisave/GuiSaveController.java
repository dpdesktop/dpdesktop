/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.guisave;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.ContextProvider;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPane;

/**
 *
 * @author Heiner Reinhardt
 */
public class GuiSaveController implements PropertyChangeListener {

    private final MainWindow m_mainView;
    private final ConfigurationService m_guiStateService;
    private final ContextProvider m_contextProvider;

    @Inject
    public GuiSaveController(MainWindow mainView, ContextProvider contextProvider, @Named("gui") ConfigurationService guiStateService) {


        this.m_mainView = mainView;
        this.m_guiStateService = guiStateService; 
        this.m_contextProvider = contextProvider;

        try {
            m_guiStateService.load(m_contextProvider.getContextFile());
        } catch (ServiceAuthException ex) {
            handleLoadException(ex);
        } catch (MalformedURLException ex) {
            handleLoadException(ex);
        } catch (IOException ex) {
            handleLoadException(ex);
        } catch (DocumentException ex) {
            handleLoadException(ex);
        }

        m_mainView.addTaskPaneCollapsedPropertyListener(this);

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof JXTaskPane) {

            JXTaskPane taskPane = (JXTaskPane) evt.getSource();

            m_guiStateService.setFlag(taskPane.getName(), taskPane.isCollapsed());
            try {
                m_guiStateService.store(m_contextProvider.getContextFile());
            } catch (MalformedURLException ex) {
                handleStoreException(ex);
            } catch (ServiceAuthException ex) {
                handleStoreException(ex);
            } catch (IOException ex) {
                handleStoreException(ex);
            }

        }
        
    }

    private void handleLoadException(Exception ex) {
        Logger.getLogger(this.getClass()).warn("Could not load task pane settings (expanded or not)", ex);
    }

    private void handleStoreException(Exception ex) {
        Logger.getLogger(this.getClass()).warn("Could not store task pane settings (expanded or not)", ex);
    }

}
