/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.guistate;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import net.sf.dpdesktop.module.submit.SubmitController;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class ApplicationStateModel {

    private final ActionListener exitAction;

    @Inject
    public ApplicationStateModel(@Named("exitAction") ActionListener exitActionListener) {

        this.exitAction = exitActionListener;

    }
    private List<ApplicationStateListener> listenerList = new ArrayList<ApplicationStateListener>();
    private List<VetoableExitListener> vetoableExitListenerList = new ArrayList<VetoableExitListener>();
    private State m_state = State.MINIMIZED;
    private boolean m_supported = false;

    public void addApplicationStateListener(ApplicationStateListener applicationStateListener) {
        listenerList.add(applicationStateListener);
    }



    public void setState(State state) {
        Logger.getLogger(this.getClass()).debug("Initializing state: " + state);

        state.init(this);
        this.m_state = state;
    }

    private synchronized List<ApplicationStateListener> getCurrentList() {

        List<ApplicationStateListener> list = new LinkedList<ApplicationStateListener>();

        for(ApplicationStateListener listener : listenerList) {
            if(this.isTraySupported()) {
                if(listener.isTraySupported()!=TraySupportEnum.TRAY_DISABLED) {
                    list.add(listener);
                }
            } else {
                if(listener.isTraySupported()!=TraySupportEnum.TRAY_ENABLED) {
                    list.add(listener);
                }
            }
        }

        return list; 
    }

    public synchronized void fireMinimizeState() {
        for (ApplicationStateListener l : getCurrentList()) {
            l.minimize();
        }
    }

    public synchronized void fireMaximizeState() {
        for (ApplicationStateListener l : getCurrentList()) {
            l.maximize();
        }
    }

    public synchronized void fireExitingState() {
        for (ApplicationStateListener l : getCurrentList()) {
            l.exiting();
        }
    }

    /**
     * @return the maximize
     */
    public boolean isMaximized() {
        return m_state == State.MAXIMIZED;
    }

    public boolean isMinimized() {
        return m_state == State.MINIMIZED;
    }

    public void setTraySupported(boolean supported) {

        this.m_supported = supported;
        this.m_state.init(this);
    }

    public boolean isTraySupported() {
        return m_supported;
    }

    public void maximize() {
        m_state.maximize(this);
    }

    public void minimize() {
        m_state.minimize(this);
    }

    public void exiting() {
        m_state.exiting(this);
    }

    public void refresh() {
        m_state.init(this);
    }

    public boolean isExitingAllowed() {



        boolean isExitingAllowed = true;

        for (VetoableExitListener vetoableExitListener : vetoableExitListenerList) {
            isExitingAllowed = isExitingAllowed && vetoableExitListener.isExitingAllowed();
        }


        return isExitingAllowed;


    }

    public void addVetoableExitListener(VetoableExitListener vetoableExitListener) {
        vetoableExitListenerList.add(vetoableExitListener);
    }

    public ActionListener getExitListener() {
        return exitAction;
    }

    public void forceExit() {
        this.exitAction.actionPerformed(null);
    }

}
