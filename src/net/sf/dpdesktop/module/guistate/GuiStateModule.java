/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.guistate;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Heiner Reinhardt
 */
public class GuiStateModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(StateController.class).in(Scopes.SINGLETON);
        bind(ApplicationStateModel.class).in(Scopes.SINGLETON);
    }

    @Provides
    @Named("exitAction")
    private ActionListener provideExitActionListener() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        };
    }


}
