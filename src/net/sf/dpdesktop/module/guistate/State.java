/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.guistate;

import java.awt.event.ActionEvent;

/**
 *
 * @author Heiner Reinhardt
 */
public enum State {

    MAXIMIZED {

        @Override
        public void init(ApplicationStateModel applicationStateModel) {
            applicationStateModel.fireMaximizeState();
        }

        @Override
        public void maximize(ApplicationStateModel applicationStateModel) {
            //this.init(applicationStateModel);
        }

        @Override
        public void minimize(ApplicationStateModel applicationStateModel) {
            applicationStateModel.setState(MINIMIZED);
        }

        @Override
        public void exiting(ApplicationStateModel applicationStateModel) {
            if(applicationStateModel.isExitingAllowed()) {
                applicationStateModel.setState(EXITING);
            }

        }

        @Override
        public void fire(ApplicationStateListener applicationStateListener) {
            applicationStateListener.maximize();
        }


    },
    MINIMIZED {

        @Override
        public void init(ApplicationStateModel applicationStateModel) {
            applicationStateModel.fireMinimizeState();
        }

        @Override
        public void maximize(ApplicationStateModel applicationStateModel) {
            applicationStateModel.setState(MAXIMIZED);
        }

        @Override
        public void minimize(ApplicationStateModel applicationStateModel) {
            //this.init(applicationStateModel);
        }

        @Override
        public void exiting(ApplicationStateModel applicationStateModel) {
            if(applicationStateModel.isExitingAllowed()) {
                applicationStateModel.setState(EXITING);
            }
        }

        @Override
        public void fire(ApplicationStateListener applicationStateListener) {
            applicationStateListener.minimize();
        }

    },
    EXITING {

        @Override
        public void init(ApplicationStateModel applicationStateModel) {
            applicationStateModel.fireExitingState();
            
            applicationStateModel.getExitListener().actionPerformed(new ActionEvent(this, 0, "exit"));

        }

        @Override
        public void maximize(ApplicationStateModel applicationStateModel) {
            applicationStateModel.setState(MAXIMIZED);
        }

        @Override
        public void minimize(ApplicationStateModel applicationStateModel) {
            applicationStateModel.setState(MINIMIZED);
        }

        @Override
        public void exiting(ApplicationStateModel applicationStateModel) {
            this.init(applicationStateModel);
        }
        
        @Override
        public void fire(ApplicationStateListener applicationStateListener) {
            //
        }


    };
    

    public abstract void init(ApplicationStateModel applicationStateModel);

    public abstract void maximize(ApplicationStateModel applicationStateModel);

    public abstract void minimize(ApplicationStateModel applicationStateModel);

    public abstract void exiting(ApplicationStateModel applicationStateModel);

    public abstract void fire(ApplicationStateListener applicationStateListener);



}
