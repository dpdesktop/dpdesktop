/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.guistate;

import com.google.inject.Inject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Heiner Reinhardt
 */
public class StateController {

    private final ApplicationStateModel applicationStateModel;

    @Inject
    public StateController(ApplicationStateModel applicationStateModel) {
        this.applicationStateModel = applicationStateModel;
    }
    private ApplicationStateListener standardApplicationStateListener = new ApplicationStateListener() {

        public void maximize() {
            applicationStateModel.maximize();
        }

        public void minimize() {
            applicationStateModel.minimize();
        }

        public void exiting() {
            applicationStateModel.exiting();
        }

        @Override
        public TraySupportEnum isTraySupported() {
            if (applicationStateModel.isTraySupported()) {
                return TraySupportEnum.TRAY_ENABLED;
            }

            return TraySupportEnum.TRAY_DISABLED;
        }
    };

    public void registerComponent(StateComponent applicationStateComponent) {

        applicationStateComponent.setApplicationStateListener(standardApplicationStateListener);

    }

    public ApplicationStateModel getApplicationStateModel() {
        return applicationStateModel;
    }

    public void setTraySupported(boolean supported) {
        applicationStateModel.setTraySupported(supported);
    }
}
