/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.guistate;

import net.sf.dpdesktop.module.guistate.ApplicationStateListener;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import net.sf.dpdesktop.module.tracking.TrackingManager;

/**
 *
 * @author Heiner Reinhardt
 */
public interface Tray extends StateComponent {




}
