package net.sf.dpdesktop.module.guistate;

/**
 *
 * @author Heiner Reinhardt
 */
public enum TraySupportEnum {

    TRAY_ENABLED, TRAY_DISABLED, BOTH

}
