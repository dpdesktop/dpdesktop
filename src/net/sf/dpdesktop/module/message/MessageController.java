/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.message;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import java.awt.Frame;
import javax.swing.JFrame;
import net.sf.dpdesktop.gui.MessageDialog;

/**
 *
 * @author Heiner Reinhardt
 */
public class MessageController {
    private final MessageModel m_messageModel;

    @Inject
    public MessageController(Frame frame, MessageModel messageModel) {
        this.m_messageModel = messageModel; 
    }

    public MessageModel getMessageModel() {
        return m_messageModel;
    }

    public static void main(String args[]) {

        MessageController mc = new MessageController(new JFrame(), null);

        MessageModel model = mc.getMessageModel();

        model.error(new Exception("Test exception"), "Test", "teeeeeeeeeest");


    }

}
