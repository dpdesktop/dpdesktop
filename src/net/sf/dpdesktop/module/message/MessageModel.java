/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.message;

import com.google.inject.Inject;
import java.awt.Component;
import java.awt.Frame;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.gui.MessageDialog;
import net.sf.dpdesktop.service.exception.ResponseCodeException;

/**
 * This is an abstraction layer for creating error messages. For example: The
 * VIEW layer may decide whether to draw ALL info messages in a message box or
 * draw the message in the bubble of a tray icon.
 * @author Heiner Reinhardt
 */
public class MessageModel {


    private List<MessageListener> messageListenerList = new LinkedList<MessageListener>();
    private List<QuestionListener> questionListenerList = new LinkedList<QuestionListener>();

    @Inject
    public MessageModel() {

    }

    public void error(Throwable ex, String headline, String message) {
        this.fireShowMessage(MessageType.ERROR, ex, headline, message);
    }

    public void warn(Throwable ex, String headline, String message) {
        this.fireShowMessage(MessageType.WARNING, ex, headline, message);
    }

    public void warn(String headline, String message) {
        this.fireShowMessage(MessageType.WARNING, headline, message);
    }

    public void info(Throwable ex, String headline, String message) {
        this.fireShowMessage(MessageType.INFORMATION, ex, headline, message);
    }

    public void addMessageListener(MessageListener messageListener) {
        messageListenerList.add(messageListener);
    }

    public void addQuestionListener(QuestionListener questionListener) {
        questionListenerList.add(questionListener);
    }

    private void fireShowMessage(MessageType messageType, Throwable ex, String headline, String message) {
        for(MessageListener messageListener : messageListenerList) {

            messageListener.showMessage(messageType, ex, headline, message);
        }
    }

    private void fireShowMessage(MessageType messageType, String headline, String message) {
        for(MessageListener messageListener : messageListenerList) {
            messageListener.showMessage(messageType, headline, message);
        }
    }

    public void info(String headline, String message) {
        this.fireShowMessage(MessageType.INFORMATION, headline, message);
    }

    public boolean question(String headline, String message) {
        for(QuestionListener questionListener : questionListenerList) {
            return questionListener.message(MessageType.QUESTION, headline, message);
        }
        return false; 
    }






}
