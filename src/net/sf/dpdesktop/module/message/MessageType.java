/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.message;

import java.util.ResourceBundle;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Heiner Reinhardt
 */
public enum MessageType {

    ERROR {
        @Override
        public String toString() {
            return "Error";
        }

        @Override
        public ImageIcon getIcon() {
            return new ImageIcon(this.getClass().getResource("/net/sf/dpdesktop/gui/resource/messagebox_critical.png"));
        }
    },

    WARNING {
        @Override
        public String toString() {
            return "Warning";
        }

        @Override
        public ImageIcon getIcon() {
            return new ImageIcon(this.getClass().getResource("/net/sf/dpdesktop/gui/resource/messagebox_warning.png"));
        }
    },

    INFORMATION {
        @Override
        public String toString() {
            return "Information";
        }

        @Override
        public ImageIcon getIcon() {
            return new ImageIcon(this.getClass().getResource("/net/sf/dpdesktop/gui/resource/messagebox_info.png"));
        }
    },

    QUESTION {
        @Override
        public String toString() {
            return "Question";
        }

        @Override
        public ImageIcon getIcon() {
            return new ImageIcon(this.getClass().getResource("/net/sf/dpdesktop/gui/resource/network.png"));
        };
    };


    
    @Override
    public abstract String toString();

    public abstract ImageIcon getIcon();

}
