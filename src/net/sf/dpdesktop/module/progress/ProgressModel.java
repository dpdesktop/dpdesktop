/**********************************************************************
*  Copyright notice
*
*  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/
package net.sf.dpdesktop.module.progress;

import java.util.LinkedList;

/**
 *
 * @author Heiner Reinhardt
 */
public class ProgressModel {

    //TODO[z][Feature] needs thread safety!

    public ProgressModel() {
    }

    private LinkedList<ProgressListener> listenerList = new LinkedList<ProgressListener>();
    private int percent = 0;
    private String text = "";
    private State state = State.DONE;

    public void addProgressListener(ProgressListener progressListener) {
        listenerList.add(progressListener);

        state.init(this, progressListener);

    }

    public void setRunning(String text, int percent) {
        this.percent = percent;
        this.text = text;
        this.setState(State.RUNNING);
    }
    
    public void setDone() {
        this.setState(State.DONE);
    }


    private void setState(State state) {
        this.state = state;
        state.init(this);
    }

    public int getPercent() {
        return percent;
    }

    public String getText() {
        return text;
    }

    void fireRunning() {
        for(ProgressListener progressListener : listenerList) {
            progressListener.running(text, percent);
        }
    }

    void fireDone() {
        for(ProgressListener progressListener : listenerList) {
            progressListener.done();
        }
    }



}
