/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.quicksubmit;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.dpdesktop.gui.DatabasePane;
import net.sf.dpdesktop.gui.submit.LogPane;
import net.sf.dpdesktop.gui.SelectionPane;
import net.sf.dpdesktop.gui.submit.QuickSubmitDialog;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.submit.SubmitHandler;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.module.tracking.TrackingController;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.module.update.UpdateController;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.log.LogItem;
import net.sf.dpdesktop.service.log.LogItemService;
import net.sf.dpdesktop.service.Context;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.ServiceDataException;
import net.sf.dpdesktop.service.log.LogItemServiceFactory;

/**
 *
 * @author Heiner Reinhardt
 */
public class QuickSubmitController implements QuickSubmitListener {

    private final LogPane m_logPane;
    private final QuickSubmitDialog m_quickSubmitDialog;
    private final SelectionPane m_selectionPane;
    private final TrackingManager m_trackingManager;
    private final ContextProvider m_contextProvider;
    private final MessageModel m_messageModel;
    private final UpdateController m_updateController;
    private final LogItemServiceFactory m_logItemServiceFactory;
    private final SubmitHandler m_submitHandler;
    private final LanguageModel m_languageModel;

    @Inject
    public QuickSubmitController(
            
            @Named("static") QuickSubmitDialog quickSubmitDialog,
            @Named("static") TrackingController trackingController,
            @Named("static") SelectionPane selectionPane,
            @Named("static") LogPane logPane,
            @Named("static") TrackingManager trackingManager,
            DatabasePane databasePane,
            ContextProvider contextProvider,
            UpdateController updateController,
            MessageModel messageModel,
            LogItemServiceFactory logItemServiceFactory,
            SubmitHandler submitHandler,
            LanguageModel languageModel
            ) {

        this.m_languageModel = languageModel;
        this.m_submitHandler = submitHandler;
        this.m_contextProvider = contextProvider;
        this.m_updateController = updateController;
        this.m_messageModel = messageModel;

        this.m_selectionPane = selectionPane;
        this.m_quickSubmitDialog = quickSubmitDialog;
        this.m_logPane = logPane;
        this.m_trackingManager = trackingManager;

        this.m_logItemServiceFactory = logItemServiceFactory;

        m_quickSubmitDialog.addSubView(m_selectionPane);
        m_quickSubmitDialog.addSubView(m_logPane);
        
        trackingController.registerComponent(m_selectionPane);

        databasePane.addQuickSubmitListener(new QuickSubmitListener() {

            @Override
            public boolean submit() {
                m_quickSubmitDialog.setVisible(true);
                return true;
            }

            @Override
            public void cancel() {
                //
            }
        });

        quickSubmitDialog.addQuickSubmitListener(this);

    }

    @Override
    public boolean submit() {
        return m_submitHandler.submit(m_logItemServiceFactory, m_trackingManager, m_messageModel, m_logPane, m_contextProvider, m_updateController, m_languageModel.getString("QuickSubmitController.message.quickSubmitFailed.headline"));
    }

    @Override
    public void cancel() {
        m_trackingManager.reset();
        m_quickSubmitDialog.setVisible(false);

    }
}
