/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.quicksubmit;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import java.awt.Frame;
import net.sf.dpdesktop.gui.submit.LogPane;
import net.sf.dpdesktop.gui.SelectionPane;
import net.sf.dpdesktop.gui.submit.QuickSubmitDialog;
import net.sf.dpdesktop.gui.util.ContainerCellRenderer;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.module.tracking.TrackingController;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.service.container.ContainerRepository;

/**
 *
 * @author Heiner Reinhardt
 */
public class QuickSubmitModule extends AbstractModule{

    @Override
    protected void configure() {
        bind(QuickSubmitController.class).in(Scopes.SINGLETON);
    }


    @Singleton
    @Provides
    @Named("static")
    private TrackingManager povideTrackingManager() {
        return new TrackingManager();
    }


    @Singleton
    @Provides
    @Named("static")
    private TrackingController provideTrackingController(@Named("static") TrackingManager trackingManager, ContainerRepository containerRepository) {
        return new TrackingController(trackingManager, containerRepository);
    }


    @Provides
    @Named("static")
    private Tracker provideTracker(@Named("static") TrackingController trackingController) {
        return trackingController.getTracker();
    }

    @Singleton
    @Provides
    @Named("static")
    private LogPane provideLogPane(@Named("static") Tracker tracker, LanguageModel languageModel) {
        return new LogPane(tracker, languageModel);
    }

    @Singleton
    @Provides
    @Named("static")
    private SelectionPane provideSelectionPane(@Named("static") Tracker tracker, ContainerRepository containerRepository, LanguageModel languageModel, ContainerCellRenderer containerCellRenderer) {
        return new SelectionPane(tracker, containerRepository, languageModel, containerCellRenderer);
    }

    @Singleton
    @Provides
    @Named("static")
    private QuickSubmitDialog provideSelectionPane(@Named("static") Tracker tracker, Frame frame, LanguageModel languageModel) {
        return new QuickSubmitDialog(frame, tracker, languageModel);
    }



}
