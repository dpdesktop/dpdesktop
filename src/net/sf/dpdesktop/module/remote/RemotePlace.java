/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.remote;

import java.net.MalformedURLException;
import java.net.URL;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.container.ContainerService;
import net.sf.dpdesktop.service.log.LogItemRepository;
import net.sf.dpdesktop.service.log.LogItemService;


/**
 *
 * @author Heiner Reinhardt
 */
public class RemotePlace implements Comparable<RemotePlace> {
    private String company;
    private String user;
    private String pass;
    private String url;


    public RemotePlace(String company, String url, String user, String pass)  {
        this.company = company;
        this.url = url;
        this.user = user;
        this.pass = pass;
    }

    public String getCompany() {
        return company;
    }

    /**
     * @return the url
     */
    public URL getURL() throws MalformedURLException {
        return new URL(url);
    }

    public String getURLAsString() {
        return url; 
    }

    /**
     * @return the user
     */
    public String getUsername() {
        return user;
    }

    /**
     * @return the pass
     */
    public String getPassword() {
        return pass;
    }

    @Override
    public String toString() {
        return this.getCompany();
    }

    /**
     * @param id the id to set
     */
    public void setCompany(String id) {
        this.company = id;
    }

    /**
     * @param user the user to set
     */
    public void setUsername(String user) {
        this.user = user;
    }

    /**
     * @param pass the pass to set
     */
    public void setPassword(String pass) {
        this.pass = pass;
    }

    /**
     * @param url the url to set
     * @return boolean value indicating whether it is a valid url or not. The local url member will be set anyway!
     */
    public boolean setUrl(String url) {
        this.url = url;

        try {
            new URL(url);
            return true;
        } catch(MalformedURLException ex) {
            return false;
        }

    }

    @Override
    public int compareTo(RemotePlace remotePlace) {
        return this.company.compareTo(remotePlace.company);
    }




}
