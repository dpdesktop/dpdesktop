/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.remote;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.log.LogItemRepository;

/**
 *
 * @author Heiner Reinhardt
 */
public class RemotePlaceController {

    @Inject
    public RemotePlaceController(
            RemotePlaceRepository remotePlaceRepository,
            @Named("clone") RemotePlaceRepository remotePlaceRepositoryClone,
            ContainerRepository containerRepository,
            LogItemRepository logItemRepository) {

        remotePlaceRepository.addRepositoryListener(containerRepository);
        remotePlaceRepository.addRepositoryListener(logItemRepository);



        remotePlaceRepositoryClone.synchronizeWith(remotePlaceRepository);

    }
}
