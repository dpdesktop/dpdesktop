/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.remote;

import com.google.inject.Inject;
import com.google.inject.internal.Collections2;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.AbstractListModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import net.sf.dpdesktop.module.util.hash.HashController;
import net.sf.dpdesktop.module.util.hash.HashFactory;
import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.log.LogItemRepository;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.DocumentType;
import org.dom4j.Element;
import org.dom4j.Node;

/**
 *
 * @author Heiner Reinhardt
 */
public class RemotePlaceRepository extends AbstractListModel implements Cloneable {

    private LinkedList<SelectionListener> selectionListenerList = new LinkedList<SelectionListener>();
    private LinkedList<RepositoryListener> repositoryListenerList = new LinkedList<RepositoryListener>();
    private volatile  int currentSelectedIndex = -1;
    private LinkedList<RemotePlaceServiceProvider> list = new LinkedList<RemotePlaceServiceProvider>();
    private final HashFactory hashFactory;

    @Inject
    public RemotePlaceRepository(HashFactory hashFactory) {
        this.hashFactory = hashFactory;
    }

    public List<RemotePlaceServiceProvider> getList() {
        return list;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public RemotePlaceServiceProvider getElementAt(int index) {
        return list.get(index);
    }

    public void addElement(String company, String url, String name, String pass, boolean isPassClearText) {


        if(isPassClearText) {
            pass = hashFactory.getValue(pass);
        }
        
        list.add(new RemotePlaceServiceProvider(company, url, name, pass));

        int index = this.getSize() - 1;

        this.fireIntervalAdded(this, index, index);
        this.fireContentsChanged(this, index, index);


        this.currentSelectedIndex = index;

        this.fireSelectionChanged();

    }

    private void fireSelectionChanged() {

        try {
            for (SelectionListener selectionListener : selectionListenerList) {
                selectionListener.selected(list.get(currentSelectedIndex), currentSelectedIndex);
            }
        } catch (IndexOutOfBoundsException ex) {
            Logger.getLogger(this.getClass()).info(ex, ex);
            //don't care
        }
    }

    public void removeElement(RemotePlaceServiceProvider remotePlaceServiceProvider) {
        int index = list.indexOf(remotePlaceServiceProvider);

        list.remove(index);

        this.fireIntervalRemoved(this, index, index);
        this.fireContentsChanged(this, 0, this.getSize() - 1);

    }

    public void addSelectionListener(SelectionListener selectionListener) {
        selectionListenerList.add(selectionListener);

        try {
            selectionListener.selected(list.get(currentSelectedIndex), currentSelectedIndex);
        } catch (IndexOutOfBoundsException ex) {
            //nothing
        }
    }

    public void addRepositoryListener(RepositoryListener repositoryListener) {
        repositoryListenerList.add(repositoryListener);

        //repositoryListener.listChanged(list);

    }

    public void refresh() {

        for (RepositoryListener repositoryListener : repositoryListenerList) {
            repositoryListener.listChanged(this.getList());
        }

        currentSelectedIndex = this.getSize()-1;

        this.fireContentsChanged(this, 0, currentSelectedIndex);

        this.fireSelectionChanged();

    }

    public int getIndexOf(RemotePlaceServiceProvider remotePlaceServiceProvider) {
        return list.indexOf(remotePlaceServiceProvider);
    }

    public void synchronizeWith(RemotePlaceRepository remotePlaceRepository) {

        this.list = new LinkedList<RemotePlaceServiceProvider>();

        for (RemotePlaceServiceProvider remotePlaceServiceProvider : remotePlaceRepository.list) {

            if (!remotePlaceRepository.cleanUpSet.contains(remotePlaceServiceProvider)) {
                this.list.add((RemotePlaceServiceProvider) remotePlaceServiceProvider.clone());
            }
        }

        for (RemotePlaceServiceProvider remotePlaceServiceProvider : remotePlaceRepository.cleanUpSet) {

            this.list.add(new RemotePlaceServiceProvider(
                    remotePlaceServiceProvider.getCompany(),
                    remotePlaceServiceProvider.getURLAsString(),
                    remotePlaceServiceProvider.getUsername(),
                    remotePlaceServiceProvider.getPassword()));

        }

        remotePlaceRepository.cleanUpSet.clear();

        this.refresh();


    }
    private HashSet<RemotePlaceServiceProvider> cleanUpSet = new HashSet<RemotePlaceServiceProvider>();

    public void setCompany(RemotePlace remotePlace, String company) {

        if (remotePlace.equals(null) || remotePlace.getCompany().equals(company)) {
            return;
        }

        remotePlace.setCompany(company);
        this.refresh(remotePlace);
    }

    public void setUrl(RemotePlace remotePlace, String url) {

        if (remotePlace.equals(null) || remotePlace.getURLAsString().equals(url)) {
            return;
        }

        remotePlace.setUrl(url);
        cleanUpSet.add((RemotePlaceServiceProvider) remotePlace);
        this.refresh(remotePlace);
    
    }

    public void setUsername(RemotePlace remotePlace, String user) {


        if (remotePlace.equals(null) || remotePlace.getUsername().equals(user)) {
            return;
        }
        remotePlace.setUsername(user);
        this.refresh(remotePlace);
    }

    public void setPassword(RemotePlace remotePlace, String pass) {


        if(remotePlace.equals(null)) {
            return;
        }

        if(pass.equals(remotePlace.getPassword())) {
            return; 
        }

        String passHash = hashFactory.getValue(pass);


        if (remotePlace.getPassword().equals(passHash)) {
            return;
        }
        remotePlace.setPassword(passHash);
        this.refresh(remotePlace);
    }

    private void refresh(RemotePlace remotePlace) {
        int index = list.indexOf(remotePlace);
        this.fireContentsChanged(index, index, index);
    }

    public void clear(boolean refresh) {
        list.clear();
        if(refresh) {
            this.refresh();
        }
    }

    public boolean isEmpty() {
        return list.isEmpty(); 
    }
}
