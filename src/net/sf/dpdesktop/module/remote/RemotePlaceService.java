/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.remote;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import net.sf.dpdesktop.service.Context;
import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.service.ServiceAuthException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

/**
 *
 * @author Heiner Reinhardt
 */
public class RemotePlaceService extends DataService<RemotePlaceService> {

    public RemotePlaceService() {
        super("remotePlaces", true, null);
    }

    @Override
    @Deprecated
    public void load(RemotePlaceService dataService) {
        this.document = (Document) dataService.clone();
    }

    public void store(RemotePlaceRepository remotePlaceRepository, Context context) throws MalformedURLException, ServiceAuthException, IOException {

        Document myDocument = DocumentHelper.createDocument(DocumentHelper.createElement("root"));

        for (RemotePlace remotePlace : remotePlaceRepository.getList()) {

            Element remotePlaceElement = DocumentHelper.createElement("remotePlace");
            remotePlaceElement.addElement("company").setText(remotePlace.getCompany());
            remotePlaceElement.addElement("url").setText(remotePlace.getURLAsString());
            remotePlaceElement.addElement("username").setText(remotePlace.getUsername());
            remotePlaceElement.addElement("password").setText(remotePlace.getPassword());

            myDocument.getRootElement().add(remotePlaceElement);

        }

        super.storeDocument(context, myDocument);
    }

    @SuppressWarnings("unchecked")
    public void load(RemotePlaceRepository remotePlaceRepository, Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
        Document myDocument = super.loadDocument(context);

        List<Element> elementList = myDocument.selectNodes("//remotePlace");


        remotePlaceRepository.clear(false);

        for (Element element : elementList) {


            remotePlaceRepository.addElement(
                    element.elementText("company"),
                    element.elementText("url"),
                    element.elementText("username"),
                    element.elementText("password"), false);

        }

        remotePlaceRepository.refresh();

    }

    @Override
    @Deprecated
    public void load(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
