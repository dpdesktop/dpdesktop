package net.sf.dpdesktop.module.remote;

import java.util.LinkedList;
import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.container.ContainerService;
import net.sf.dpdesktop.service.log.LogItemRepository;
import net.sf.dpdesktop.service.log.LogItemService;

/**
 *
 * @author Heiner Reinhardt
 */
public class RemotePlaceServiceProvider extends RemotePlace {
    
    private ContainerService m_containerService;
    private LogItemService m_logItemService;
    private ConfigurationService m_remoteConfigurationService;


    @Override
    public RemotePlaceServiceProvider clone() {

        RemotePlaceServiceProvider remotePlaceServiceProvider = new RemotePlaceServiceProvider(
                this.getCompany(),
                this.getURLAsString(),
                this.getUsername(),
                this.getPassword());

                remotePlaceServiceProvider.getContainerService().load(m_containerService);
                remotePlaceServiceProvider.getLogItemService().load(m_logItemService);
                remotePlaceServiceProvider.getRemoteConfigurationService().load(m_remoteConfigurationService);


        return remotePlaceServiceProvider; 

    }

    public RemotePlaceServiceProvider(
            
            String company,
            String url,
            String user,
            String pass) {
        super(company, url, user, pass);

        this.m_containerService = new ContainerService(this);
        this.m_logItemService = new LogItemService(this);
        this.m_remoteConfigurationService = new ConfigurationService("config", false, this);
    }


    public ContainerService getContainerService() {
        return m_containerService;
    }

    public LogItemService getLogItemService() {
        return m_logItemService;
    }

    public ConfigurationService getRemoteConfigurationService() {
        return m_remoteConfigurationService;
    }

    public Iterable<DataService> getServices() {
        LinkedList<DataService> list = new LinkedList<DataService>();

        list.add(m_containerService);
        list.add(m_logItemService);
        list.add(m_remoteConfigurationService);

        return list; 
    }
}
