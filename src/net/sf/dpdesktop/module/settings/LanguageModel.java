/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.settings;

import com.google.inject.AbstractModule;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import net.sf.dpdesktop.gui.AboutDialog;
import net.sf.dpdesktop.gui.util.LocaleListCellRenderer;
import net.sf.dpdesktop.util.CalendarParser;
import net.sf.dpdesktop.util.Helper;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXStatusBar.Constraint.ResizeBehavior;

/**
 *
 * @author Heiner Reinhardt
 */
public class LanguageModel extends AbstractListModel implements ComboBoxModel {

    private final static String baseName = "net/sf/dpdesktop/i18n/lang";
    private volatile  ResourceBundle i18n = ResourceBundle.getBundle(baseName);
    private LinkedList<Locale> list = new LinkedList<Locale>();
    private Integer selected = null;
    private Locale defaultLocale = new Locale("en", "GB");
    private LinkedList<LocaleListener> localeListenerList = new LinkedList<LocaleListener>();

    public String getString(String key) {

        if(key.equals("AboutDialog.version")) {
            if(Helper.getVersion().equals("trunk")) {
                return i18n.getString(key);
            } else {
                return Helper.getVersion(); 
            }
        }

        return i18n.getString(key);
    }

    public String getDate(String isoDate ) {

        try {

            GregorianCalendar calendar = CalendarParser.getCalendar(isoDate);

            return CalendarParser.getString(calendar, i18n.getString("LanguageModel.format.dateTime"));

        } catch (ParseException ex) {
            Logger.getLogger(this.getClass()).info(ex, ex);
        }


        return isoDate;



    }



    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public LanguageModel() {
        this.addSupportedLocale(defaultLocale);
        this.addSupportedLocale(new Locale("de", "DE"));
        this.addSupportedLocale(new Locale("fr", "FR"));
        this.addSupportedLocale(new Locale("es", "AR"));

    }

    public void addSupportedLocale(Locale locale) {
        list.add(locale);
        Collections.sort(list, new Comparator<Locale>() {

            @Override
            public int compare(Locale o1, Locale o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        fireContentsChanged();
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public Object getElementAt(int index) {
        return list.get(index);
    }

    @Override
    public void setSelectedItem(Object item) {
        selected = list.indexOf(item);
        fireContentsChanged();
    }

    @Override
    public Locale getSelectedItem() {
        Locale locale;
        try {
            locale = list.get(selected);
        } catch (NullPointerException ex) {
            locale = null;
        }
        return locale;
    }

    public void setActiveLanguage(String language, String country) throws LanguageUnsupportedException {
        for (Locale locale : list) {
            if (locale.getLanguage().equals(language) && locale.getCountry().equals(country)) {
                this.setActiveLocale(locale);
                return;
            }
        }

        throw new LanguageUnsupportedException(String.format("Language %s not supported", language));

    }

    private void fireContentsChanged() {
        this.fireContentsChanged(this, 0, list.size() - 1);
    }

    public void refresh() {
        this.fireContentsChanged();
    }

    private void fireLocaleChanged() {
        for (LocaleListener localeListener : localeListenerList) {
            localeListener.updateLocale(this);
        }
    }

    private void setActiveLocale(Locale locale) {
        Locale.setDefault(locale);
        i18n = ResourceBundle.getBundle(baseName, locale);
        this.setSelectedItem(locale);
        this.fireLocaleChanged();



    }

    public void addLocaleListener(LocaleListener localeListener) {
        localeListenerList.add(localeListener);
        localeListener.updateLocale(this);
    }


}
