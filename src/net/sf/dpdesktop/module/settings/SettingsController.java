/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.settings;

import net.sf.dpdesktop.gui.SettingsDialog;
import com.google.inject.Guice;
import java.util.logging.Level;
import net.sf.dpdesktop.*;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import java.awt.Frame;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.tracking.TrackingController;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.exception.ProcolNotSupportedException;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.module.remote.RemotePlaceRepository;
import net.sf.dpdesktop.module.remote.RemotePlaceService;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.util.hash.HashFactory;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

/**
 *
 * @author Heiner Reinhardt
 */
public class SettingsController implements SettingsChangedListener {

    private final MessageModel m_messageHandler;
    private final ConfigurationService m_settingsConfigurationService;
    private final ContextProvider m_contextProvider;
    private final LanguageModel m_languageModel;
    private final RemotePlaceRepository m_remotePlaceRepositoryClone;
    private final SettingsDialog m_settingsDialog;
    private final RemotePlaceRepository m_remotePlaceRepository;
    private final RemotePlaceService m_remotePlaceService;
    private final MainWindow m_mainWindow;

    @Inject
    public SettingsController(
            MainWindow mainWindow, 
            MessageModel messageHandler,
            TrackingController trackingController,
            ContextProvider contextProvider,
            RemotePlaceService remotePlaceService,
            @Named("clone") RemotePlaceRepository remotePlaceRepositoryClone,
            RemotePlaceRepository remotePlaceRepository,
            @Named("settings") ConfigurationService settingsConfigurationService,
            SettingsDialog settingsDialog,
            LanguageModel languageModel) {
        
      

        this.m_mainWindow = mainWindow;
        this.m_remotePlaceService = remotePlaceService;
        this.m_messageHandler = messageHandler;
        this.m_settingsConfigurationService = settingsConfigurationService;
        this.m_contextProvider = contextProvider;
        this.m_languageModel = languageModel;
        this.m_remotePlaceRepositoryClone = remotePlaceRepositoryClone;
        this.m_remotePlaceRepository = remotePlaceRepository;
        this.m_settingsDialog = settingsDialog;

        this.registerComponent(m_settingsDialog);

        try {
            m_settingsConfigurationService.load(contextProvider.getContextFile());

            m_remotePlaceService.load(remotePlaceRepository, m_contextProvider.getContextFile());
            m_remotePlaceRepositoryClone.synchronizeWith(remotePlaceRepository);


        } catch (ServiceAuthException ex) {
            //ignore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error", ex);

        } catch (MalformedURLException ex) {
            //ignore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error", ex);
        } catch (IOException ex) {

            this.setSettingsWithDefaultValues();

            m_messageHandler.warn(ex, languageModel.getString("SettingsController.message.loadSettingsFailed.headline"),
                    languageModel.getString("SettingsController.message.loadSettingsFailed.iOException.text"));
            Logger.getLogger(this.getClass()).warn("IOException while loading settings", ex);

        } catch (DocumentException ex) {

            this.setSettingsWithDefaultValues();

            m_messageHandler.warn(ex, languageModel.getString("SettingsController.message.loadSettingsFailed.headline"),
                    languageModel.getString("SettingsController.message.loadSettingsFailed.documentException.text"));
            Logger.getLogger(this.getClass()).warn("Deleting broken data.", ex);
        }


        if (remotePlaceRepository.isEmpty()) {
            
            remotePlaceRepository.addElement("Test remote place",
                    "http://dpdesktop.sourceforge.net/api/service-0.70/service.php",
                    "dpDesktop",
                    "dpDesktopPW", true);

            remotePlaceRepositoryClone.synchronizeWith(remotePlaceRepository);

        }

        this.setSelectedLanguage();
    }

    public void registerComponent(SettingsComponent settingsComponent) {
        settingsComponent.addSettingsChangedListener(this);
    }

    @Override
    public Boolean settingsChanged(boolean autoUpdate, boolean notifyOnNewVersionAvailable, int maxIdleTime, Locale locale) {
        this.setSettings(autoUpdate, notifyOnNewVersionAvailable, maxIdleTime, locale);

        try {
            m_settingsConfigurationService.store(m_contextProvider.getContextFile());
            m_remotePlaceRepository.synchronizeWith(m_remotePlaceRepositoryClone);
            m_remotePlaceService.store(m_remotePlaceRepository, m_contextProvider.getContextFile());

        } catch (MalformedURLException ex) {
            //ignore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error", ex);
        } catch (ServiceAuthException ex) {
            //ignore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error", ex);
        } catch (IOException ex) {
            m_messageHandler.error(ex, m_languageModel.getString("SettingsController.message.saveSettingsFailed.headline"), m_languageModel.getString("SettingsController.message.loadSettingsFailed.iOException.text"));
        }
        try {
            m_languageModel.setActiveLanguage(locale.getLanguage(), locale.getCountry());
            m_mainWindow.pack();

        } catch (LanguageUnsupportedException ex) {
            //ignore
        }

        m_settingsDialog.setVisible(false);


        return null;

    }

    @Override
    public void cancel() {

        this.setSelectedLanguage();
        m_settingsConfigurationService.refresh();
        m_settingsDialog.setVisible(false);

    }

    private void setSettingsWithDefaultValues() {
        this.setSettings(false, false, 10, m_languageModel.getDefaultLocale());
    }

    private void setSelectedLanguage() {
        try {
            m_languageModel.setActiveLanguage(m_settingsConfigurationService.getValue("language"), m_settingsConfigurationService.getValue("country"));
        } catch (Exception ex) {
            m_languageModel.setSelectedItem(m_languageModel.getDefaultLocale());
        }
    }

    private void setSettings(boolean autoUpdate, boolean notifyOnNewVersionAvailable, int maxIdleTime, Locale locale) {
        m_settingsConfigurationService.setFlag("autoUpdate", autoUpdate);
        m_settingsConfigurationService.setFlag("notifyOnNewVersionAvailable", notifyOnNewVersionAvailable);
        m_settingsConfigurationService.setValue("maxIdleTime", String.valueOf(maxIdleTime));
        m_settingsConfigurationService.setValue("language", locale.getLanguage());
        m_settingsConfigurationService.setValue("country", locale.getCountry());
    }

    @Override
    public void addRemotePlace() {
        m_remotePlaceRepositoryClone.addElement("company", "any url", "any name", "any pass", true);
    }

    @Override
    public boolean removeRemotePlace(RemotePlaceServiceProvider remotePlaceServiceProvider) {
        if (m_remotePlaceRepositoryClone.getSize() == 1) {
            return false;
        } else {
            m_remotePlaceRepositoryClone.removeElement(remotePlaceServiceProvider);
            return true;
        }
    }

    @Override
    public void changeRemotePlaceCompany(RemotePlace remotePlace, String company) {
        m_remotePlaceRepositoryClone.setCompany(remotePlace, company);
    }

    @Override
    public boolean changeRemotePlaceUrl(RemotePlace remotePlace, String url) {
        m_remotePlaceRepositoryClone.setUrl(remotePlace, url);

        try {
            new URL(url);
            return true;
        } catch (MalformedURLException ex) {
            return false;
        }

    }

    @Override
    public void changeRemotePlaceUser(RemotePlace remotePlace, String user) {
        m_remotePlaceRepositoryClone.setUsername(remotePlace, user);
    }

    @Override
    public void changeRemotePlacePass(RemotePlace remotePlace, String pass) {
        m_remotePlaceRepositoryClone.setPassword(remotePlace, pass);
    }

    public void registerStartupComponent(SettingsStartupComponent settingsStartupComponent) {
        settingsStartupComponent.addSettingsStartupListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                m_remotePlaceRepositoryClone.synchronizeWith(m_remotePlaceRepository);

                m_settingsDialog.setVisible(true);
            }
        });
    }
}
