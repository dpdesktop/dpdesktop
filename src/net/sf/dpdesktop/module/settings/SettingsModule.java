/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.settings;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import java.awt.Frame;
import javax.swing.JFrame;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.service.config.ConfigurationService;

/**
 *
 * @author Heiner Reinhardt
 */
public class SettingsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SettingsController.class).in(Scopes.SINGLETON);
        bind(LanguageModel.class).in(Scopes.SINGLETON);

    }

    // e.g. store max idle time locally
    @Provides
    @Singleton
    @Named("settings")
    private ConfigurationService provideSettingsConfigurationService() {
        return new ConfigurationService("settings", true);
    }

    // e.g. store max idle time locally
    @Provides
    @Named("settings")
    private Configuration provideSettingsConfiguration(@Named("settings") ConfigurationService configurationService) {
        return configurationService; 
    }



}
