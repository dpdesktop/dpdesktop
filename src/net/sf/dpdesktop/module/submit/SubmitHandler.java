/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.submit;

import java.io.IOException;
import java.net.MalformedURLException;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.ServiceDataException;
import net.sf.dpdesktop.service.log.LogItem;
import net.sf.dpdesktop.service.log.LogItemService;
import net.sf.dpdesktop.gui.SettingsDialog;
import com.google.inject.Guice;
import java.util.logging.Level;
import net.sf.dpdesktop.*;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import java.awt.Frame;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.gui.submit.LogPane;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.tracking.TrackingController;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.exception.ProcolNotSupportedException;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.module.remote.RemotePlaceRepository;
import net.sf.dpdesktop.module.remote.RemotePlaceService;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.module.update.UpdateController;
import net.sf.dpdesktop.module.util.hash.HashFactory;
import net.sf.dpdesktop.service.log.LogItemServiceFactory;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

/**
 *
 * @author Heiner Reinhardt
 */
public class SubmitHandler {
    private final LanguageModel m_languageModel;

    @Inject
    public SubmitHandler(LanguageModel languageModel) {
        this.m_languageModel = languageModel;
    }

    public boolean submit(LogItemServiceFactory logItemServiceFactory, 
            TrackingManager trackingManager,
            MessageModel messagModel,
            LogPane logPane,
            ContextProvider contextProvider,
            UpdateController updateController,
            String submitFailedHeadline) {


        LogItemService logItemService = logItemServiceFactory.createLogItemService(trackingManager);
        
        LogItem logItem = trackingManager.createLogItem(logPane.getSummary(),
                logPane.getComment(),
                logPane.getComplete(),
                logPane.getWorkedTimeHours(),
                logPane.getWorkedTimeMinutes(),
                logPane.getBillableTimeHours(),
                logPane.getBillableTimeMinutes());


        boolean success = false;
        try {
            success = logItemService.store(contextProvider.getContextHttp(), logItem);
        } catch (ServiceDataException ex) {
            messagModel.error(ex, submitFailedHeadline, m_languageModel.getString("SubmitHandler.message.submitFailed.serviceDataException.text"));
        } catch (MalformedURLException ex) {
            messagModel.error(ex, submitFailedHeadline, m_languageModel.getString("SubmitHandler.message.submitFailed.malformedURLException.text"));
        } catch (ServiceAuthException ex) {
            messagModel.error(ex, submitFailedHeadline, m_languageModel.getString("SubmitHandler.message.submitFailed.serviceAuthException.text"));
        } catch (IOException ex) {
            messagModel.error(ex, submitFailedHeadline, m_languageModel.getString("SubmitHandler.message.submitFailed.iOException.text"));
        }
        if (success) {
            updateController.update( (RemotePlaceServiceProvider) trackingManager.getTrackableContainer().getCreator().getRemotePlace());
            trackingManager.reset();
        }
        return success;
    }
}
