/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.tracking;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.awt.AWTPermission;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.Timer;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.service.config.ConfigurationListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class IdleController {

    private final TrackingManager m_trackingManager;
    private final ActionListener mouseInfoListener = new ActionListener() {

        /**
         * The previous mouse position as a field
         */
        private Point prevPoint;
        /**
         * The current mouse position as a field
         */
        private Point currentPoint;

        @Override
        public void actionPerformed(ActionEvent e) {
            currentPoint = MouseInfo.getPointerInfo().getLocation();



            if (!currentPoint.equals(prevPoint)) {
                m_trackingManager.resetAutoIdleTimer();
            }

            prevPoint = currentPoint;

        }
    };
    private Timer idleTimer = new Timer(100, mouseInfoListener);
    private boolean autoIdleSupported = false;

    @Inject
    public IdleController(TrackingManager trackingManager, @Named("settings") Configuration configuration) {
        m_trackingManager = trackingManager;

        try {

            try {
                System.getSecurityManager().checkPermission(new AWTPermission("watchMousePointer"));
            } catch (NullPointerException ex) {
                //don't care
                Logger.getLogger(this.getClass()).debug("IT's OK. This exception is just displayed for DEBUG purposes and normally can be IGNORED", ex);
            }

            this.setAutoIdleSupported(true);

        } catch (SecurityException ex) {

            this.setAutoIdleSupported(false);

            Logger.getLogger(this.getClass()).warn("Access to mouse pointer position is not allowed", ex);
        }



        configuration.addUniqueConfigurationListener(new ConfigurationListener("maxIdleTime") {

            @Override
            public void valueChanged(String value) {
                setMaxIdleTime(Integer.valueOf(value));
            }

            @Override
            public void flagChanged(boolean b) {
                // don't care
            }
        });


    }

    private void setMaxIdleTime(int seconds) {

        if (this.isAutoIdleSupported()) {

            m_trackingManager.setAutoIdleTimeInMinutes(seconds);
            m_trackingManager.setAutoIdleEnabled(seconds != 0);
            idleTimer.start();

        }
    }

    private void setAutoIdleSupported(boolean supported) {
        this.autoIdleSupported = supported;
    }

    public boolean isAutoIdleSupported() {
        return this.autoIdleSupported;
    }
}
