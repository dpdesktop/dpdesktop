/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.tracking;

import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.service.log.LogItem;

/**
 *
 * @author Heiner Reinhardt
 */
public interface Tracker {

    public void addListener(TrackingListener l);

    /**
     * Returns the hours of the tracked time.
     * @return Hour
     */
    public int getHours();

    /**
     * Returns the minutes of the tracked time.
     * @return Minute
     */
    public int getMinutes();

    public int getSeconds();

    /**
     * Returns the tracked time.
     * @return time in seconds
     */
    public int getTime();

    /**
     * Return time in format HH:mm:ss
     * @return String
     */
    public String getTimeAsString();

    /**
     * Getter for the current container tracked
     * @return Any container
     */
    public Container getTrackableContainer();

    public boolean isCleared();

    public boolean isInIdle();


    public LogItem createLogItem(String summary, String comment, int complete, int workedH, int workedM, int billableH, int billableM);

    public boolean wasInIdle();

    public boolean isRunning();



}
