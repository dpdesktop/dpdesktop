/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.tracking;

import com.google.inject.Guice;
import java.util.logging.Level;
import net.sf.dpdesktop.*;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import net.sf.dpdesktop.gui.submit.ContainerChangedBar;
import net.sf.dpdesktop.gui.submit.DefaultBar;
import net.sf.dpdesktop.gui.submit.DefaultSubmitDialog;
import net.sf.dpdesktop.gui.HistoryPane;
import net.sf.dpdesktop.gui.submit.LogPane;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.gui.SelectionPane;
import net.sf.dpdesktop.gui.TrackingPane;
import net.sf.dpdesktop.gui.CommonTray;
import net.sf.dpdesktop.module.guistate.Tray;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.container.ContainerRepositoryListener;
import net.sf.dpdesktop.service.container.ContainerService;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class TrackingController implements TrackingListener, ContainerRepositoryListener {

    private final TrackingManager m_trackingManager;
    private final ContainerRepository m_containerRepository;

    @Inject
    public TrackingController(
            TrackingManager trackingManager,
            ContainerRepository containerRepository) {

        Logger.getLogger(this.getClass()).debug("Tracking Controller created");

        this.m_trackingManager = trackingManager;
        this.m_containerRepository = containerRepository;

        m_containerRepository.addContainerRepositoryListener(this);
    }

    public Tracker getTracker() {
        return m_trackingManager;
    }

    public void registerComponent(TrackingComponent trackingComponent) {
        trackingComponent.addTrackingListener(this);
    }

    public TrackingManager getTrackingManager() {
        return m_trackingManager;
    }

    // TrackingListener
    @Override
    public void containerChanged(Container container) {
        if (container == null) {
            return;
        }

        if (container.equals(m_trackingManager.getTrackableContainer())) {
            return;
        }

        try {
            m_trackingManager.setTrackableContainer(container);
        } catch (ContainerNotTrackableException ex) {
            Logger.getLogger(this.getClass()).debug(String.format("container \"%s\" is not trackable. Ignoring...", container.getName()), ex);
        }


    }

    @Override
    public void start() {
        m_trackingManager.start();
    }

    @Override
    public void stop() {
        m_trackingManager.stop();
    }

    @Override
    public void clear() {
        m_trackingManager.clear();
    }

    @Override
    public void timeChanged() {
        //
    }

    @Override
    public void ready() {
        //
    }

    @Override
    public void unready() {
        //
    }

    @Override
    public void commentChanged(String comment) {
        m_trackingManager.setComment(comment);
    }

    @Override
    public void summaryChanged(String summary) {
        m_trackingManager.setSummary(summary);
    }

    @Override
    public void completeChanged(int complete) {
        m_trackingManager.setComplete(complete);
    }

    @Override
    public void rootContainerChanged(Container container) {

        Container c = m_containerRepository.recovery(m_trackingManager.getTrackableContainer());

        if(c == null) {
            this.m_trackingManager.unready();
        } else {
            try {
                this.m_trackingManager.setTrackableContainer(container);
            } catch (ContainerNotTrackableException ex) {
                this.m_trackingManager.unready();
            }
        }


    }
}
