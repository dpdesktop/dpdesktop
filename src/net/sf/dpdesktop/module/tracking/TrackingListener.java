/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.tracking;

import net.sf.dpdesktop.service.container.Container;

/**
 *
 * @author Heiner Reinhardt
 */
public interface TrackingListener {

    public void containerChanged(Container container);

    public void start();
    public void stop();
    public void clear();

    public void timeChanged();

    public void ready();

    public void unready();

    public void commentChanged(String comment);

    public void summaryChanged(String summary);

    public void completeChanged(int complete);

}
