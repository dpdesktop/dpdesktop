/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.tracking;

import net.sf.dpdesktop.service.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.Timer;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import net.sf.dpdesktop.service.log.LogItem;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class TrackingManager implements Tracker {

    protected int trackedTime = 0;
    private TrackingManagerState lastState = TrackingManagerState.UNREADY; 
    private TrackingManagerState state = TrackingManagerState.UNREADY;
    protected Container container = null;
    protected int currentIdleTime = 0;
    protected int maxIdleTime = 5;
    protected boolean autoIdleEnabled = false;
    protected Timer timer = new Timer(1000, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {

            addTimeOffset(1);

            if (autoIdleEnabled) {
                currentIdleTime = currentIdleTime + 1;
                state.idle(TrackingManager.this);
            }
        }
    });
    private String comment = "";
    private String summary = "";
    private int complete = 0;
    private List<VetoableContainerChangedListener> vetoableContainerChangedListenerList = new LinkedList<VetoableContainerChangedListener>();

    protected void setState(TrackingManagerState nextState) {
        this.state.leave(this);
        this.lastState = this.state;
        this.state = nextState;
        this.state.init(this);

        Logger.getLogger(this.getClass()).debug("Current state is: " + nextState);

    }

    protected TrackingManagerState getState() {
        return this.state;
    }

    protected void addTimeOffset(int seconds) {
        trackedTime = trackedTime + seconds;
        this.fireTimeChangedEvent();
    }

    public void stop() {
        state.stop(this);
    }

    public void start() {
        state.start(this);
    }

    protected void idle() {
        state.idle(this);
    }

    public boolean isInIdle() {
        return state == TrackingManagerState.IDLE;
    }

    public boolean wasInIdle() {
        return lastState == TrackingManagerState.IDLE; 
    }

    protected void clear() {
        this.trackedTime = 0;
        this.fireTimeChangedEvent();
        state.stop(this);
        this.fireClearedEvent();

        comment = "";
        summary = "";

        this.fireSummaryChanged(summary);
        this.fireCommentChanged(comment);
    }

    public boolean isCleared() {
        return trackedTime == 0;
    }

    protected void unready() {
        state.unready(this);
    }

    public boolean isUnready() {
        return state == TrackingManagerState.UNREADY;
    }

    /**
     * Getter for the current container tracked
     * @return Any container
     */
    public Container getTrackableContainer() {
        return this.container;
    }

    protected void setTrackableContainer(Container container) throws ContainerNotTrackableException {

        if (!container.isTrackable()) {
            throw new ContainerNotTrackableException("The selected objective is not trackable. You may choose another one.");
        }


        if (isContainerChangeAllowed(container)) {

            this.fireContainerChangedEvent(container);
            this.container = container;

            if (this.isUnready()) {
                state.stop(this);
            }

            this.setComplete(Integer.valueOf(container.get("complete", "0")));
        } else {
            refresh();
        }


    }

    /**
     * Enable/Disable auto tracking stop
     * @param enable true - enable; false - disable
     */
    protected void setAutoIdleEnabled(boolean enable) {
        autoIdleEnabled = enable;
    }

    /**
     * Sets the idle time. After this time the tracker will be stopped
     * automatically.
     * @param seconds
     */
    protected void setAutoIdleTimeInMinutes(int minutes) {
        this.setAutoIdleTimeInSeconds(minutes * 60);
    }

    protected void setAutoIdleTimeInSeconds(int seconds) {
        maxIdleTime = seconds;
    }
    /**
     * Sets the time since last reset to zero.
     */
    protected void resetAutoIdleTimer() {
        currentIdleTime = 0;
        if (isInIdle()) {
            this.start();
        }
    }

    /**
     * Returns the hours of the tracked time.
     * @return Hour
     */
    public int getHours() {
        int h = 0;
        if (trackedTime > 0) {
            h = trackedTime / 3600;
        }
        return h;
    }

    /**
     * Returns the minutes of the tracked time.
     * @return Minute
     */
    public int getMinutes() {
        int m = 0;
        if (trackedTime > 0) {
            m = (trackedTime % 3600) / 60;
        }
        return m;
    }

    /**
     * Returns the seconds of the tracked time.
     * @return Second
     */
    public int getSeconds() {
        int s = 0;
        if (trackedTime > 0) {
            s = trackedTime % 3600 % 60;
        }
        return s;
    }

    /**
     * Returns the tracked time.
     * @return time in seconds
     */
    public int getTime() {
        return trackedTime;
    }

    /**
     * Return time in format HH:mm:ss
     * @return String
     */
    public synchronized String getTimeAsString() {
        return String.format("%02d:%02d:%02d", getHours(), getMinutes(), getSeconds());
    }
    //
    // LISTENER API
    //
    private Vector<TrackingListener> listener = new Vector<TrackingListener>();

    public void addListener(TrackingListener l) {
        listener.add(l);

        state.init(this);

        l.commentChanged(comment);
        l.summaryChanged(summary);

        l.timeChanged();

    }

    public boolean removeListener(TrackingListener l) {
        return listener.remove(l);
    }

    protected void fireTimeChangedEvent() {
        for (TrackingListener l : listener) {
            l.timeChanged();
        }
    }

    protected void fireContainerChangedEvent(Container container) {
        for (TrackingListener l : listener) {
            l.containerChanged(container);
        }
    }

    protected void fireStoppedEvent() {
        for (TrackingListener l : listener) {
            l.stop();
        }
    }

    protected void fireStartedEvent() {
        for (TrackingListener l : listener) {
            l.start();
        }
    }

    protected void fireClearedEvent() {
        for (TrackingListener l : listener) {
            l.clear();
        }
    }

    protected void fireReadyEvent() {
        for (TrackingListener l : listener) {
            l.ready();
        }
    }

    protected void fireUnreadyEvent() {
        for (TrackingListener l : listener) {
            l.unready();
        }
    }

    @Override
    public LogItem createLogItem(String summary, String comment, int complete, int workedH, int workedM, int billableH, int billableM) {

        LogItem item = new LogItem(summary, comment, container);

        item.put("workedTime", String.format("%02d:%02d", workedH, workedM));
        item.put("billableTime", String.format("%02d:%02d", billableH, billableM));

        item.put("complete", String.format("%02d", complete));


        return item;
    }

    public void reset() {
        this.setSummary("");
        this.setComment("");
        this.unready();
    }

    public void setComment(String comment) {
        this.comment = comment;

        this.fireCommentChanged(comment);

    }

    public void setSummary(String summary) {
        this.summary = summary;

        this.fireSummaryChanged(summary);
    }

    public void setComplete(int complete) {
        this.complete = complete;

        this.fireCompleteChanged(complete);
    }

    private void fireCommentChanged(String comment) {
        for (TrackingListener l : listener) {
            l.commentChanged(comment);
        }
    }

    private void fireSummaryChanged(String summary) {
        for (TrackingListener l : listener) {
            l.summaryChanged(summary);
        }
    }

    public void refresh() {
        state.init(this);
        fireCommentChanged(comment);
        fireSummaryChanged(summary);
        fireCompleteChanged(complete);
    }

    private void fireCompleteChanged(int complete) {
        for (TrackingListener l : listener) {
            l.completeChanged(complete);
        }
    }

    public void addVetoableContainerChangedListener(VetoableContainerChangedListener vetoableContainerChangedListener) {
        vetoableContainerChangedListenerList.add(vetoableContainerChangedListener);
    }

    private boolean isContainerChangeAllowed(Container newContainer) {
        boolean containerChangedAllowed = true;
        for (VetoableContainerChangedListener vetoableContainerChangedListener : vetoableContainerChangedListenerList) {
            containerChangedAllowed = containerChangedAllowed && vetoableContainerChangedListener.isContainerChangeAllowed(newContainer);
        }
        return containerChangedAllowed;
    }

    @Override
    public boolean isRunning() {
        return state == TrackingManagerState.RUNNING; 
    }


}
