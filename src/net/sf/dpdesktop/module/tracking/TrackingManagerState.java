/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.tracking;

import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public enum TrackingManagerState {

    STOPPED {

        @Override
        public void init(TrackingManager trackingManager) {
            trackingManager.timer.stop();
            trackingManager.fireStoppedEvent();
        }

        @Override
        public void start(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.RUNNING);
        }

        @Override
        public void stop(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.STOPPED);
        }

        @Override
        public void idle(TrackingManager trackingManager) {
            // cannot switch from STOPPED to IDLE mode
        }

        @Override
        public void unready(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.UNREADY);
        }

        @Override
        public void leave(TrackingManager trackingManager) {
        }
    },
    RUNNING {

        @Override
        public void init(TrackingManager trackingManager) {
            trackingManager.timer.start();
            trackingManager.fireStartedEvent();
        }

        @Override
        public void start(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.RUNNING);
        }

        @Override
        public void stop(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.STOPPED);
        }

        @Override
        public void idle(TrackingManager trackingManager) {

            Logger.getLogger(this.getClass()).debug(String.format("Current idle time | max idle time: %s | %s ", trackingManager.currentIdleTime, trackingManager.maxIdleTime));

            if (trackingManager.currentIdleTime >= trackingManager.maxIdleTime) {
                trackingManager.setState(TrackingManagerState.IDLE);
            }

        }

        @Override
        public void unready(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.UNREADY);

        }

        @Override
        public void leave(TrackingManager trackingManager) {
        }
    },
    IDLE {

        @Override
        public void init(TrackingManager trackingManager) {
            trackingManager.timer.stop();
            trackingManager.fireStoppedEvent();
        }

        @Override
        public void start(TrackingManager trackingManager) {
            trackingManager.currentIdleTime = 0;
            trackingManager.setState(TrackingManagerState.RUNNING);
        }

        @Override
        public void stop(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.STOPPED);
        }

        @Override
        public void idle(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.IDLE);
        }

        @Override
        public void unready(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.UNREADY);
        }

        @Override
        public void leave(TrackingManager trackingManager) {
            trackingManager.currentIdleTime = 0;
        }
    },
    /**
     * TrackingManager is unready when there is no container for tracking. If tracked time is zero, the
     * current tracking manager state will be UNREADY anyway
     */
    UNREADY {

        @Override
        public void init(TrackingManager trackingManager) {
            trackingManager.container = null;
            trackingManager.timer.stop();
            trackingManager.fireUnreadyEvent();
            trackingManager.setComplete(0);

        }

        @Override
        public void start(TrackingManager trackingManager) {
            if (trackingManager.container != null) {
                trackingManager.setState(TrackingManagerState.RUNNING);
            }
        }

        @Override
        public void stop(TrackingManager trackingManager) {
            if (trackingManager.container != null) {
                trackingManager.setState(TrackingManagerState.STOPPED);
            }
        }

        @Override
        public void idle(TrackingManager trackingManager) {
            // cannot switch from UNREADY to IDLE mode
        }

        @Override
        public void unready(TrackingManager trackingManager) {
            trackingManager.setState(TrackingManagerState.UNREADY);
        }

        @Override
        public void leave(TrackingManager trackingManager) {
            trackingManager.fireReadyEvent();
        }
    };

    public abstract void init(TrackingManager trackingManager);

    public abstract void start(TrackingManager trackingManager);

    public abstract void stop(TrackingManager trackingManager);

    public abstract void idle(TrackingManager trackingManager);

    public abstract void unready(TrackingManager trackingManager);

    public abstract void leave(TrackingManager trackingManager);
}
