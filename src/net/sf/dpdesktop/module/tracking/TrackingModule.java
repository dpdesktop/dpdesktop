/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.tracking;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import net.sf.dpdesktop.service.config.Configuration;


/**
 *
 * @author Heiner Reinhardt
 */
public class TrackingModule extends AbstractModule {


    @Override
    protected void configure() {
        bind(TrackingController.class).in(Scopes.SINGLETON);
        bind(TrackingManager.class).in(Scopes.SINGLETON);
        bind(IdleController.class).in(Scopes.SINGLETON);
    }


    @Provides
    private Tracker provideTracker(TrackingController trackingController) {
        return trackingController.getTracker();
    }

    @Named("isAutoIdleSupported")
    @Provides
    private Boolean isAutoIdleSupported(IdleController idleController) {
        return idleController.isAutoIdleSupported();
    }
    




}
