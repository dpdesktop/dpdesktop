/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.update;

import java.util.logging.Level;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.log.LogItemService;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.container.ContainerService;
import net.sf.dpdesktop.service.config.Configuration;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ResourceBundle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.GregorianCalendar;
import net.sf.dpdesktop.gui.DatabasePane;
import net.sf.dpdesktop.gui.HistoryPane;
import net.sf.dpdesktop.gui.submit.LogPane;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.gui.SelectionPane;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.progress.AbstractProgressController;
import net.sf.dpdesktop.module.progress.ProgressModel;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.module.tracking.ContainerNotTrackableException;
import net.sf.dpdesktop.module.tracking.Tracker;
import net.sf.dpdesktop.service.exception.ProcolNotSupportedException;
import net.sf.dpdesktop.service.exception.ResponseCodeException;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.Context;
import net.sf.dpdesktop.service.container.ContainerRepository;
import net.sf.dpdesktop.service.log.LogItemRepository;
import net.sf.dpdesktop.module.remote.RemotePlaceRepository;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.util.CalendarParser;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

/**
 *
 * @author Heiner Reinhardt
 */
public class UpdateController {

    private ConfigurationService m_infoConfigurationService;
    private ContextProvider m_contextProvider;
    private MessageModel m_messageHandler;
    private Configuration m_settingsConfiguration;
    private final RemotePlaceRepository m_remotePlaceRepository;
    private final ProgressModel m_progressModel;
    private final LanguageModel m_languageModel;

    /**
     * This method should be called only at application start. Normally it would be called inside this constructor
     */
    public void init() {

        new AbstractProgressController(m_progressModel) {

            @Override
            protected Void doInBackground() {
                boolean anySuccess = false;
                if (m_settingsConfiguration.getFlag("autoUpdate", false)) {
                    for (RemotePlaceServiceProvider provider : m_remotePlaceRepository.getList()) {
                        boolean success = false;
                        success = UpdateController.this.proceedLoadHttp(provider, true);
                        if (success) {
                            anySuccess = true;
                            UpdateController.this.proceedStoreFile(provider);
                        } else {
                            UpdateController.this.proceedLoadFile(provider);
                        }
                    }
                } else {
                    for (RemotePlaceServiceProvider provider : m_remotePlaceRepository.getList()) {
                        UpdateController.this.proceedLoadFile(provider);
                    }
                }
                if (anySuccess) {
                    UpdateController.this.setLastSyncDateToNow();
                } else {
                    try {
                        m_infoConfigurationService.load(m_contextProvider.getContextFile());
                    } catch (ServiceAuthException ex) {
                        //ignore
                        Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any authentication problems");
                    } catch (MalformedURLException ex) {
                        //ignore
                        Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any malformed url exception");
                    } catch (IOException ex) {
                        //ignore
                        Logger.getLogger(this.getClass()).debug("Exception only relevant for debugging", ex);
                    } catch (DocumentException ex) {
                        Logger.getLogger(this.getClass()).debug("Exception only relevant for debugging", ex);
                    }
                }
                m_remotePlaceRepository.refresh();
                return null;
            }
        }.execute();

    }

    public void update() {

        new AbstractProgressController(m_progressModel) {

            @Override
            protected Void doInBackground() {
                boolean anySuccess = false;



                for (RemotePlaceServiceProvider provider : m_remotePlaceRepository.getList()) {
                    anySuccess = UpdateController.this.proceedUpdate(this.getProgressModel(), provider);
                }

                if (anySuccess) {
                    UpdateController.this.setLastSyncDateToNow();
                    m_remotePlaceRepository.refresh();
                }


                return null;
            }
        }.execute();


    }

    public void update(final RemotePlaceServiceProvider remotePlaceServiceProvider) {
        new AbstractProgressController(m_progressModel) {

            @Override
            protected Void doInBackground() {
                boolean success = UpdateController.this.proceedUpdate(m_progressModel, remotePlaceServiceProvider);
                
                if(success) {
                    UpdateController.this.setLastSyncDateToNow();
                    m_remotePlaceRepository.refresh();
                }
                
                return null; 
            }
        }.execute();
    }

    public void update(final DataService dataService) {

        final RemotePlace remotePlace = dataService.getRemotePlace();

        new AbstractProgressController(m_progressModel) {

            @Override
            protected Void doInBackground() {

                adjustProgressModel(m_progressModel, remotePlace, 20);

                try {
                    dataService.load(m_contextProvider.getContextHttp());

                    m_remotePlaceRepository.refresh();

                } catch (ServiceAuthException ex) {
                    //ignoring
                    Logger.getLogger(this.getClass()).warn(ex, ex);
                } catch (MalformedURLException ex) {
                    //ignoring
                    Logger.getLogger(this.getClass()).warn(ex, ex);
                } catch (IOException ex) {
                    //ignoring
                    Logger.getLogger(this.getClass()).warn(ex, ex);
                } catch (DocumentException ex) {
                    //ignoring
                    Logger.getLogger(this.getClass()).warn(ex, ex);
                }

                adjustProgressModel(m_progressModel, remotePlace, 80);




                return null;
            }
        }.execute();
    }

    private void adjustProgressModel(ProgressModel progressModel, RemotePlace remotePlace, int value) {
        progressModel.setRunning(m_languageModel.getString("UpdateController.progress.text").replace("%COMPANY%", remotePlace.getCompany()), value);
    }

    private boolean proceedUpdate(ProgressModel progressModel, RemotePlaceServiceProvider provider) {

        boolean anySuccess = false;

        adjustProgressModel(progressModel, provider, 20);

        boolean success = UpdateController.this.proceedLoadHttp(provider, false);
        if (success) {
            anySuccess = true;
            UpdateController.this.proceedStoreFile(provider);
        }

        adjustProgressModel(progressModel, provider, 80);

        return anySuccess;
    }

    private void proceedLoadFile(RemotePlaceServiceProvider provider) {
        // IF UPDATE FAILED, LOAD FROM CACHE
        try {
            this.load(provider, m_contextProvider.getContextFile());
        } catch (ServiceAuthException ex) {
            //ignore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any authentication problems");
        } catch (MalformedURLException ex) {
            //ignore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any malformed url exception");
        } catch (IOException ex) {
            m_messageHandler.warn(ex, m_languageModel.getString("UpdateController.message.loadFailed.headline"), m_languageModel.getString("UpdateController.message.loadFailed.iOException.text"));
        } catch (DocumentException ex) {
            this.clean(provider, m_contextProvider.getContextFile());
            m_messageHandler.warn(ex, m_languageModel.getString("UpdateController.message.loadFailed.headline"), m_languageModel.getString("UpdateController.message.loadFailed.documentException.text"));
            Logger.getLogger(this.getClass()).warn("Deleting broken data.", ex);
        }
    }

    private void proceedStoreFile(RemotePlaceServiceProvider provider) {
        try {
            this.store(provider, m_contextProvider.getContextFile());
        } catch (MalformedURLException ex) {
            Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any malformed url exception");
        } catch (ServiceAuthException ex) {
            Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any authentication problems");
        } catch (IOException ex) {
            m_messageHandler.warn(ex, m_languageModel.getString("UpdateController.message.saveFailed.headline"), m_languageModel.getString("UpdateController.message.loadFailed.iOException.text"));
            Logger.getLogger(this.getClass()).warn(ex, ex);
        }
    }

    private boolean proceedLoadHttp(RemotePlaceServiceProvider provider, boolean callingFunctionWillLoadFromFileAsFallback) {
        String extendedMessage = "";

        if (callingFunctionWillLoadFromFileAsFallback) {
            extendedMessage = " " + m_languageModel.getString("UpdateController.message.loadFailed.fallback.text");
        }

        boolean success = false;
        final String headline = m_languageModel.getString("UpdateController.message.updateFailed.headline").replace("%COMPANY%", provider.getCompany());
        try {
            success = this.load(provider, m_contextProvider.getContextHttp());
        } catch (ServiceAuthException ex) {
            m_messageHandler.error(ex, headline,
                    m_languageModel.getString("UpdateController.message.updateFailed.serviceAuthException.text") + extendedMessage);
            Logger.getLogger(this.getClass()).warn(ex, ex);

        } catch (MalformedURLException ex) {
            m_messageHandler.error(ex, headline,
                    m_languageModel.getString("UpdateController.message.updateFailed.malformedURLException.text").replace("%URL%", provider.getURLAsString()) + extendedMessage);
            Logger.getLogger(this.getClass()).warn(ex, ex);

        } catch (IOException ex) {
            m_messageHandler.error(ex, headline,
                    m_languageModel.getString("UpdateController.message.updateFailed.iOException.text").replace("%URL%", provider.getURLAsString()) + extendedMessage);
            Logger.getLogger(this.getClass()).warn(ex, ex);

        } catch (DocumentException ex) {
            m_messageHandler.error(ex, headline,
                    m_languageModel.getString("UpdateController.message.updateFailed.documentException.text") + extendedMessage);
            Logger.getLogger(this.getClass()).warn(ex, ex);
        }

        return success;
    }

    private boolean store(RemotePlaceServiceProvider provider, Context context) throws MalformedURLException, ServiceAuthException, IOException {

        ContainerService containerService = provider.getContainerService();
        LogItemService logItemService = provider.getLogItemService();
        ConfigurationService remoteConfigurationService = provider.getRemoteConfigurationService();

        containerService.store(context);
        logItemService.store(context);
        remoteConfigurationService.store(context);

        return true;
    }

    private void clean(RemotePlaceServiceProvider provider, Context context) {

        ContainerService containerService = provider.getContainerService();
        LogItemService logItemService = provider.getLogItemService();
        ConfigurationService remoteConfigurationService = provider.getRemoteConfigurationService();

        containerService.clean(context);
        logItemService.clean(context);
        remoteConfigurationService.clean(context);
    }

    private boolean load(RemotePlaceServiceProvider provider, Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {

        ContainerService containerService = provider.getContainerService();
        LogItemService logItemService = provider.getLogItemService();
        ConfigurationService remoteConfigurationService = provider.getRemoteConfigurationService();

        ContainerService containerServiceClone = containerService.clone();
        LogItemService logItemServiceClone = logItemService.clone();
        ConfigurationService remoteConfigurationServiceClone = remoteConfigurationService.clone();

        containerServiceClone.load(context);
        logItemServiceClone.load(context);
        remoteConfigurationServiceClone.load(context);

        containerService.load(containerServiceClone);
        logItemService.load(logItemServiceClone);
        remoteConfigurationService.load(remoteConfigurationServiceClone);

        return true;

    }

    @Inject
    public UpdateController(
            MessageModel messageHandler,
            @Named("settings") Configuration settingsConfiguration,
            RemotePlaceRepository remotePlaceRepository,
            ContainerRepository containerRepository,
            LogItemRepository logItemRepository,
            ContextProvider contextProvider,
            ProgressModel progressModel,
            LanguageModel languageModel,
            @Named("info") ConfigurationService infoConfigurationService) {


        this.m_languageModel = languageModel;
        this.m_progressModel = progressModel;
        this.m_messageHandler = messageHandler;
        this.m_settingsConfiguration = settingsConfiguration;
        this.m_infoConfigurationService = infoConfigurationService;
        this.m_contextProvider = contextProvider;
        this.m_remotePlaceRepository = remotePlaceRepository;


    }

    private void setLastSyncDateToNow() {
        GregorianCalendar calendar = new GregorianCalendar();

        m_infoConfigurationService.setValue("lastSyncDate", CalendarParser.getString(calendar));

        try {
            m_infoConfigurationService.store(m_contextProvider.getContextFile());
        } catch (MalformedURLException ex) {
            //ignnore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any malformed url exception");
        } catch (ServiceAuthException ex) {
            //ignore
            Logger.getLogger(this.getClass()).fatal("Hardcoded error: Normally, accessing a file should not lead to any malformed url exception");
        } catch (IOException ex) {
            Logger.getLogger(this.getClass()).warn("Date of last synchronization could not be stored", ex);
            m_messageHandler.info(ex, m_languageModel.getString("UpdateController.message.saveLastSyncDateFailed.headline"), m_languageModel.getString("UpdateController.message.saveLastSyncDateFailed.IOException.text"));
        }

    }

    public void registerComponent(UpdateComponent updateComponent) {
        updateComponent.addUpdateListener(new UpdateListener() {

            @Override
            public void update() {
                UpdateController.this.update();
            }
        });
    }
}
