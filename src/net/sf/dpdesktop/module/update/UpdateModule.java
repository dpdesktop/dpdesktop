

package net.sf.dpdesktop.module.update;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.service.config.ConfigurationService;

/**
 *
 * @author Heiner Reinhardt
 */
public class UpdateModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(UpdateController.class).in(Scopes.SINGLETON); 
    }



    @Provides
    @Named("info")
    @Singleton
    private ConfigurationService provideInfoConfigurationService() {
        return new ConfigurationService("info", true);
    }
    // e.g. store last update date

    @Provides
    @Named("info")
    private Configuration provideInfoConfigurationService(@Named("info") ConfigurationService configurationService) {
        return configurationService; 
    }

}
