/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.util.hash;

import com.google.inject.Inject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.settings.LanguageModel;
import org.apache.log4j.Logger;

/**
 *
 * @author Heiner Reinhardt
 */
public class HashController {
    private MessageModel m_messageHandler;
    private HashFactory hash;
    private final LanguageModel m_languageModel;
    private final ApplicationStateModel m_applicationStateModel;

    @Inject
    public HashController(MessageModel messageHandler, LanguageModel languageModel, ApplicationStateModel applicationStateModel) {

        m_languageModel = languageModel; 
        m_messageHandler = messageHandler;
        m_applicationStateModel = applicationStateModel;

        try {
            hash = new HashFactory();
        } catch (NoSuchAlgorithmException ex) {
            m_messageHandler.error(ex, m_languageModel.getString("HashController.message.algorithmIsMissing.headline"), m_languageModel.getString("HashController.message.algorithmIsMissing.text"));

            Logger.getLogger(this.getClass()).fatal("Fatal error: MD5 Algorithm is not supported. Application is closed.",ex);
            
            m_applicationStateModel.forceExit();
        }
    }


    public HashFactory getHashFactory() {
        return hash;
    }
}
