/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.util.ssl;

import com.google.inject.Inject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.service.ContextHttp;
import net.sf.dpdesktop.service.ContextProvider;
import net.sourceforge.jcetaglib.lib.CertTools;
import org.apache.log4j.Logger;
/**
 *
 * @author Heiner Reinhardt
 */
public class HttpsController implements HostnameVerifier, X509TrustManager {

    private boolean enableHttps = false;
    private final MessageModel m_messageModel;
    private final SSLController m_sslController;

    @Inject
    public HttpsController(MessageModel messageModel, SSLController sslController, ContextProvider contextProvider, LanguageModel languageModel) {

        this.m_messageModel = messageModel;
        this.m_sslController = sslController;


        // see: http://forums.sun.com/thread.jspa?threadID=521779 for further information
        HttpsURLConnection.setDefaultHostnameVerifier(this);

        // see: http://www.casabac.com/pdf/CasabacInfoSSL.pdf for further information
        TrustManager[] trustManagerArray = new TrustManager[]{this};


        try {
            SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");

            try {
                
                sc.init(null, trustManagerArray, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            } catch (KeyManagementException ex) {
                Logger.getLogger(this.getClass()).fatal(ex, ex);
            }

            this.enableHttps = true;


        } catch (NoSuchAlgorithmException ex) {
            this.enableHttps = false;

            m_messageModel.info(ex, languageModel.getString("HttpsController.message.httpsNotSupported.headline"), languageModel.getString("HttpsController.message.httpsNotSupported.text"));

            Logger.getLogger(this.getClass()).fatal(ex, ex);


        }

        contextProvider.getContextHttp().setHttpSupported(enableHttps);

    }



    @Override
    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[]{};
    }

    @Override
    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
    }

    @Override
    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
    }



    @Override
    public boolean verify(String hostname, SSLSession session) {

        boolean isAllowed = false;

        try {

            isAllowed = m_sslController.isCertificateAllowed(hostname, (X509Certificate) session.getPeerCertificates()[0]);

        } catch (SSLPeerUnverifiedException ex) {
            
            isAllowed = m_sslController.isUntrustedConnectionAllowed(hostname);

            Logger.getLogger(this.getClass()).error(ex, ex);
        }
        return isAllowed;
    }

    /**
     * @return the httpsSupported
     */
    public boolean isHttpsSupported() {
        return enableHttps;
    }



}
