/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.util.ssl;

import com.google.inject.Inject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.dpdesktop.gui.CertificateDialog;
import net.sf.dpdesktop.module.guistate.ApplicationStateListener;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import net.sf.dpdesktop.module.guistate.TraySupportEnum;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.ServiceDataException;
import org.dom4j.DocumentException;

/**
 *
 * @author Heiner Reinhardt
 */
public class SSLController implements ApplicationStateListener{

    private final SSLModel sslModel;
    private final CertificateDialog certificateDialog;
    private final MessageModel messageModel;
    private final ContextProvider contextProvider;
    private final LanguageModel languageModel;

    @Inject
    public SSLController(SSLModel sslModel, 
            CertificateDialog certificateDialog,
            MessageModel messageModel,
            ContextProvider contextProvider,
            ApplicationStateModel applicationStateModel,
            LanguageModel languageModel) {
        
        this.sslModel = sslModel;
        this.certificateDialog = certificateDialog;
        this.messageModel = messageModel;
        this.contextProvider = contextProvider;
        this.languageModel = languageModel;

        try {
            sslModel.load(contextProvider.getContextFile());
        } catch (ServiceAuthException ex) {
            //ignore
        } catch (MalformedURLException ex) {
            //ignore
        } catch (IOException ex) {
            //ignore
        } catch (DocumentException ex) {
            //ignore
        
            sslModel.clean(contextProvider.getContextFile());
        }

        applicationStateModel.addApplicationStateListener(this);

    }

    public boolean isCertificateAllowed(String hostname, X509Certificate x509Certificate) {

        boolean isAllowed = false;

        try {
            isAllowed = sslModel.isCertificateAllowed(hostname, x509Certificate);
        } catch (ServiceDataException ex) {

            switch (certificateDialog.isAllowed(hostname, x509Certificate)) {
                case YES_EVERYTIME:
                    sslModel.setCertificateAllowed(hostname, x509Certificate, true, true);
                    isAllowed = true;
                    break;
                case YES_ONCE:
                    sslModel.setCertificateAllowed(hostname, x509Certificate, true, false);
                    isAllowed = true;
                    break;
                case NO_EVERYTIME:
                    sslModel.setCertificateAllowed(hostname, x509Certificate, false, true);
                    break;
                case NO_ONCE:
                    sslModel.setCertificateAllowed(hostname, x509Certificate, false, false);
                    break;
            }
            
        }



        return isAllowed;
    }

    public boolean isUntrustedConnectionAllowed(String hostname) {

        return messageModel.question(languageModel.getString("SSLController.message.untrustedConnection.headline"),
                languageModel.getString("SSLController.message.untrustedConnection.text").replace("%HOST%", hostname));

    }

    @Override
    public void maximize() {
        //
    }

    @Override
    public void minimize() {
        //
    }

    @Override
    public void exiting() {
        try {
            sslModel.store(contextProvider.getContextFile());
        } catch (MalformedURLException ex) {
            //ignore
        } catch (ServiceAuthException ex) {
            //ignore
        } catch (IOException ex) {
            //ignore 
        }
    }

    @Override
    public TraySupportEnum isTraySupported() {
        return TraySupportEnum.BOTH;
    }
}
