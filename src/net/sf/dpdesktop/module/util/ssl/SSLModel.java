/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2009 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.util.ssl;

import com.google.inject.Inject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.LinkedList;

import java.util.List;
import net.sf.dpdesktop.service.Context;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.ServiceDataException;
import net.sourceforge.jcetaglib.lib.CertTools;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

/**
 *
 * @author Heiner Reinhardt
 */
public class SSLModel extends DataService<SSLModel> {

    private HashMap<String, HashMap<String, Boolean>> hashMap = new HashMap<String, HashMap<String, Boolean>>();
    private ContextProvider m_contextProvider;

    @Inject
    public SSLModel(ContextProvider contextProvider) {
        super("ssl", true, null);

        this.m_contextProvider = contextProvider;
    }

    public void setCertificateAllowed(String hostname, X509Certificate x509Certificate, boolean allowed, boolean permanently) {

        final String fingerprint = CertTools.getFingerprintAsString(x509Certificate);


        HashMap<String, Boolean> innerHashMap = new HashMap<String, Boolean>();
        innerHashMap.put(fingerprint, allowed);
        hashMap.put(hostname, innerHashMap);

        if (permanently) {

            Element elemment = DocumentHelper.createElement("key");

            elemment.addElement("hostname").setText(hostname);
            elemment.addElement("fingerprint").setText(fingerprint);
            elemment.addAttribute("allowed", super.convertBooleanToString(allowed));


            document.getRootElement().add(elemment);

        }

    }

    /**
     *
     * @param hostname
     * @param x509Certificate
     * @return
     * @throws ServiceDataException - Data for decission is missing
     */
    public boolean isCertificateAllowed(String hostname, X509Certificate x509Certificate) throws ServiceDataException {
        final String fingerprint = CertTools.getFingerprintAsString(x509Certificate);

        try {
            return hashMap.get(hostname).get(fingerprint);
        } catch (NullPointerException ex) {
            throw new ServiceDataException();
        }

    }

    @Deprecated
    @Override
    public void load(SSLModel dataService) {
        throw new UnsupportedOperationException("Operation not supported");
    }

    @SuppressWarnings("unchecked")
    public void load(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
        document = super.loadDocument(context);

        hashMap = new HashMap<String, HashMap<String, Boolean>>();

        List<Element> elementList = document.selectNodes("//key");

        for (Element element : elementList) {

            HashMap<String, Boolean> innerHashMap = new HashMap<String, Boolean>();
            innerHashMap.put(element.elementText("fingerprint"), super.convertStringToBoolean(element.attributeValue("allowed")));

            hashMap.put(element.elementText("hostname"), innerHashMap);
        }


    }

    public void store(Context context) throws MalformedURLException, ServiceAuthException, IOException {
        this.storeDocument(m_contextProvider.getContextFile(), document);
    }
}
