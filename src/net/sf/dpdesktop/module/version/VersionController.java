/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.version;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import net.sf.dpdesktop.gui.AboutDialog;
import net.sf.dpdesktop.gui.MainWindow;
import net.sf.dpdesktop.gui.MessageDialog;
import net.sf.dpdesktop.module.message.MessageModel;
import net.sf.dpdesktop.module.settings.LanguageModel;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.util.Helper;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

/**
 *
 * @author Heiner Reinhardt
 */
public class VersionController {

    private MessageModel m_messageHandler;
    private final ContextProvider m_contextProvider;
    private final ConfigurationService m_versionService;
    private final LanguageModel m_languageModel;
    private final Configuration m_settingsConfiguration;

    @Inject
    public VersionController(
            MessageModel messageHandler,
            @Named("settings") Configuration settingsConfiguration,
            @Named("version") ConfigurationService configurationService,
            ContextProvider contextProvider,
            LanguageModel languageModel) {

        this.m_messageHandler = messageHandler;
        this.m_contextProvider = contextProvider;
        this.m_versionService = configurationService;
        this.m_languageModel = languageModel;
        this.m_settingsConfiguration = settingsConfiguration;



    }

    public void init() {

        if (m_settingsConfiguration.getFlag("notifyOnNewVersionAvailable", false)) {

            String localVersion = Helper.getVersion();

            try {
                m_versionService.load(m_contextProvider.getContextHttp());
                String newVersion = m_versionService.getValue("version", "0.00");
                if (Float.valueOf(newVersion) > Float.valueOf(localVersion)) {
                    m_messageHandler.info(m_languageModel.getString("VersionController.message.newVersionAvailable.headline"), m_languageModel.getString("VersionController.message.newVersionAvailable.text"));
                }
            } catch (ServiceAuthException ex) {
                Logger.getLogger(this.getClass()).error("ServiceAuthException while retrieving data from version interface", ex);
                //ignore
            } catch (MalformedURLException ex) {
                Logger.getLogger(this.getClass()).error("Hardcoded error; MalformedURLException while retrieving data from version interface.", ex);
                //ignore
            } catch (IOException ex) {
                Logger.getLogger(this.getClass()).info("IOException while retrieving data from version interface.", ex);
                //ignore
            } catch (DocumentException ex) {
                Logger.getLogger(this.getClass()).error("Hardcoded error; DocumentException while retrieving data from version interface. (Have a look at versioning service?)", ex);
                //ignore
            } catch (NumberFormatException ex) {
                Logger.getLogger(this.getClass()).debug(ex, ex);
            }

        }

    }
}
