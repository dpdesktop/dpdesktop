/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.module.version;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import java.net.MalformedURLException;
import net.sf.dpdesktop.service.config.Configuration;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.service.config.ConfigurationService;

/**
 *
 * @author Heiner Reinhardt
 */
public class VersionModule extends AbstractModule{

    private final static String versionURL = "http://dpdesktop.sourceforge.net/api/version/version.php";


    @Override
    protected void configure() {
        bind(VersionController.class).in(Scopes.SINGLETON);
    }

    @Named("version")
    @Provides
    @Singleton
    public RemotePlace provideRmotePlace()  {
        return new RemotePlace("version", versionURL, "", "");
    }

    @Named("version")
    @Provides
    @Singleton
    public ConfigurationService provideConfigurationService(@Named("version") RemotePlace config) {
        return new ConfigurationService("version", true, config);
    }

    // e.g. connect to dpdesktop.sourceforge.net and get current version
    @Provides
    @Named("version")
    @Singleton
    private Configuration provideVersionConfigurationService(@Named("version") ConfigurationService configurationService) {
        return configurationService; 
    }
}
