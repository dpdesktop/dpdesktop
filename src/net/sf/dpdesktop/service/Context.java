/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.net.MalformedURLException;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author Heiner Reinhardt
 */
public abstract class Context {

    boolean store(Writer writer, Document storeDocument) throws IOException {
        XMLWriter w = new XMLWriter(writer);
        w.write(storeDocument);
        w.flush();
        w.close();

        return true; 
    }

    Document load(Reader reader) throws DocumentException, IOException {


        BufferedReader bufferedReader = new BufferedReader(reader);

        String buffer = "";
        String currentLine = "";
        while(currentLine!=null) {
            buffer = buffer + currentLine;
            currentLine = bufferedReader.readLine();
        }

        Logger.getLogger(this.getClass()).debug("XML-Reply: "+buffer);

        return new SAXReader().read(new StringReader(buffer));
    }

    public abstract boolean store(DataService dataService, Document storeDocument) throws ServiceAuthException, MalformedURLException, IOException;

    public abstract Document load(DataService dataService) throws ServiceAuthException, MalformedURLException, IOException, DocumentException;

    public abstract void clean(DataService dataService);
}
