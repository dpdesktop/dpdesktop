/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.dpdesktop.util.Helper;
import org.dom4j.Document;
import org.dom4j.DocumentException;

/**
 *
 * @author Heiner Reinhardt
 */
public class ContextFile extends Context {

    public String getStoragePath(DataService dataService) {

        String storagePath = null;

        if (dataService.isGeneral()) {
            storagePath = Helper.getStoragePath();
        } else {
            storagePath = String.format("/%s/RemotePlaces/%s/", Helper.getStoragePath(), dataService.encode(dataService.getRemotePlace().getURLAsString()));
        }

        new File(storagePath).mkdirs();
        storagePath = storagePath + dataService.getModuleName() + ".xml";

        return storagePath;

    }

    /**
     * @param dataService
     * @param storeDocument
     * @throws IOException - Could not write to file 
     */
    @Override
    public boolean store(DataService dataService, Document storeDocument) throws IOException {
        return super.store(new FileWriter(this.getStoragePath(dataService)), storeDocument);
    }

    /**
     * 
     * @param dataService
     * @return
     * @throws IOException - File not found
     * @throws DocumentException - Parsing of data failed
     */
    @Override
    public Document load(DataService dataService) throws IOException, DocumentException {
        return super.load(new FileReader(this.getStoragePath(dataService)));

    }

    @Override
    public void clean(DataService dataService) {
        new File(this.getStoragePath(dataService)).delete();
    }
}
