/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service;

import com.google.inject.Inject;
import net.sf.dpdesktop.module.util.ssl.HttpsController;
import net.sf.dpdesktop.module.util.hash.HashFactory;

/**
 *
 * @author Heiner Reinhardt
 */
public class ContextProvider {
    private final ContextHttp contextHttp;
    private final ContextFile contextFile;
    private final ContextString contextString;

    @Inject
    public ContextProvider() {

        contextHttp = new ContextHttp();
        contextFile = new ContextFile();
        contextString = new ContextString();

    }

    public ContextString getContextString() {
        return contextString; 
    }

    public ContextHttp getContextHttp() {
        return contextHttp;
    }

    public ContextFile getContextFile() {
        return contextFile; 
    }




}
