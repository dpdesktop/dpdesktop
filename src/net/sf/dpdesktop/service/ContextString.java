/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import org.dom4j.Document;
import org.dom4j.DocumentException;

/**
 *
 * @author Heiner Reinhardt
 */
public class ContextString extends Context {

    private StringWriter writer = new StringWriter();
    private StringReader reader = new StringReader("");


    @Override
    public boolean store(DataService dataService, Document storeDocument) throws IOException {
        return super.store(writer, storeDocument);
    }

    @Override
    public Document load(DataService dataService) throws DocumentException, IOException {
        return super.load(reader);
    }

    /**
     * @return the writer
     */
    public StringWriter getWriter() {
        return writer;
    }

    /**
     * @param writer the writer to set
     */
    public void setWriter(StringWriter writer) {
        this.writer = writer;
    }

    /**
     * @return the reader
     */
    public StringReader getReader() {
        return reader;
    }

    /**
     * @param reader the reader to set
     */
    public void setReader(StringReader reader) {
        this.reader = reader;
    }

    @Override
    public void clean(DataService dataService) {
        writer = new StringWriter();
        reader = new StringReader("");
    }

}
