/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.service;

import net.sf.dpdesktop.module.remote.RemotePlace;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLDecoder;

import java.net.URLEncoder;
import net.sf.dpdesktop.service.container.Container;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

/**
 *
 * @author Heiner Reinhardt
 */
public abstract class DataService<T extends DataService> implements Cloneable {

    protected Document document = DocumentHelper.createDocument(DocumentHelper.createElement("data"));


    @SuppressWarnings("unchecked")
    @Override
    public T clone() {
        try {
            T dataService = (T) super.clone();

            dataService.document = (Document) this.document.clone();

            return dataService;
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(this.getClass()).fatal("Hardcoded error: This should not happen.", ex);
        }

        return null;

    }
    private final RemotePlace config;
    private final String moduleName;
    private final boolean general;

    public DataService(String moduleName, boolean general, RemotePlace config) {
        this.moduleName = moduleName;
        this.general = general;
        this.config = config;
    }

    public String decode(String string) {
        try {
            string = URLDecoder.decode(string, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(this.getClass()).fatal("Encoding is not supported", ex); 
        }

        return string;
    }

    public String encode(String string) {
        try {
            string = URLEncoder.encode(string, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(this.getClass()).fatal("Encoding UTF-8 is not supported", ex);
        }

        return string;
    }


    protected boolean storeDocument(Context context, Document storeDocument) throws MalformedURLException, ServiceAuthException, IOException {
        return context.store(this, storeDocument);
    }

    protected Document loadDocument(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
        return context.load(this);
    }

    public RemotePlace getRemotePlace() {
        return config;
    }

    public String getModuleName() {
        return moduleName;
    }

    /**
     * Shows wether a service is general admitted. That means, its contents are usually relevant for the
     * whole application and not remote service specific
     * @return
     */
    public boolean isGeneral() {
        return general;
    }

    public abstract void load(T dataService);

    public abstract void load(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException; 

    /**
     * This function should clean broken data. E.g. delete broken files on hard disk. The clean method is implemented
     * by the specified context.
     * @param context
     */

    public void clean(Context context) {
        context.clean(this);
    }


    /**
     * Moved this function here to prevent doubled code
     * @param e
     * @return
     */

    protected Container createContainer(Element e) {

        Container con = new Container(e.attributeValue("id"), this, e.attributeValue("isTrackable").equals("true"));


        for (Object o : e.attributes()) {
            Attribute a = (Attribute) o;
            con.put(((Attribute) a).getName(), this.decode(((Attribute) a).getValue()));
        }
        return con;
    }

    public String convertBooleanToString(boolean value) {
        if(value) {
            return "true";
        }
        
        return "false"; 
    }

    public boolean convertStringToBoolean(String value) {
        return value.equals("true"); 
    }



}
