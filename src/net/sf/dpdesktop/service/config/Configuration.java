/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/
package net.sf.dpdesktop.service.config;

/**
 *
 * @author Heiner Reinhardt
 */
public interface Configuration {

    /**
     * Returns the boolean value for a specific key
     * @param key A specific key
     * @return true, if the text of the element specified by the key is "true"
     */
    public boolean getFlag(String key) throws Exception;

    public boolean getFlag(String key, boolean defaultFlagValue);

    /**
     * Gets the value for a specific key
     * @param key A specific key
     * @throws Exception is thrown when no document was loaded.
     * @return value for the key
     */
    public String getValue(String key) throws Exception;

    public String getValue(String key, String defaultValue);

    public void addUniqueConfigurationListener(ConfigurationListener configurationListener);

}
