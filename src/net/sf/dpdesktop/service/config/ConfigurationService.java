/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.service.config;

import net.sf.dpdesktop.module.remote.RemotePlace;
import java.io.IOException;
import java.net.MalformedURLException;
import net.sf.dpdesktop.service.*;
import com.google.inject.Inject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;

/**
 *
 * @author Heiner Reinhardt
 */
public class ConfigurationService extends DataService<ConfigurationService> implements Configuration {

    private HashMap<String, List<ConfigurationListener>> map = new HashMap<String, List<ConfigurationListener>>();

    public ConfigurationService(String moduleName, boolean general) {
        super(moduleName, general, null);
    }
    
    public ConfigurationService(String moduleName, boolean general, RemotePlace config ) {
        super(moduleName, general, config);
    }



    /**
     * This call is equal to setValue(key, "true") or setValue(key, "false").
     * @param key A specific key
     * @param value The boolean value to be saved.
     */
    public void setFlag(String key, boolean value) {
        setValue(key, String.valueOf(value));
    }

    /**
     * Returns the boolean value for a specific key
     * @param key A specific key
     * @return true, if the text of the element specified by the key is "true"
     */
    public boolean getFlag(String key) throws Exception {
        return getFlagFromValue(getValue(key));
    }

    /**
     * Sets the value for a specific key
     * @param key A specific key
     * @param value The value to be set
     */
    public void setValue(String key, String value) {

        document.getRootElement().remove(document.getRootElement().element(key));

        document.getRootElement().addElement(key).addAttribute("value", value);

        this.fireChanges(key, value);
    }

    /**
     * Gets the value for a specific key
     * @param key A specific key
     * @throws Exception is thrown when no document was loaded.
     * @return value for the key
     */
    public String getValue(String key) throws Exception {
        return document.getRootElement().element(key).attributeValue("value");
    }

    public String getValue(String key, String defaultValue) {
        try {
            return getValue(key);
        } catch (Exception ex) {
            return defaultValue;
        }
    }

    public boolean getFlag(String key, boolean defaultFlagValue) {
        try {
            return getFlag(key);
        } catch (Exception ex) {
            return defaultFlagValue;
        }
    }

    public boolean store(Context context) throws MalformedURLException, ServiceAuthException, IOException  {
        return super.storeDocument(context, document);
    }

    public void load(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
        document = super.loadDocument(context);
        refresh();

    }

    Document getDocument() {
        return document;
    }



    public void addUniqueConfigurationListener(ConfigurationListener configurationListener) {

        String key = configurationListener.getKey();

        if(map.containsKey(key)) {
            if(map.get(key).contains(configurationListener)) {
                return;
            }
        } else {
            map.put(key, new LinkedList<ConfigurationListener>());
        }

        map.get(key).add(configurationListener);


        String value = null;

        try {
            value = this.getValue(key);
        } catch (Exception ex) {
            value = null; 
        } finally {
            if(value!=null) {
                configurationListener.valueChanged(value);
                configurationListener.flagChanged( getFlagFromValue(value) );
            }
        }

    }


    private boolean getFlagFromValue(String value) {
        return value.equals("true");
    }


    private void fireChanges(String key, String value) {

        if(map.containsKey(key)) {

            for(ConfigurationListener listener : map.get(key)) {
                listener.valueChanged(value);
                listener.flagChanged(getFlagFromValue(value));
            }

        }

    }

    @Override
    public void load(ConfigurationService dataService) {
        document = (Document) dataService.getDocument().clone();
        refresh();
    }

    public List<String> keys() {

        List<String> list = new LinkedList<String>();

        for(Object element : document.getRootElement().elements()) {
            list.add( ((Node)element).getName());
        }

        return list; 

    }

    public void refresh() {
        for (String key : this.keys()) {
            fireChanges(key, this.getValue(key, ""));
        }
    }

}
