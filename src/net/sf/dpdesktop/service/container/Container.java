/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/
 

package net.sf.dpdesktop.service.container;

import net.sf.dpdesktop.util.Property;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import net.sf.dpdesktop.service.DataService;

/**
 * This object may represent a company, task, or project or something else. 
 * @author Heiner Reinhardt
 */

public class Container extends Property implements Comparable<Container>  {

    /**
     * Name of the container
     */
    private final String ID;
    /**
     * Parent container
     */
    private Container parent;

    /**
     * List including the child-container
     */
    private LinkedList<Container> children = new LinkedList<Container>();
    private boolean trackable;
    private final DataService creator;


    /**
     * @return is this container trackable? 
     */
    public Boolean isTrackable() {
        return trackable; 
    }


    public Container(String ID) {
        this(ID, null); 
    }

    /**
     * Creates a new container with no parent, the parent will be null
     * @param name Name of the container
     */

    public Container(String ID, DataService creator) {
        this(ID, creator, false);
    }

    /**
     * Creates a new container with a given parent.
     * @param ID ID of the Container.
     * @param trackable Specifies whether the container is trackable or not.
     * @param parent Parent of the container.
     */

    public Container(String ID, DataService creator, boolean trackable) {
        this.ID = ID;
        this.creator = creator;
        this.trackable = trackable;
    }

    /**
     * Adds a children to the container
     * @param container child-container
     */
    public void addChild( Container container ) {
        children.add(container);
        container.parent = this;
    }

    /**
     * Returns a cloned list of children
     * @return cloned container list
     */

    @SuppressWarnings(value = "unchecked")
    public LinkedList<Container> getChildren() {

        return  (LinkedList<Container>) children.clone();
    }

    /**
     * @return the name
     */
    public String getID() {
        return ID;
    }

    public String getName() {
        return this.get("name", "unnamed");
    }

  

    /**
     * @return the parent
     */
    public Container getParent() {
        return parent;
    }


    public boolean hasParent() {
        return parent != null;
    }

    @Override
    public boolean equals(Object obj) {

        if( obj instanceof Container ) {

            Container container = (Container) obj;

            return this.getID().equals(container.getID()) && this.getName().equals(container.getName());
        }

        return false; 

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.ID != null ? this.ID.hashCode() : 0);
        return hash;
    }

    public DataService getCreator() {
        return creator;
    }

    public void removeAll() {
        this.children.clear();
    }

    @Override
    public int compareTo(Container o) {
        return this.getName().compareTo(o.getName()); 
    }


}
