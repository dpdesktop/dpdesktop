/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service.container;

import java.util.LinkedList;
import java.util.List;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.remote.RepositoryListener;

/**
 *
 * @author Heiner Reinhardt
 */
public class ContainerRepository implements RepositoryListener {

    private List<ContainerRepositoryListener> listenerList = new LinkedList<ContainerRepositoryListener>();

    private Container rootContainer = new Container("root");

    public Container getRootContainer() {
        return rootContainer;
    }

    public void addContainerRepositoryListener(ContainerRepositoryListener containerRepositoryListener) {
        listenerList.add(containerRepositoryListener);
        containerRepositoryListener.rootContainerChanged(rootContainer);
    }

    public void fireRootContainerChanged() {

        for(ContainerRepositoryListener containerRepositoryListener : listenerList) {
            containerRepositoryListener.rootContainerChanged(rootContainer);
        }
    }

    @Override
    public void listChanged(List<RemotePlaceServiceProvider> list) {

        rootContainer.removeAll();

        for(RemotePlaceServiceProvider provider : list) {
            rootContainer.addChild(provider.getContainerService().getRootContainer());
        }

        fireRootContainerChanged();

    }

    public Container recovery(Container container) {
        return recovery(rootContainer, container);
    }

    private Container recovery(Container rootContainer, Container searchContainer) {
        if(rootContainer.equals(searchContainer)) {
            return rootContainer;
        } else {
            for(Container newRootContainer : rootContainer.getChildren()) {

                Container c = this.recovery(newRootContainer, searchContainer);

                if(c!=null) {
                    return c;
                }

            }
        }

        return null;
    }



}
