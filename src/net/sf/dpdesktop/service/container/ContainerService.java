/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service.container;

import com.google.inject.internal.Collections2;
import net.sf.dpdesktop.module.remote.RemotePlace;
import java.net.MalformedURLException;
import net.sf.dpdesktop.service.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.dom.DOMDocument;
import org.dom4j.tree.DefaultDocument;
import org.jaxen.function.ConcatFunction;

/**
 *
 * @author Heiner Reinhardt
 */
public class ContainerService  extends DataService<ContainerService> {

    public ContainerService(RemotePlace config) {
        super("container", false, config);
    }

    public Container getRootContainer() {

        Container root = new Container( this.getRemotePlace().getCompany(), this);
        root.put("name", "Remote: " + this.getRemotePlace().getCompany());
        root.put("color", "FF8D00");


        traverseContainer(document.getRootElement().elementIterator("container"), root);

        return root;

    }

    /**
     * Tests wether a specific container exists in the container repository.
     * Two container are equal when they have the same ID.
     * @see net.sf.dpdesktop.service.Container
     * @param container
     * @return True if the container exists; False if it does not exist.
     */
    public boolean exists(Container container) {

        String id = container.getID();
        Node node = document.selectSingleNode("//container[@id=\"" + id + "\"]");
        //return false;
        return node != null;

    }


    /**
     * This function generates a container tree from XML data recursivly
     * @param iterator a element iterator
     * @param parent the parent container
     */
    private void traverseContainer(Iterator iterator, Container parent) {

        while (iterator.hasNext()) {

            Element e = (Element) iterator.next();

            Container con = super.createContainer(e);

            traverseContainer(e.elementIterator(), con);

            parent.addChild(con);
        }

        Collections.sort(parent.getChildren());

    }


    public void load(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
        document = super.loadDocument(context);
    }

    public void store(Context context) throws MalformedURLException, ServiceAuthException, IOException {
        super.storeDocument(context, document);
    }

    @Override
    public void load(ContainerService dataService) {
        document = (Document) dataService.document.clone();
    }





}
