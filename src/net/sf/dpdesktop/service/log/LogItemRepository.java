/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service.log;

import java.util.LinkedList;
import java.util.List;
import net.sf.dpdesktop.module.remote.RemotePlaceServiceProvider;
import net.sf.dpdesktop.module.remote.RepositoryListener;


/**
 *
 * @author Heiner Reinhardt
 */
public class LogItemRepository implements RepositoryListener {


    private List<LogItem> m_list = new LinkedList<LogItem>();

    private List<LogItemRepositoryListener> listenerList = new LinkedList<LogItemRepositoryListener>();

    @Deprecated
    protected void setList(List<LogItem> list) {
        this.m_list = list;
        this.fireListChanged();
    }

    public void addLogItemRepositoryListener(LogItemRepositoryListener logItemRepositoryListener) {
        listenerList.add(logItemRepositoryListener);

        logItemRepositoryListener.listChanged(m_list);

    }

    private void fireListChanged() {
        for(LogItemRepositoryListener logItemRepositoryListener : listenerList) {
            logItemRepositoryListener.listChanged(m_list);
        }
    }

    @Override
    public void listChanged(List<RemotePlaceServiceProvider> list) {

        m_list.clear();

        for(RemotePlaceServiceProvider provider : list) {
            m_list.addAll(provider.getLogItemService().getLogItemList());
        }

        fireListChanged();

    }


    public List<LogItem> getList() {
        return m_list;
    }





}
