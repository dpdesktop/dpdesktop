/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service.log;


import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.module.remote.RemotePlace;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import net.sf.dpdesktop.service.Context;
import net.sf.dpdesktop.service.ServiceAuthException;
import net.sf.dpdesktop.service.ServiceDataException;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;


/**
 *
 * @author Heiner Reinhardt
 */
public class LogItemService extends DataService<LogItemService> {


    public LogItemService(RemotePlace remotePlace) {
        super("history", false, remotePlace);
    }

    /**
     * Creates a list of log entries inside the database.
     * @return List of LogItems
     * @throws java.text.ParseException Error while parsing the date
     */

    public List<LogItem> getLogItemList() {

        LinkedList<LogItem> l = new LinkedList<LogItem>();

        for (Object element : document.selectNodes("//log")) {

            Element e = (Element) element;

            LogItem logItem = new LogItem(
                    e.attributeValue("summary"),
                    e.attributeValue("comment"),
                    super.createContainer(e.element("container")));


            for (Object o : e.attributes()) {
                Attribute a = (Attribute) o;
                logItem.put(((Attribute) a).getName(), ((Attribute) a).getValue());
            }

            l.add(logItem);

        }

        return l;


    }





    public void load(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
        document = super.loadDocument(context);
    }


    public boolean store(Context context, LogItem item) throws MalformedURLException, ServiceAuthException, IOException, ServiceDataException {

        Document storeDocument = createDocument(item);

        return super.storeDocument(context, storeDocument);

    }


    private Document createDocument(LogItem item) throws ServiceDataException {

        if(item.get("summary","").length()==0) {
            throw new ServiceDataException("summary");
        }
        
        if(item.get("comment", "").length()==0) {
            throw new ServiceDataException("comment");
        }

         
        if(item.get("workedTime", "00:00").equals("00:00")) {
            throw new ServiceDataException("workedTime");
        }




        Element data = DocumentHelper.createElement("data");
        Document doc = DocumentHelper.createDocument(data);

        Element e = DocumentHelper.createElement("log");

        data.add( e );

        e.addAttribute("containerID", item.getContainer().getID());
        e.addAttribute("summary", super.encode(item.get( "summary", "")));
        e.addAttribute("comment", super.encode(item.get("comment", "")));
        e.addAttribute("complete", super.encode(item.get("complete", "0")));
        e.addAttribute("workedTime", item.get("workedTime", "00:00"));
        e.addAttribute("billableTime", item.get("billableTime", "00:00"));

        return doc;
    }


    public void store(Context contextFile) throws MalformedURLException, ServiceAuthException, IOException {
        super.storeDocument(contextFile, document);
    }

    @Override
    public void load(LogItemService dataService) {
        document = (Document) dataService.document.clone();
    }
}
