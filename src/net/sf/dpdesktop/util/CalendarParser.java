/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * This class provides helper function to parse the string representation of
 * a specific date or to build a string representation of a given date. 
 * @author Heiner Reinhardt
 */
public class CalendarParser {

    private final static String FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * Returns a string representation for a given calendar in format
     * (yyyy-MM-dd HH:mm:ss)
     * @see #getString(java.util.GregorianCalendar, java.lang.String)
     * @see #getCalendar(java.lang.String) 
     * @param calendar
     * @return The respective String
     */

    public static String getString(GregorianCalendar calendar) {
        return CalendarParser.getString(calendar, FORMAT);
    }

    /**
     * Returns a string representation for a given calendar in the given format.
     * If no format is given, (yyyy-MM-dd HH:mm:ss) will be used instead.
     * @see #getString(java.util.GregorianCalendar)
     * @see #getCalendar(java.lang.String)
     * @param calendar
     * @param pattern output format pattern
     * @return The respective String
     */

    public static String getString(GregorianCalendar calendar, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat( pattern );
        return dateFormat.format(calendar.getTime());
    }

    /**
     * This function will deliver a GregorianCalendar for a given date, 
     * represented by a string. To parse this string, an common iso date format
     * pattern will be used (yyyy-MM-dd HH:mm:ss)
     * @see #getString(java.util.GregorianCalendar)
     * @see #getString(java.util.GregorianCalendar, java.lang.String) 
     * @param isoDate
     * @return The respective GregorianCalendar
     * @throws java.text.ParseException
     */

    public static GregorianCalendar getCalendar(String isoDate) throws ParseException {
        return CalendarParser.getCalendar(isoDate, FORMAT);
    }

    public static GregorianCalendar getCalendar(String isoDate, String pattern) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat( pattern );
        dateFormat.parse(isoDate);
        return (GregorianCalendar) dateFormat.getCalendar();
    }

}
