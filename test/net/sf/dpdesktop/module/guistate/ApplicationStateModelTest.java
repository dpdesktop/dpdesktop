/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.guistate;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import net.sf.dpdesktop.module.guistate.ApplicationStateListener;
import net.sf.dpdesktop.module.guistate.ApplicationStateModel;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.internal.ExpectationBuilder;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class ApplicationStateModelTest {

    public ApplicationStateModelTest() {
    }
    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };

    @Test
    public void dummy() {
        
    }

    /*
    @Test
    public void testInitialState() {

        final ApplicationStateListener stateComponentListenerWithTray = context.mock(ApplicationStateListener.class, "listenerWithTray");
        final ApplicationStateListener stateComponentListenerWithoutTray = context.mock(ApplicationStateListener.class, "listenerWithoutTray");
        final ActionListener exitActionListener = context.mock(ActionListener.class);
        ApplicationStateModel applicationStateModel = new ApplicationStateModel(exitActionListener);

        context.checking(new Expectations() {

            {
                ignoring(exitActionListener);
                exactly(1).of(stateComponentListenerWithoutTray).minimize();

            }
        });

        applicationStateModel.addApplicationStateListenerWithTray(stateComponentListenerWithTray);
        applicationStateModel.addApplicationStateListenerWithoutTray(stateComponentListenerWithoutTray);

        assertTrue(applicationStateModel.isMinimized());
        assertFalse(applicationStateModel.isMaximized());
        assertFalse(applicationStateModel.isTraySupported());


        context.assertIsSatisfied();

    }

    @Test
    public void testListenerUpdateOnTraySupportChanged() {
        final ApplicationStateListener stateComponentListenerWithTray = context.mock(ApplicationStateListener.class, "listenerWithTray");
        final ApplicationStateListener stateComponentListenerWithoutTray = context.mock(ApplicationStateListener.class, "listenerWithoutTray");
        final ActionListener exitActionListener = context.mock(ActionListener.class);
        ApplicationStateModel applicationStateModel = new ApplicationStateModel(exitActionListener);




        final Sequence sequence = context.sequence("traySupportChanged");
        context.checking(new Expectations() {

            {

                ignoring(exitActionListener);


                exactly(1).of(stateComponentListenerWithTray).minimize();
                inSequence(sequence);
                exactly(1).of(stateComponentListenerWithoutTray).minimize();
                inSequence(sequence);
                exactly(1).of(stateComponentListenerWithTray).minimize();
                inSequence(sequence);
                exactly(1).of(stateComponentListenerWithoutTray).minimize();
                inSequence(sequence);
            }
        });


        applicationStateModel.setTraySupported(true);
        applicationStateModel.addApplicationStateListenerWithTray(stateComponentListenerWithTray);
        assertTrue(applicationStateModel.isTraySupported());

        applicationStateModel.setTraySupported(false);
        applicationStateModel.addApplicationStateListenerWithoutTray(stateComponentListenerWithoutTray);
        assertFalse(applicationStateModel.isTraySupported());

        applicationStateModel.setTraySupported(true);
        applicationStateModel.setTraySupported(false);


        context.assertIsSatisfied();

    }

    @Test
    public void testActualStateSwitch() {
        final ApplicationStateListener stateComponentListenerWithTray = context.mock(ApplicationStateListener.class, "listenerWithTray");
        final ActionListener exitActionListener = context.mock(ActionListener.class);
        ApplicationStateModel applicationStateModel = new ApplicationStateModel(exitActionListener);
        applicationStateModel.setTraySupported(true);



        final Sequence sequence = context.sequence("traySupportChanged");
        context.checking(new Expectations() {

            {
                ignoring(exitActionListener);


                // initial state
                exactly(1).of(stateComponentListenerWithTray).minimize();
                inSequence(sequence);

                exactly(1).of(stateComponentListenerWithTray).maximize();
                inSequence(sequence);
                exactly(1).of(stateComponentListenerWithTray).exiting();
                inSequence(sequence);

                exactly(1).of(stateComponentListenerWithTray).minimize();
                inSequence(sequence);
                exactly(1).of(stateComponentListenerWithTray).maximize();
                inSequence(sequence);
                exactly(1).of(stateComponentListenerWithTray).exiting();
                inSequence(sequence);

                exactly(1).of(stateComponentListenerWithTray).exiting();
                inSequence(sequence);
                exactly(1).of(stateComponentListenerWithTray).maximize();
                inSequence(sequence);


                exactly(1).of(stateComponentListenerWithTray).minimize();
                inSequence(sequence);




            }
        });

        applicationStateModel.addApplicationStateListenerWithTray(stateComponentListenerWithTray);



        applicationStateModel.maximize();
        applicationStateModel.exiting();

        applicationStateModel.minimize();
        applicationStateModel.maximize();
        applicationStateModel.exiting();

        applicationStateModel.exiting();
        applicationStateModel.maximize();
        applicationStateModel.maximize();

        applicationStateModel.minimize();
        applicationStateModel.minimize();


        context.assertIsSatisfied();


    }

    @Test
    public void testVetoableListenerTrue() {

        final VetoableExitListener vetoableExitListener = context.mock(VetoableExitListener.class);
        final ActionListener exitActionListener = context.mock(ActionListener.class);
        ApplicationStateModel applicationStateModel = new ApplicationStateModel(exitActionListener);

        applicationStateModel.addVetoableExitListener(vetoableExitListener);

        context.checking(new Expectations() {

            {

                exactly(1).of(exitActionListener).actionPerformed(with(any(ActionEvent.class)));

                allowing(vetoableExitListener).isExitingAllowed();
                will(returnValue(true));

            }
        });


        applicationStateModel.exiting();

        context.assertIsSatisfied();

    }



    @Test
    public void testVetoableListenerFalse() {

        final VetoableExitListener vetoableExitListener = context.mock(VetoableExitListener.class);
        final ActionListener exitActionListener = context.mock(ActionListener.class);
        ApplicationStateModel applicationStateModel = new ApplicationStateModel(exitActionListener);

        applicationStateModel.addVetoableExitListener(vetoableExitListener);

        context.checking(new Expectations() {

            {

                never (exitActionListener).actionPerformed(with(any(ActionEvent.class)));

                allowing(vetoableExitListener).isExitingAllowed();
                will(returnValue(false));

            }
        });


        applicationStateModel.exiting();

        context.assertIsSatisfied();


        assertTrue(applicationStateModel.isMaximized() || applicationStateModel.isMinimized());

    }
     */
}
