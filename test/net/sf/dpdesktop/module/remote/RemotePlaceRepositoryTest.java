/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/
package net.sf.dpdesktop.module.remote;


import net.sf.dpdesktop.module.remote.SelectionListener;
import net.sf.dpdesktop.module.remote.RemotePlaceRepository;
import java.util.List;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.module.util.hash.HashFactory;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class RemotePlaceRepositoryTest {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };




    @Test
    public void testElementListener() {

        final HashFactory hashFactory = context.mock(HashFactory.class);

        context.checking(new Expectations(){{
            allowing (hashFactory).getValue(with(any(String.class)));
        }});

        final SelectionListener selectionListener = context.mock(SelectionListener.class);

        RemotePlaceRepository remotePlaceRepository = new RemotePlaceRepository(hashFactory);
        context.checking(new Expectations(){{

            exactly(1).of (selectionListener).selected(with(aNonNull(RemotePlace.class)), with(any(Integer.class)));

        }});
        remotePlaceRepository.addSelectionListener( selectionListener );

        remotePlaceRepository.addElement("a", "b", "c", "d", false);

        


        context.assertIsSatisfied();
    }

    @Test
    public void testDataListener() {

        final RemotePlaceRepository remotePlaceRepository = new RemotePlaceRepository(null);

        remotePlaceRepository.addListDataListener(new ListDataListener() {

            @Override
            public void intervalAdded(ListDataEvent e) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                assertEquals(remotePlaceRepository, e.getSource());
            }
        });


    }

    @Test
    public void testLoading() {


        final HashFactory hashFactory = context.mock(HashFactory.class);

        context.checking(new Expectations(){{
            allowing (hashFactory).getValue(with(any(String.class)));
        }});

        RemotePlaceRepository remotePlaceRepository1 = new RemotePlaceRepository( hashFactory );

        RemotePlaceRepository remotePlaceRepository2 = new RemotePlaceRepository( hashFactory );

        remotePlaceRepository1.addElement("a", "b", "c", "d", false);

        remotePlaceRepository2.synchronizeWith(remotePlaceRepository1);

        assertEquals(remotePlaceRepository2.getSize(),1);
        
        remotePlaceRepository1.addElement("x", "y", "z", "x", false);

        assertEquals(remotePlaceRepository2.getSize(),1);



    }

}