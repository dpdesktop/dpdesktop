/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.module.tracking;

import net.sf.dpdesktop.service.*;
import net.sf.dpdesktop.module.tracking.TrackingManagerState;
import net.sf.dpdesktop.module.tracking.TrackingListener;
import net.sf.dpdesktop.module.tracking.TrackingManager;
import net.sf.dpdesktop.service.container.Container;
import org.junit.*;
import static org.junit.Assert.*;
import org.jmock.*;
import org.jmock.integration.junit4.*;
import org.jmock.lib.legacy.ClassImposteriser;

/**
 *
 * @author Heiner Reinhardt
 */
public class TrackingManagerTest {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };

    @Test
    public void testListenerVectorAPI() {

        // simply tests add/remove of listeners 

        final TrackingListener t1 = context.mock(TrackingListener.class, "t1");

        TrackingManager tm = new TrackingManager();

        context.checking(new Expectations() {

            {

                exactly(1).of(t1).unready();
                allowing (t1).commentChanged(with(any(String.class)));
                allowing (t1).summaryChanged(with(any(String.class)));
                allowing (t1).completeChanged(with(any(Integer.class)));
                allowing (t1).timeChanged();

            }
        });

        tm.addListener(t1);

        assertTrue(tm.removeListener(t1));
        assertFalse(tm.removeListener(t1));

        context.assertIsSatisfied();

    }

    @Test
    public void testTimeOffsetAddition() {
        TrackingManager tm = new TrackingManager();

        assertEquals(tm.getTime(), 0);

        int sum = 0;

        for (int x : new int[]{0, 1, 2}) {
            sum = sum + x;
            tm.addTimeOffset(x);
            assertEquals(tm.getTime(), sum);
        }
    }

    @Test
    public void testUnreadyState() {
        TrackingManager tm = new TrackingManager();
        assertTrue(tm.isUnready());

        tm.start();
        assertTrue(tm.isUnready());

        tm.stop();
        assertTrue(tm.isUnready());

        tm.idle();
        assertTrue(tm.isUnready());

        assertEquals(tm.getTime(), 0);
    }

    @Test
    @SuppressWarnings("empty-statement")
    public void testSimpleTrackingForTwoSeconds() throws ContainerNotTrackableException {

        final TrackingListener trackingListener = context.mock(TrackingListener.class);
        final Container container = context.mock(Container.class);

        final Sequence tracking = context.sequence("tracking");


        final int secondsToBeTracked = 2;

        context.checking(new Expectations() {

            {

                allowing(container).get(with(equal("complete")), with(equal("0")));
                will(returnValue("0"));

                allowing (trackingListener).commentChanged(with(any(String.class)));
                allowing (trackingListener).summaryChanged(with(any(String.class)));
                allowing (trackingListener).completeChanged(with(any(Integer.class)));

                atLeast(1).of(container).isTrackable();
                will(returnValue(true));

                // initial state
                exactly(1).of(trackingListener).unready();
                inSequence(tracking);

                exactly(1).of(trackingListener).timeChanged();

                exactly(1).of(trackingListener).containerChanged(container);
                inSequence(tracking);
                exactly(1).of(trackingListener).ready();
                inSequence(tracking);
                exactly(1).of(trackingListener).stop();
                inSequence(tracking);

                exactly(1).of(trackingListener).start();
                inSequence(tracking);
                exactly(secondsToBeTracked).of(trackingListener).timeChanged();
                inSequence(tracking);
                exactly(1).of(trackingListener).stop();
                inSequence(tracking);

            }
        });


        TrackingManager tm = new TrackingManager();
        tm.addListener(trackingListener);


        // initial state
        assertEquals(tm.getState(), TrackingManagerState.UNREADY);
        assertTrue(tm.isCleared());

        // let's add a container -> tracking manager should be cleared and stopped
        tm.setTrackableContainer(container);
        assertEquals(tm.getState(), TrackingManagerState.STOPPED);
        assertTrue(tm.isCleared());

        // let's start tracking and wait a little bit
        tm.start();
        assertEquals(tm.getState(), TrackingManagerState.RUNNING);


        // calculate end time with 2% fuzziness

        long endTime = System.currentTimeMillis() + secondsToBeTracked * 1000 + 2 * secondsToBeTracked * 10;

        while (endTime > System.currentTimeMillis()) {
            // busy waiting
        }
        

        assertEquals(tm.getTime(), secondsToBeTracked);

        // let's stop the trackinger again
        tm.stop();

        context.assertIsSatisfied();



    }

    @Test
    public void testSetContainerBehaviour() {
        final TrackingListener t1 = context.mock(TrackingListener.class, "t1");
        final Container c1 = context.mock(Container.class, "c1");
        final Container c2 = context.mock(Container.class, "c2");

        final Sequence tracking = context.sequence("tracking");

        TrackingManager tm = new TrackingManager();

        context.checking(new Expectations() {

            {
                allowing(c1).get(with(equal("complete")), with(equal("0")));
                will(returnValue("0"));

                allowing(c2).get(with(equal("complete")), with(equal("0")));
                will(returnValue("0"));

                allowing (t1).commentChanged(with(any(String.class)));
                allowing (t1).summaryChanged(with(any(String.class)));
                allowing (t1).completeChanged(with(any(Integer.class)));
                allowing (t1).timeChanged();

                allowing(c1).isTrackable();
                will(onConsecutiveCalls(returnValue(true), returnValue(false)));
                allowing(c2).isTrackable();
                will(returnValue(true));
                // initial state
                exactly(1).of(t1).unready();
                inSequence(tracking);

                exactly(1).of(t1).containerChanged(c1);
                inSequence(tracking);
                exactly(1).of(t1).ready();
                inSequence(tracking);
                exactly(1).of(t1).stop();
                inSequence(tracking);

                exactly(1).of(t1).containerChanged(c2);
                inSequence(tracking);


            }
        });

        tm.addListener(t1);


        try {
            tm.setTrackableContainer(c1);
            assertEquals("Wrong Container returned", tm.getTrackableContainer(), c1);
            tm.setTrackableContainer(c2);
            assertEquals("Wrong Container returned", tm.getTrackableContainer(), c2);
        } catch (ContainerNotTrackableException ex) {
            fail("Exeption raised within unexpected context");
        }
        try {
            tm.setTrackableContainer(c1);
            fail("No Exeption raised within context");
        } catch (Exception ex) {
        }
        context.assertIsSatisfied();
    }

    @Test
    public void testUntrackableContainerBehaviour() {

        final Container container = context.mock(Container.class);

        TrackingManager tm = new TrackingManager();

        context.checking(new Expectations() {

            {

                allowing(container).isTrackable();
                will(returnValue(false));

            }
        });


        try {
            tm.setTrackableContainer(container);
            fail();
        } catch (ContainerNotTrackableException ex) {
            // all right
        }

        assertEquals(tm.getState(), TrackingManagerState.UNREADY);


        context.assertIsSatisfied();


    }

    @Test
    @SuppressWarnings("empty-statement")
    public void testIdleState() throws ContainerNotTrackableException {

        final TrackingListener trackingListener = context.mock(TrackingListener.class);

        final Container container = context.mock(Container.class);

        final Sequence tracking = context.sequence("tracking");

        final int secondsToBeTracked = 2;


        context.checking(new Expectations() {

            {
                allowing(container).get(with(equal("complete")), with(equal("0")));
                will(returnValue("0"));

                allowing (trackingListener).commentChanged(with(any(String.class)));
                allowing (trackingListener).summaryChanged(with(any(String.class)));
                allowing (trackingListener).completeChanged(with(any(Integer.class)));

                allowing(container).isTrackable();
                will(returnValue(true));

                // initial state when adding listener
                exactly(1).of(trackingListener).start();
                inSequence(tracking);

                exactly(1).of(trackingListener).timeChanged();
                inSequence(tracking); 
                exactly(secondsToBeTracked).of(trackingListener).timeChanged();
                inSequence(tracking);
                exactly(1).of(trackingListener).stop();
                inSequence(tracking);
                exactly(1).of(trackingListener).start();
                inSequence(tracking);
                exactly(1).of(trackingListener).stop();
                inSequence(tracking);

            }
        });

        TrackingManager tm = new TrackingManager();

        tm.setAutoIdleEnabled(true);
        tm.setAutoIdleTimeInSeconds(secondsToBeTracked);
        tm.setTrackableContainer(container);
        tm.start();

        tm.addListener(trackingListener);

        long endTime = System.currentTimeMillis() + secondsToBeTracked * 1000 + 2 * secondsToBeTracked * 10;

        while (endTime > System.currentTimeMillis()) {
            // busy waiting
        }
        ;

        tm.resetAutoIdleTimer();
        tm.stop();

        context.assertIsSatisfied();

    }

    @Test
    public void calculationCheck() {
        TrackingManager tm = new TrackingManager();
        tm.addTimeOffset(1);
        assertEquals("Seconds not correct calculated", tm.getSeconds(), 1);
        assertEquals("Time not correct calculated", tm.getTime(), 1);
        tm.addTimeOffset(59);
        assertEquals("Hours not correct calculated", tm.getHours(), 0);
        assertEquals("Minutes not correct calculated", tm.getMinutes(), 1);
        assertEquals("Time not correct calculated", tm.getTime(), 60);
        assertEquals("Seconds not correct calculated", tm.getSeconds(), 0);
        tm.addTimeOffset(59 * 60);
        assertEquals("Hours not correct calculated", tm.getHours(), 1);
        assertEquals("Time not correct calculated", tm.getTime(), 3600);
        assertEquals("Minutes not correct calculated", tm.getMinutes(), 0);
        assertEquals("Seconds not correct calculated", tm.getSeconds(), 0);
    }

    @Test
    public void testClearing() throws ContainerNotTrackableException {
        final TrackingListener trackingListener = context.mock(TrackingListener.class);

        final Container container = context.mock(Container.class);

        final Sequence tracking = context.sequence("tracking");


        context.checking(new Expectations() {

            {
                allowing(container).get(with(equal("complete")), with(equal("0")));
                will(returnValue("0"));

                allowing (trackingListener).commentChanged(with(any(String.class)));
                allowing (trackingListener).summaryChanged(with(any(String.class)));
                allowing (trackingListener).completeChanged(with(any(Integer.class)));


                allowing(container).isTrackable();
                will(returnValue(true));

                allowing(trackingListener).timeChanged();

                //initial state when adding listener
                exactly(1).of(trackingListener).stop();
                inSequence(tracking);


                exactly(1).of(trackingListener).stop();
                inSequence(tracking);
                exactly(1).of(trackingListener).clear();
                inSequence(tracking);

                exactly(1).of(trackingListener).start();
                inSequence(tracking);

                exactly(1).of(trackingListener).stop();
                inSequence(tracking);
                exactly(1).of(trackingListener).clear();
                inSequence(tracking);

            }
        });

        TrackingManager tm = new TrackingManager();
        tm.setTrackableContainer(container);
        tm.addListener(trackingListener);


        tm.clear();
        assertTrue(tm.isCleared());

        // add a time offset
        tm.addTimeOffset(200);
        assertFalse(tm.isCleared());

        // start the tracker
        tm.start();

        // and clear the tracker again
        tm.clear();
        assertTrue(tm.isCleared());

        context.assertIsSatisfied();
    }



    @Test
    public void testVetoableListenerTrue() throws ContainerNotTrackableException {

        final VetoableContainerChangedListener vetoableContainerChangedListener = context.mock(VetoableContainerChangedListener.class);

        TrackingManager trackingManager = new TrackingManager();

        trackingManager.addVetoableContainerChangedListener(vetoableContainerChangedListener);

        final Container newContainer = context.mock(Container.class);

        context.checking(new Expectations(){{

            allowing(newContainer).get(with(equal("complete")), with(equal("0")));
            will(returnValue("0"));

            allowing(newContainer).isTrackable();
            will(returnValue(true));

            exactly(1).of (vetoableContainerChangedListener).isContainerChangeAllowed(with(same(newContainer)));
            will(returnValue(true));
        }});
        
        trackingManager.setTrackableContainer(newContainer);

        assertEquals(trackingManager.getTrackableContainer(), newContainer);


        context.assertIsSatisfied();
    }

    @Test
    public void testVetoableListenerFalse() throws ContainerNotTrackableException {

        final VetoableContainerChangedListener vetoableContainerChangedListener = context.mock(VetoableContainerChangedListener.class);

        TrackingManager trackingManager = new TrackingManager();

        Container oldContainer = trackingManager.getTrackableContainer();

        trackingManager.addVetoableContainerChangedListener(vetoableContainerChangedListener);

        final Container newContainer = context.mock(Container.class);

        context.checking(new Expectations(){{

            allowing(newContainer).get(with(equal("complete")), with(equal("0")));
            will(returnValue("0"));

            allowing(newContainer).isTrackable();
            will(returnValue(true));

            exactly(1).of (vetoableContainerChangedListener).isContainerChangeAllowed(with(same(newContainer)));
            will(returnValue(false));
        }});

        trackingManager.setTrackableContainer(newContainer);

        assertEquals(trackingManager.getTrackableContainer(), oldContainer);


        context.assertIsSatisfied();
    }



}
