/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service;
import java.io.IOException;
import java.net.MalformedURLException;
import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.service.Context;
import static org.junit.Assert.*;
import java.io.Reader;
import java.io.Writer;
import net.sf.dpdesktop.module.remote.RemotePlace;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Heiner Reinhardt
 */
public class DataServiceTest {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };

    @Test
    public void test() throws DocumentException, Exception {
        
        final Context c = context.mock(Context.class); 
        
        context.checking(new Expectations() {{
            exactly(1).of (c).load(with(any(DataService.class)));
            exactly(1).of (c).store(with(any(DataService.class)), with(any(Document.class)));
        }});


        final RemotePlace config = context.mock(RemotePlace.class);


        DataService dataService = new DataService("test", true, config) {



            @Override
            public void load(DataService dataService) {
                //
            }

            @Override
            public void load(Context context) throws ServiceAuthException, MalformedURLException, IOException, DocumentException {
                //
            }
        };

        dataService.loadDocument(c);
        dataService.storeDocument(c, DocumentHelper.createDocument());


        context.assertIsSatisfied();


        assertEquals(dataService.getModuleName(), "test");
        assertEquals(dataService.isGeneral(), true);
        assertEquals(dataService.getRemotePlace(), config);
    }






}
