/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.service.config;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import net.sf.dpdesktop.service.ContextString;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.config.ConfigurationService;
import net.sf.dpdesktop.service.config.ConfigurationListener;
import net.sf.dpdesktop.service.*;
import java.io.StringReader;
import java.util.List;
import net.sf.dpdesktop.module.remote.RemotePlace;
import net.sf.dpdesktop.module.util.hash.HashFactory;
import org.dom4j.DocumentException;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class ConfigurationServiceTest {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };
    private String xml =
            "<data>" +
            "<stringKey value=\"bla\"/>" +
            "<flagKeyTrue value=\"true\"/>" +
            "<flagKeyFalse value=\"false\"/>" +
            "</data>";

    @Test
    public void getFlagAndValue() throws Exception {

        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService configurationService = new ConfigurationService("test", false, config);

        ContextString contextString = new ContextString();
        contextString.setReader(new StringReader(xml));


        configurationService.load(contextString);

        assertEquals(configurationService.getFlag("flagKeyTrue"), true);
        assertEquals(configurationService.getFlag("flagKeyFalse"), false);

        assertEquals(configurationService.getFlag("flagKeyTrue", false), true);
        assertEquals(configurationService.getFlag("flagKeyFalse", true), false);

        assertEquals(configurationService.getValue("stringKey"), "bla");
        assertEquals(configurationService.getValue("stringKey", "default"), "bla");
        assertEquals(configurationService.getValue("stringKey", "default2"), "bla");


    }

    @Test(expected = Exception.class)
    public void getFlag() throws Exception {
        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService emptyConfig = new ConfigurationService("test", false, config);
        emptyConfig.getFlag("test");

    }

    @Test(expected = Exception.class)
    public void getValue() throws Exception {
        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService emptyConfig = new ConfigurationService("test", false, config);
        emptyConfig.getValue("test");
    }

    @Test
    public void getValueWithDefault() {
        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService emptyConfig = new ConfigurationService("test", false, config);
        assertEquals(emptyConfig.getValue("test", "default"), "default");
        assertEquals(emptyConfig.getValue("test", "default2"), "default2");
    }

    @Test
    public void getFlagWithDefault() {
        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService emptyConfig = new ConfigurationService("test", false, config);
        assertEquals(emptyConfig.getFlag("test", false), false);
        assertEquals(emptyConfig.getFlag("test", true), true);
    }

    @Test
    public void testWhetherItIsPossibleToDoubleSetValue() {
        RemotePlace config = context.mock(RemotePlace.class);

        ConfigurationService emptyConfig = new ConfigurationService("test", false, config);

        emptyConfig.setValue("key", "value");
        emptyConfig.setValue("key", "value");

        assertEquals(emptyConfig.getDocument().getRootElement().elements("key").size(), 1);

    }

    @Test
    public void setFlag() throws Exception {
        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService emptyConfig = new ConfigurationService("test", false, config);


        emptyConfig.setFlag("key", true);
        assertTrue(emptyConfig.getFlag("key", false));
        assertTrue(emptyConfig.getFlag("key"));

    }

    @Test
    public void testModelBehaviorValue() {

        RemotePlace config = context.mock(RemotePlace.class);


        final ConfigurationListener configurationListener1 = context.mock(ConfigurationListener.class, "listener1");
        final ConfigurationListener configurationListener2 = context.mock(ConfigurationListener.class, "listener2");
        final ConfigurationListener configurationListener3 = context.mock(ConfigurationListener.class, "listener3");

        ConfigurationService emptyConfig = new ConfigurationService("test", false, config);

        final String key = "key";
        final String value1 = "value1";
        final String value2 = "true";
        emptyConfig.setValue( key, value1);

        final Sequence listenerSequence1 = context.sequence("listenerValue1");
                final Sequence listenerSequenceFlag1 = context.sequence("listenerFlag1");


        final Sequence listenerSequence3 = context.sequence("listenerValue3");
                final Sequence listenerSequenceFlag3 = context.sequence("listenerFlag3");


        context.checking( new Expectations() {{

            allowing(configurationListener1).getKey(); will(returnValue(key));
            allowing(configurationListener2).getKey(); will(returnValue("anyKey"));
            allowing(configurationListener3).getKey(); will(returnValue(key));


            exactly(1).of (configurationListener1).valueChanged( value1 ); inSequence(listenerSequence1);
            exactly(1).of (configurationListener1).valueChanged( value2 ); inSequence(listenerSequence1);
            exactly(1).of (configurationListener1).flagChanged( false ); inSequence(listenerSequenceFlag1);
            exactly(1).of (configurationListener1).flagChanged( true ); inSequence(listenerSequenceFlag1);


            exactly(1).of (configurationListener3).valueChanged( value1 ); inSequence(listenerSequence3);
            exactly(1).of (configurationListener3).valueChanged( value2 ); inSequence(listenerSequence3);
            exactly(1).of (configurationListener3).flagChanged( false ); inSequence(listenerSequenceFlag3);
            exactly(1).of (configurationListener3).flagChanged( true ); inSequence(listenerSequenceFlag3);

            never (configurationListener2).valueChanged(with(any(String.class)));
            never (configurationListener2).flagChanged(with(any(Boolean.class)));

        }} );

        emptyConfig.addUniqueConfigurationListener(configurationListener1);
        emptyConfig.addUniqueConfigurationListener(configurationListener2);
        emptyConfig.addUniqueConfigurationListener(configurationListener3);

        emptyConfig.setValue( key , value2 );

        context.assertIsSatisfied();

    }

    @Test
    public void testKeyListing() {
        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService configurationService = new ConfigurationService("test", false, config);

        configurationService.setFlag("key1", true);
        configurationService.setValue("key2", "value2");

        List<String> keys = configurationService.keys();

        assertTrue(keys.contains("key1"));
        assertTrue(keys.contains("key2"));

        assertEquals(keys.size(),2);
    }

    @Test
    public void testNotificationOnLoadFromDataService() {
        final ConfigurationListener configurationListener = context.mock(ConfigurationListener.class);
        RemotePlace config = context.mock(RemotePlace.class);
        ConfigurationService configurationService = new ConfigurationService("test", false, config);

        ConfigurationService configurationServiceClone = configurationService.clone();
        configurationServiceClone.setValue("key", "value");


        context.checking(new Expectations(){{

            allowing (configurationListener).getKey(); 
            will(returnValue("key")); 

            exactly(1).of (configurationListener).flagChanged(with(equal(false)));
            exactly(1).of (configurationListener).valueChanged(with(equal("value")));

        }});

        configurationService.addUniqueConfigurationListener(configurationListener);
        configurationService.load(configurationServiceClone);

        context.assertIsSatisfied();
    }



}
