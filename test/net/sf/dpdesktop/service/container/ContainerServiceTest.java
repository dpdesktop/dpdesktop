/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.service.container;

import net.sf.dpdesktop.service.*;
import net.sf.dpdesktop.service.ContextString;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.service.container.ContainerService;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.dpdesktop.module.remote.RemotePlace;
import org.dom4j.DocumentException;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class ContainerServiceTest {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };




    @Test
    public void testExistsMethod() throws DocumentException, Exception {

        RemotePlace config = context.mock(RemotePlace.class);
        ContainerService containerService = new ContainerService(config);

        assertFalse(containerService.exists(new Container("testID", null)));

        assertFalse(containerService.exists(new Container("ID-x-50", null)));
        assertFalse(containerService.exists(new Container("ID-2-20", null)));
        assertFalse(containerService.exists(new Container("ID-1-99", null)));


        String xml = "" +
                "<root>" +
                "<container name=\"company\" id=\"ID-1-99\" isTrackable=\"false\">" +
                "<container name=\"project\" id=\"ID-2-20\" isTrackable=\"true\">" +
                "<container name=\"task\" id=\"ID-x-50\" isTrackable=\"true\">" +
                "</container>" +
                "</container>" +
                "</container>" +
                "</root>";

        ContextString c = new ContextString();

        c.setReader(new StringReader(xml));
        containerService.load(c);

        assertFalse(containerService.exists(new Container("testID", null)));

        assertTrue(containerService.exists(new Container("ID-x-50", null)));
        assertTrue(containerService.exists(new Container("ID-2-20", null)));
        assertTrue(containerService.exists(new Container("ID-1-99", null)));

        context.assertIsSatisfied();


    }

    /**
     * An empty container service should deliver a root container
     * without children!
     */
    @Test
    public void testEmptyContainerDelivery() {

        final RemotePlace remotePlace = context.mock(RemotePlace.class);

        context.checking(new Expectations(){{        
            allowing(remotePlace).getCompany();
        }});

        ContainerService containerService = new ContainerService(remotePlace);

        assertEquals(containerService.getRootContainer().getChildren().size(), 0);

        context.assertIsSatisfied();


    }

    @Test
    public void testDocumentParsing() throws DocumentException, Exception {

        final RemotePlace remotePlace = context.mock(RemotePlace.class);
        ContainerService containerService = new ContainerService(remotePlace);
        context.checking(new Expectations(){{
            allowing(remotePlace).getCompany();
        }});
        String xml = "" +
                "<root>" +
                "<container name=\"company\" id=\"ID-1-99\" isTrackable=\"false\">" +
                "<container name=\"project\" id=\"ID-2-20\" isTrackable=\"true\">" +
                "<container name=\"task\" id=\"ID-x-50\" isTrackable=\"true\">" +
                "</container>" +
                "</container>" +
                "</container>" +
                "</root>";


        ContextString c = new ContextString();

        c.setReader(new StringReader(xml));
        containerService.load(c);

        Container root = containerService.getRootContainer();

        Container company = root.getChildren().get(0);
        Container project = company.getChildren().get(0);
        Container task = project.getChildren().get(0);

        assertEquals(company.get("id"), "ID-1-99");
        assertEquals(project.get("id"), "ID-2-20");
        assertEquals(task.get("id"), "ID-x-50");

        assertEquals(root.getCreator(), containerService);
        assertEquals(company.getCreator(), containerService);
        assertEquals(project.getCreator(), containerService);
        assertEquals(task.getCreator(), containerService);

        context.assertIsSatisfied();


    }
}
