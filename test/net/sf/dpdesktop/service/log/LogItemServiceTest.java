/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.service.log;

import net.sf.dpdesktop.service.*;
import net.sf.dpdesktop.service.log.LogItemRepository;
import net.sf.dpdesktop.service.ContextString;
import net.sf.dpdesktop.service.ContextProvider;
import net.sf.dpdesktop.service.container.ContainerService;
import net.sf.dpdesktop.service.DataService;
import net.sf.dpdesktop.service.log.LogItemService;
import net.sf.dpdesktop.service.log.LogItem;
import net.sf.dpdesktop.service.container.Container;
import java.io.StringReader;
import java.util.List;
import net.sf.dpdesktop.module.remote.RemotePlace;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class LogItemServiceTest  {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };

    /**
     * Test of getLogItemList method, of class LogItemService.
     */
    @Test
    public void testGetLogList() throws Exception {

        String xml = "" +
                "<root>" +
                "<log id=\"57\" summary=\"teeeeeeeeee\" comment=\"teeeeeeeeeeeeeeeeeeeeeeeeeeeeeest\" workedTime=\"00:30\" date=\"2009-04-10 08:51:54\">" +
                "<container id=\"ID-3-1\" owner=\"admin\" name=\"Task+I\" priority=\"normal\" complete=\"5%25\" isTrackable=\"true\"/>" +
                "</log>" +
                "<log id=\"56\" summary=\"teeeeeeeeeeeee\" comment=\"teeeeeeeeest\" workedTime=\"00:10\" date=\"2009-04-10 08:48:30\">" +
                "<container id=\"ID-3-2\" owner=\"admin\" name=\"Task+I\" priority=\"normal\" complete=\"5%25\" isTrackable=\"false\"/>" +
                "</log>" +
                "</root>";

        RemotePlace config = context.mock(RemotePlace.class);

        final LogItemRepository logItemRepository = context.mock(LogItemRepository.class);

        context.checking(new Expectations() {{
            ignoring(logItemRepository);
        }});

        LogItemService logItemService = new LogItemService(config);

        ContextString c = new ContextString();

        c.setReader(new StringReader(xml));
        logItemService.load(c);


        List<LogItem> list = logItemService.getLogItemList();

        assertEquals(list.size(), 2);

        Container c0 = list.get(0).getContainer();

        assertEquals(c0.getID(), "ID-3-1");
        assertEquals(c0.isTrackable(), true);

        Container c1 = list.get(1).getContainer();

        assertEquals(c1.getID(), "ID-3-2");
        assertEquals(c1.isTrackable(), false);

        /*
        other attribute values do not need to be testet,
        because they should not lead to critical errors
         */

        context.assertIsSatisfied();


    }

    @Test
    public void testSuperclass() {
        assertEquals(ContainerService.class.getSuperclass(), DataService.class);
    }
    
}
