/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service.log;

import net.sf.dpdesktop.service.container.Container;
import net.sf.dpdesktop.service.log.LogItem;
import net.sf.dpdesktop.util.Property;
import net.sf.dpdesktop.util.PropertyTest;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class LogItemTest {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }

    };


    /**
     * Test of getContainer method, of class LogItem.
     */
    @Test
    public void testGetter() {

        Container container = context.mock(Container.class);

        LogItem instance = new LogItem("summaryValue", "commentValue", container);

        assertEquals(container, instance.getContainer());
        assertEquals("summaryValue", instance.get("summary"));
        assertEquals("commentValue", instance.get("comment"));
    }


    @Test
    public void testProperty() {
        assertEquals(LogItem.class.getSuperclass(), Property.class); 
    }

}