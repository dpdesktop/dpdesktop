/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.service.log;

import net.sf.dpdesktop.service.log.LogItemRepository;
import net.sf.dpdesktop.service.log.LogItemRepositoryListener;
import net.sf.dpdesktop.service.log.LogItem;
import java.util.List;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 *
 * @author Heiner Reinhardt
 */
public class LogitemRepositoryTest {

    private Mockery context = new JUnit4Mockery() {

        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };

    @Test
    public void simpleAPITest() {

        final List list = context.mock(List.class);
        final LogItemRepositoryListener logItemRepositoryListener = context.mock(LogItemRepositoryListener.class);

        context.checking(new Expectations() {{
            
            exactly(2).of (logItemRepositoryListener).listChanged(with(any(List.class)));
            
        }});
        LogItemRepository logItemRepository = new LogItemRepository();

        logItemRepository.addLogItemRepositoryListener(logItemRepositoryListener);

        logItemRepository.setList(list);

        context.assertIsSatisfied();

    }

}
