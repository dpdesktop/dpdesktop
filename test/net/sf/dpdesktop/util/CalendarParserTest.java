/**********************************************************************
*  Copyright notice
*
*  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
*  All rights reserved
*
*  DPDesktop is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the program!
**********************************************************************/

package net.sf.dpdesktop.util;

import java.text.ParseException;
import java.util.GregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class CalendarParserTest {

    public CalendarParserTest() {
    }

    @Test
    public void test() throws ParseException {

        int year = 2008;
        int month = 9;
        int day = 12;
        int hour = 9;
        int min = 8;
        int sec = 7;

        GregorianCalendar calendar = new GregorianCalendar(year, month-1, day, hour, min, sec);

        String date = String.format("%04d-%02d-%02d %02d:%02d:%02d",
                year, month, day, hour, min, sec);
        
        assertEquals(CalendarParser.getString(calendar), date);
        assertEquals(CalendarParser.getCalendar(date), calendar);

    }

}