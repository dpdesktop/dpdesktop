/**********************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Heiner Reinhardt, Tolleiv Nietsch
 *  All rights reserved
 *
 *  DPDesktop is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the program!
 **********************************************************************/
package net.sf.dpdesktop.util;


import net.sf.dpdesktop.module.util.hash.HashFactory;
import java.security.NoSuchAlgorithmException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Heiner Reinhardt
 */
public class HashTest {

    /**
     * This is somehow a lazy test. 3 given hash strings will be compared 
     * with the result of the md5() function available in php.
     */

    @Test
    public void testMd5() throws NoSuchAlgorithmException {
        assertEquals("098f6bcd4621d373cade4e832627b4f6", new HashFactory().getValue("test"));
        assertEquals("0cbc6611f5540bd0809a388dc95a615b", new HashFactory().getValue("Test"));
        assertEquals("3858f62230ac3c915f300c664312c63f", new HashFactory().getValue("foobar"));
    }

}